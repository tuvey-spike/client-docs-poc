[client](../../../index.md) / [Exports](../modules.md) / [lib/ErrorBoundary](../modules/lib_ErrorBoundary.md) / ErrorBoundary

# Class: ErrorBoundary

[lib/ErrorBoundary](../modules/lib_ErrorBoundary.md).ErrorBoundary

Component for catching any script errors in UI definitions.
Gracefully handling it to prevent the application from crash,
displaying specified fallback UI.

## Hierarchy

- `Component`<[`IErrorBoundaryProps`](../interfaces/lib_ErrorBoundary.IErrorBoundaryProps.md), `IErrorBoundaryState`\>

  ↳ **`ErrorBoundary`**

## Table of contents

### Constructors

- [constructor](lib_ErrorBoundary.ErrorBoundary.md#constructor)

### Methods

- [componentDidCatch](lib_ErrorBoundary.ErrorBoundary.md#componentdidcatch)
- [render](lib_ErrorBoundary.ErrorBoundary.md#render)

## Constructors

### constructor

• **new ErrorBoundary**(`props`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `props` | [`IErrorBoundaryProps`](../interfaces/lib_ErrorBoundary.IErrorBoundaryProps.md) |

#### Overrides

Component&lt;IErrorBoundaryProps, IErrorBoundaryState\&gt;.constructor

#### Defined in

[lib/ErrorBoundary/index.tsx:40](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ErrorBoundary/index.tsx#lines-40)

## Methods

### componentDidCatch

▸ **componentDidCatch**(`error`, `errorInfo`): `void`

Updates state when the component caught an error in a child component.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `error` | `Error` | The error that has been caught. |
| `errorInfo` | `ErrorInfo` | Additional error information, e.g the call stack. |

#### Returns

`void`

#### Overrides

Component.componentDidCatch

#### Defined in

[lib/ErrorBoundary/index.tsx:50](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ErrorBoundary/index.tsx#lines-50)

___

### render

▸ **render**(): `ReactNode`

#### Returns

`ReactNode`

#### Overrides

Component.render

#### Defined in

[lib/ErrorBoundary/index.tsx:57](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ErrorBoundary/index.tsx#lines-57)
