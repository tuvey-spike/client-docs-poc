[client](../../../index.md) / [Exports](../modules.md) / [lib/ExpandableSection](../modules/lib_ExpandableSection.md) / IExpandableSectionAction

# Interface: IExpandableSectionAction

[lib/ExpandableSection](../modules/lib_ExpandableSection.md).IExpandableSectionAction

## Table of contents

### Properties

- [icon](lib_ExpandableSection.IExpandableSectionAction.md#icon)

### Methods

- [onClick](lib_ExpandableSection.IExpandableSectionAction.md#onclick)

## Properties

### icon

• **icon**: `string`

#### Defined in

[lib/ExpandableSection/index.tsx:25](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ExpandableSection/index.tsx#lines-25)

## Methods

### onClick

▸ **onClick**(): `void`

#### Returns

`void`

#### Defined in

[lib/ExpandableSection/index.tsx:26](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ExpandableSection/index.tsx#lines-26)
