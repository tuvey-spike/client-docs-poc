[client](../../../index.md) / [Exports](../modules.md) / [lib/Content/Icon](../modules/lib_Content_Icon.md) / IIconProps

# Interface: IIconProps

[lib/Content/Icon](../modules/lib_Content_Icon.md).IIconProps

## Hierarchy

- `SVGProps`<`SVGSVGElement`\>

  ↳ **`IIconProps`**

## Table of contents

### Properties

- [fill](lib_Content_Icon.IIconProps.md#fill)
- [name](lib_Content_Icon.IIconProps.md#name)
- [size](lib_Content_Icon.IIconProps.md#size)

## Properties

### fill

• `Optional` **fill**: `string`

The fill colour.

#### Overrides

SVGProps.fill

#### Defined in

[lib/Content/Icon/index.tsx:20](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/Content/Icon/index.tsx#lines-20)

___

### name

• **name**: `string`

The name of the icon to display.

#### Overrides

SVGProps.name

#### Defined in

[lib/Content/Icon/index.tsx:15](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/Content/Icon/index.tsx#lines-15)

___

### size

• `Optional` **size**: `string` \| `number`

The dimensions of the resulting svg.

#### Defined in

[lib/Content/Icon/index.tsx:25](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/Content/Icon/index.tsx#lines-25)
