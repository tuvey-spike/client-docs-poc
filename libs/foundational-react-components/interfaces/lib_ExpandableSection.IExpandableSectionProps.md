[client](../../../index.md) / [Exports](../modules.md) / [lib/ExpandableSection](../modules/lib_ExpandableSection.md) / IExpandableSectionProps

# Interface: IExpandableSectionProps

[lib/ExpandableSection](../modules/lib_ExpandableSection.md).IExpandableSectionProps

## Table of contents

### Properties

- [actions](lib_ExpandableSection.IExpandableSectionProps.md#actions)
- [children](lib_ExpandableSection.IExpandableSectionProps.md#children)
- [prefix](lib_ExpandableSection.IExpandableSectionProps.md#prefix)
- [title](lib_ExpandableSection.IExpandableSectionProps.md#title)
- [values](lib_ExpandableSection.IExpandableSectionProps.md#values)

### Methods

- [onClick](lib_ExpandableSection.IExpandableSectionProps.md#onclick)

## Properties

### actions

• `Optional` **actions**: [`IExpandableSectionAction`](lib_ExpandableSection.IExpandableSectionAction.md)[]

#### Defined in

[lib/ExpandableSection/index.tsx:31](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ExpandableSection/index.tsx#lines-31)

___

### children

• **children**: `ReactChild` \| `ReactChild`[] \| `Element`[]

#### Defined in

[lib/ExpandableSection/index.tsx:33](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ExpandableSection/index.tsx#lines-33)

___

### prefix

• `Optional` **prefix**: `string`

#### Defined in

[lib/ExpandableSection/index.tsx:32](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ExpandableSection/index.tsx#lines-32)

___

### title

• **title**: `string`

#### Defined in

[lib/ExpandableSection/index.tsx:30](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ExpandableSection/index.tsx#lines-30)

___

### values

• `Optional` **values**: [`IExpandableSectionValue`](lib_ExpandableSection.IExpandableSectionValue.md)[]

#### Defined in

[lib/ExpandableSection/index.tsx:34](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ExpandableSection/index.tsx#lines-34)

## Methods

### onClick

▸ `Optional` **onClick**(): `void`

#### Returns

`void`

#### Defined in

[lib/ExpandableSection/index.tsx:35](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ExpandableSection/index.tsx#lines-35)
