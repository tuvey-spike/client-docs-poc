[client](../../../index.md) / [Exports](../modules.md) / [lib/ExpandableSection](../modules/lib_ExpandableSection.md) / IExpandableSectionValue

# Interface: IExpandableSectionValue

[lib/ExpandableSection](../modules/lib_ExpandableSection.md).IExpandableSectionValue

## Table of contents

### Properties

- [label](lib_ExpandableSection.IExpandableSectionValue.md#label)
- [value](lib_ExpandableSection.IExpandableSectionValue.md#value)

## Properties

### label

• **label**: `string`

#### Defined in

[lib/ExpandableSection/index.tsx:20](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ExpandableSection/index.tsx#lines-20)

___

### value

• **value**: `string`

#### Defined in

[lib/ExpandableSection/index.tsx:21](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ExpandableSection/index.tsx#lines-21)
