[client](../../../index.md) / [Exports](../modules.md) / [lib/InfiniteScroller](../modules/lib_InfiniteScroller.md) / IInfiniteScrollerProps

# Interface: IInfiniteScrollerProps

[lib/InfiniteScroller](../modules/lib_InfiniteScroller.md).IInfiniteScrollerProps

## Table of contents

### Properties

- [children](lib_InfiniteScroller.IInfiniteScrollerProps.md#children)
- [root](lib_InfiniteScroller.IInfiniteScrollerProps.md#root)
- [rootMargin](lib_InfiniteScroller.IInfiniteScrollerProps.md#rootmargin)

### Methods

- [callback](lib_InfiniteScroller.IInfiniteScrollerProps.md#callback)

## Properties

### children

• **children**: `ReactNode`

Child elements wrapped by the component.

#### Defined in

[lib/InfiniteScroller/index.tsx:13](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/InfiniteScroller/index.tsx#lines-13)

___

### root

• `Optional` **root**: ``null`` \| `Element` \| `Document`

The element to check intersection on.
(Leave null for the root element of the app.)

#### Defined in

[lib/InfiniteScroller/index.tsx:31](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/InfiniteScroller/index.tsx#lines-31)

___

### rootMargin

• `Optional` **rootMargin**: `string`

The margin used on the root element when detecting
intersection.

#### Defined in

[lib/InfiniteScroller/index.tsx:25](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/InfiniteScroller/index.tsx#lines-25)

## Methods

### callback

▸ `Optional` **callback**(): `void`

Callback function called when intersection is
detected.

#### Returns

`void`

#### Defined in

[lib/InfiniteScroller/index.tsx:19](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/InfiniteScroller/index.tsx#lines-19)
