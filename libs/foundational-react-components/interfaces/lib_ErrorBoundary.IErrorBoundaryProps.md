[client](../../../index.md) / [Exports](../modules.md) / [lib/ErrorBoundary](../modules/lib_ErrorBoundary.md) / IErrorBoundaryProps

# Interface: IErrorBoundaryProps

[lib/ErrorBoundary](../modules/lib_ErrorBoundary.md).IErrorBoundaryProps

## Table of contents

### Properties

- [children](lib_ErrorBoundary.IErrorBoundaryProps.md#children)
- [fallback](lib_ErrorBoundary.IErrorBoundaryProps.md#fallback)

## Properties

### children

• **children**: `ReactNode`

Child elements protected by the error boundary.

#### Defined in

[lib/ErrorBoundary/index.tsx:13](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ErrorBoundary/index.tsx#lines-13)

___

### fallback

• **fallback**: `ReactElement`<`IErrorBoundaryState`, `string` \| `JSXElementConstructor`<`any`\>\>

The fallback UI to render when an error is caught.

#### Defined in

[lib/ErrorBoundary/index.tsx:18](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ErrorBoundary/index.tsx#lines-18)
