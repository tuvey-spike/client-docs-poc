[client](../../../index.md) / [Exports](../modules.md) / [lib/ErrorStack](../modules/lib_ErrorStack.md) / IErrorStackProps

# Interface: IErrorStackProps

[lib/ErrorStack](../modules/lib_ErrorStack.md).IErrorStackProps

## Table of contents

### Properties

- [error](lib_ErrorStack.IErrorStackProps.md#error)
- [errorInfo](lib_ErrorStack.IErrorStackProps.md#errorinfo)
- [small](lib_ErrorStack.IErrorStackProps.md#small)

## Properties

### error

• `Optional` **error**: `Error`

The error to display.

#### Defined in

[lib/ErrorStack/index.tsx:17](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ErrorStack/index.tsx#lines-17)

___

### errorInfo

• `Optional` **errorInfo**: `ErrorInfo`

Additional error information, e.g the call stack.

#### Defined in

[lib/ErrorStack/index.tsx:22](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ErrorStack/index.tsx#lines-22)

___

### small

• `Optional` **small**: `Boolean`

Indicates the error stack should display in a smaller area.

#### Defined in

[lib/ErrorStack/index.tsx:27](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ErrorStack/index.tsx#lines-27)
