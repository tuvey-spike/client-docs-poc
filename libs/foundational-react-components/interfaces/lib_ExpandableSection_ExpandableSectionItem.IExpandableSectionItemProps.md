[client](../../../index.md) / [Exports](../modules.md) / [lib/ExpandableSection/ExpandableSectionItem](../modules/lib_ExpandableSection_ExpandableSectionItem.md) / IExpandableSectionItemProps

# Interface: IExpandableSectionItemProps

[lib/ExpandableSection/ExpandableSectionItem](../modules/lib_ExpandableSection_ExpandableSectionItem.md).IExpandableSectionItemProps

## Table of contents

### Properties

- [actions](lib_ExpandableSection_ExpandableSectionItem.IExpandableSectionItemProps.md#actions)
- [title](lib_ExpandableSection_ExpandableSectionItem.IExpandableSectionItemProps.md#title)
- [values](lib_ExpandableSection_ExpandableSectionItem.IExpandableSectionItemProps.md#values)

### Methods

- [onClick](lib_ExpandableSection_ExpandableSectionItem.IExpandableSectionItemProps.md#onclick)

## Properties

### actions

• `Optional` **actions**: [`IExpandableSectionItemAction`](lib_ExpandableSection_ExpandableSectionItem.IExpandableSectionItemAction.md)[]

#### Defined in

[lib/ExpandableSection/ExpandableSectionItem/index.tsx:24](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ExpandableSection/ExpandableSectionItem/index.tsx#lines-24)

___

### title

• **title**: `string`

#### Defined in

[lib/ExpandableSection/ExpandableSectionItem/index.tsx:23](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ExpandableSection/ExpandableSectionItem/index.tsx#lines-23)

___

### values

• `Optional` **values**: [`IExpandableSectionItemValue`](lib_ExpandableSection_ExpandableSectionItem.IExpandableSectionItemValue.md)[]

#### Defined in

[lib/ExpandableSection/ExpandableSectionItem/index.tsx:25](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ExpandableSection/ExpandableSectionItem/index.tsx#lines-25)

## Methods

### onClick

▸ `Optional` **onClick**(): `void`

#### Returns

`void`

#### Defined in

[lib/ExpandableSection/ExpandableSectionItem/index.tsx:26](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ExpandableSection/ExpandableSectionItem/index.tsx#lines-26)
