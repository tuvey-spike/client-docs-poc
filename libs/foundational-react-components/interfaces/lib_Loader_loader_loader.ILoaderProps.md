[client](../../../index.md) / [Exports](../modules.md) / [lib/Loader/loader/loader](../modules/lib_Loader_loader_loader.md) / ILoaderProps

# Interface: ILoaderProps

[lib/Loader/loader/loader](../modules/lib_Loader_loader_loader.md).ILoaderProps

## Table of contents

### Properties

- [text](lib_Loader_loader_loader.ILoaderProps.md#text)

## Properties

### text

• **text**: `string`

#### Defined in

[lib/Loader/loader/loader.tsx:3](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/Loader/loader/loader.tsx#lines-3)
