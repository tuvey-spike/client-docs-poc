[client](../../../index.md) / [Exports](../modules.md) / [lib/ExpandableSection/ExpandableSectionItem](../modules/lib_ExpandableSection_ExpandableSectionItem.md) / IExpandableSectionItemAction

# Interface: IExpandableSectionItemAction

[lib/ExpandableSection/ExpandableSectionItem](../modules/lib_ExpandableSection_ExpandableSectionItem.md).IExpandableSectionItemAction

## Table of contents

### Properties

- [icon](lib_ExpandableSection_ExpandableSectionItem.IExpandableSectionItemAction.md#icon)

### Methods

- [onClick](lib_ExpandableSection_ExpandableSectionItem.IExpandableSectionItemAction.md#onclick)

## Properties

### icon

• **icon**: `string`

#### Defined in

[lib/ExpandableSection/ExpandableSectionItem/index.tsx:18](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ExpandableSection/ExpandableSectionItem/index.tsx#lines-18)

## Methods

### onClick

▸ **onClick**(): `void`

#### Returns

`void`

#### Defined in

[lib/ExpandableSection/ExpandableSectionItem/index.tsx:19](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ExpandableSection/ExpandableSectionItem/index.tsx#lines-19)
