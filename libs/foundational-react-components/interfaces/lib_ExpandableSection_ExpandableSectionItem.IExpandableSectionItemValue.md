[client](../../../index.md) / [Exports](../modules.md) / [lib/ExpandableSection/ExpandableSectionItem](../modules/lib_ExpandableSection_ExpandableSectionItem.md) / IExpandableSectionItemValue

# Interface: IExpandableSectionItemValue

[lib/ExpandableSection/ExpandableSectionItem](../modules/lib_ExpandableSection_ExpandableSectionItem.md).IExpandableSectionItemValue

## Table of contents

### Properties

- [label](lib_ExpandableSection_ExpandableSectionItem.IExpandableSectionItemValue.md#label)
- [value](lib_ExpandableSection_ExpandableSectionItem.IExpandableSectionItemValue.md#value)

## Properties

### label

• **label**: `string`

#### Defined in

[lib/ExpandableSection/ExpandableSectionItem/index.tsx:13](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ExpandableSection/ExpandableSectionItem/index.tsx#lines-13)

___

### value

• **value**: `string`

#### Defined in

[lib/ExpandableSection/ExpandableSectionItem/index.tsx:14](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ExpandableSection/ExpandableSectionItem/index.tsx#lines-14)
