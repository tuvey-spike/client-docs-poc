[client](../../../index.md) / [Exports](../modules.md) / lib/Content/Icon

# Module: lib/Content/Icon

## Table of contents

### Interfaces

- [IIconProps](../interfaces/lib_Content_Icon.IIconProps.md)

### Functions

- [Icon](lib_Content_Icon.md#icon)

## Functions

### Icon

▸ **Icon**(`props`): `Element`

Simple icon component.

#### Parameters

| Name | Type |
| :------ | :------ |
| `props` | [`IIconProps`](../interfaces/lib_Content_Icon.IIconProps.md) |

#### Returns

`Element`

#### Defined in

[lib/Content/Icon/index.tsx:31](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/Content/Icon/index.tsx#lines-31)
