[client](../../../index.md) / [Exports](../modules.md) / lib/Loader/loader/loader

# Module: lib/Loader/loader/loader

## Table of contents

### References

- [default](lib_Loader_loader_loader.md#default)

### Interfaces

- [ILoaderProps](../interfaces/lib_Loader_loader_loader.ILoaderProps.md)

### Functions

- [Loader](lib_Loader_loader_loader.md#loader)

## References

### default

Renames and re-exports [Loader](lib_Loader_loader_loader.md#loader)

## Functions

### Loader

▸ **Loader**(`__namedParameters`): `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | [`ILoaderProps`](../interfaces/lib_Loader_loader_loader.ILoaderProps.md) |

#### Returns

`Element`

#### Defined in

[lib/Loader/loader/loader.tsx:6](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/Loader/loader/loader.tsx#lines-6)
