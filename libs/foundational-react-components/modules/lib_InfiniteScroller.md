[client](../../../index.md) / [Exports](../modules.md) / lib/InfiniteScroller

# Module: lib/InfiniteScroller

## Table of contents

### Interfaces

- [IInfiniteScrollerProps](../interfaces/lib_InfiniteScroller.IInfiniteScrollerProps.md)

### Functions

- [InfiniteScroller](lib_InfiniteScroller.md#infinitescroller)

## Functions

### InfiniteScroller

▸ **InfiniteScroller**(`__namedParameters`): `Element`

Component for implementing infinite scroller functionality.

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | [`IInfiniteScrollerProps`](../interfaces/lib_InfiniteScroller.IInfiniteScrollerProps.md) |

#### Returns

`Element`

#### Defined in

[lib/InfiniteScroller/index.tsx:37](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/InfiniteScroller/index.tsx#lines-37)
