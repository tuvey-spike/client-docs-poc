[client](../../../index.md) / [Exports](../modules.md) / lib/ErrorBoundary

# Module: lib/ErrorBoundary

## Table of contents

### Classes

- [ErrorBoundary](../classes/lib_ErrorBoundary.ErrorBoundary.md)

### Interfaces

- [IErrorBoundaryProps](../interfaces/lib_ErrorBoundary.IErrorBoundaryProps.md)
