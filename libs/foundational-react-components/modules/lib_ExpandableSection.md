[client](../../../index.md) / [Exports](../modules.md) / lib/ExpandableSection

# Module: lib/ExpandableSection

## Table of contents

### References

- [ExpandableSectionItem](lib_ExpandableSection.md#expandablesectionitem)
- [IExpandableSectionItemAction](lib_ExpandableSection.md#iexpandablesectionitemaction)
- [IExpandableSectionItemProps](lib_ExpandableSection.md#iexpandablesectionitemprops)
- [IExpandableSectionItemValue](lib_ExpandableSection.md#iexpandablesectionitemvalue)

### Interfaces

- [IExpandableSectionAction](../interfaces/lib_ExpandableSection.IExpandableSectionAction.md)
- [IExpandableSectionProps](../interfaces/lib_ExpandableSection.IExpandableSectionProps.md)
- [IExpandableSectionValue](../interfaces/lib_ExpandableSection.IExpandableSectionValue.md)

### Functions

- [ExpandableSection](lib_ExpandableSection.md#expandablesection)

## References

### ExpandableSectionItem

Re-exports [ExpandableSectionItem](lib_ExpandableSection_ExpandableSectionItem.md#expandablesectionitem)

___

### IExpandableSectionItemAction

Re-exports [IExpandableSectionItemAction](../interfaces/lib_ExpandableSection_ExpandableSectionItem.IExpandableSectionItemAction.md)

___

### IExpandableSectionItemProps

Re-exports [IExpandableSectionItemProps](../interfaces/lib_ExpandableSection_ExpandableSectionItem.IExpandableSectionItemProps.md)

___

### IExpandableSectionItemValue

Re-exports [IExpandableSectionItemValue](../interfaces/lib_ExpandableSection_ExpandableSectionItem.IExpandableSectionItemValue.md)

## Functions

### ExpandableSection

▸ **ExpandableSection**(`__namedParameters`): `Element`

**`component`**

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | [`IExpandableSectionProps`](../interfaces/lib_ExpandableSection.IExpandableSectionProps.md) |

#### Returns

`Element`

#### Defined in

[lib/ExpandableSection/index.tsx:41](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ExpandableSection/index.tsx#lines-41)
