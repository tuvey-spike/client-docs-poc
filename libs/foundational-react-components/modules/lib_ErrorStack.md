[client](../../../index.md) / [Exports](../modules.md) / lib/ErrorStack

# Module: lib/ErrorStack

## Table of contents

### Interfaces

- [IErrorStackProps](../interfaces/lib_ErrorStack.IErrorStackProps.md)

### Functions

- [ErrorStack](lib_ErrorStack.md#errorstack)

## Functions

### ErrorStack

▸ **ErrorStack**(`props`): `Element`

Debug component for displaying errors.

#### Parameters

| Name | Type |
| :------ | :------ |
| `props` | [`IErrorStackProps`](../interfaces/lib_ErrorStack.IErrorStackProps.md) |

#### Returns

`Element`

#### Defined in

[lib/ErrorStack/index.tsx:33](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ErrorStack/index.tsx#lines-33)
