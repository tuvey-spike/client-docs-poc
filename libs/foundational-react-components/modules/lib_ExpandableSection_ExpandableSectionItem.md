[client](../../../index.md) / [Exports](../modules.md) / lib/ExpandableSection/ExpandableSectionItem

# Module: lib/ExpandableSection/ExpandableSectionItem

## Table of contents

### Interfaces

- [IExpandableSectionItemAction](../interfaces/lib_ExpandableSection_ExpandableSectionItem.IExpandableSectionItemAction.md)
- [IExpandableSectionItemProps](../interfaces/lib_ExpandableSection_ExpandableSectionItem.IExpandableSectionItemProps.md)
- [IExpandableSectionItemValue](../interfaces/lib_ExpandableSection_ExpandableSectionItem.IExpandableSectionItemValue.md)

### Functions

- [ExpandableSectionItem](lib_ExpandableSection_ExpandableSectionItem.md#expandablesectionitem)

## Functions

### ExpandableSectionItem

▸ **ExpandableSectionItem**(`__namedParameters`): `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | [`IExpandableSectionItemProps`](../interfaces/lib_ExpandableSection_ExpandableSectionItem.IExpandableSectionItemProps.md) |

#### Returns

`Element`

#### Defined in

[lib/ExpandableSection/ExpandableSectionItem/index.tsx:29](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/foundational-react-components/src/lib/ExpandableSection/ExpandableSectionItem/index.tsx#lines-29)
