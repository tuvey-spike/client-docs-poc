[client](../../../index.md) / [Exports](../modules.md) / [lib/http](../modules/lib_http.md) / HttpStatusCode

# Enumeration: HttpStatusCode

[lib/http](../modules/lib_http.md).HttpStatusCode

## Table of contents

### Enumeration members

- [BAD\_REQUEST](lib_http.HttpStatusCode.md#bad_request)
- [CONFLICT](lib_http.HttpStatusCode.md#conflict)
- [CREATED](lib_http.HttpStatusCode.md#created)
- [FORBIDDEN](lib_http.HttpStatusCode.md#forbidden)
- [INTERNAL\_SERVER\_ERROR](lib_http.HttpStatusCode.md#internal_server_error)
- [NETWORK\_AUTH\_REQUIRED](lib_http.HttpStatusCode.md#network_auth_required)
- [NOT\_FOUND](lib_http.HttpStatusCode.md#not_found)
- [NO\_CONTENT](lib_http.HttpStatusCode.md#no_content)
- [OK](lib_http.HttpStatusCode.md#ok)
- [UNAUTHORIZED](lib_http.HttpStatusCode.md#unauthorized)

## Enumeration members

### BAD\_REQUEST

• **BAD\_REQUEST** = `400`

#### Defined in

[api/src/lib/http.ts:23](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/lib/http.ts#lines-23)

___

### CONFLICT

• **CONFLICT** = `409`

#### Defined in

[api/src/lib/http.ts:27](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/lib/http.ts#lines-27)

___

### CREATED

• **CREATED** = `201`

#### Defined in

[api/src/lib/http.ts:21](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/lib/http.ts#lines-21)

___

### FORBIDDEN

• **FORBIDDEN** = `403`

#### Defined in

[api/src/lib/http.ts:25](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/lib/http.ts#lines-25)

___

### INTERNAL\_SERVER\_ERROR

• **INTERNAL\_SERVER\_ERROR** = `500`

#### Defined in

[api/src/lib/http.ts:28](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/lib/http.ts#lines-28)

___

### NETWORK\_AUTH\_REQUIRED

• **NETWORK\_AUTH\_REQUIRED** = `511`

#### Defined in

[api/src/lib/http.ts:29](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/lib/http.ts#lines-29)

___

### NOT\_FOUND

• **NOT\_FOUND** = `404`

#### Defined in

[api/src/lib/http.ts:26](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/lib/http.ts#lines-26)

___

### NO\_CONTENT

• **NO\_CONTENT** = `204`

#### Defined in

[api/src/lib/http.ts:22](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/lib/http.ts#lines-22)

___

### OK

• **OK** = `200`

#### Defined in

[api/src/lib/http.ts:20](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/lib/http.ts#lines-20)

___

### UNAUTHORIZED

• **UNAUTHORIZED** = `401`

#### Defined in

[api/src/lib/http.ts:24](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/lib/http.ts#lines-24)
