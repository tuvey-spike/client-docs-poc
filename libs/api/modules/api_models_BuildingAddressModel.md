[client](../../../index.md) / [Exports](../modules.md) / api/models/BuildingAddressModel

# Module: api/models/BuildingAddressModel

## Table of contents

### Type aliases

- [BuildingAddressModel](api_models_BuildingAddressModel.md#buildingaddressmodel)

## Type aliases

### BuildingAddressModel

Ƭ **BuildingAddressModel**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `cityTown` | `string` |
| `country?` | `string` \| ``null`` |
| `line1` | `string` |
| `line2?` | `string` \| ``null`` |
| `postCode` | `string` |

#### Defined in

[api/src/api/models/BuildingAddressModel.ts:5](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/models/BuildingAddressModel.ts#lines-5)
