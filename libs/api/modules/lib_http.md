[client](../../../index.md) / [Exports](../modules.md) / lib/http

# Module: lib/http

## Table of contents

### Enumerations

- [HttpStatusCode](../enums/lib_http.HttpStatusCode.md)

### Classes

- [UniteHttpError](../classes/lib_http.UniteHttpError.md)

### Interfaces

- [IUniteHttp](../interfaces/lib_http.IUniteHttp.md)
