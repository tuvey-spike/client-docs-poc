[client](../../../index.md) / [Exports](../modules.md) / api/models/ContactModel

# Module: api/models/ContactModel

## Table of contents

### Type aliases

- [ContactModel](api_models_ContactModel.md#contactmodel)

## Type aliases

### ContactModel

Ƭ **ContactModel**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `contactNo?` | `string` \| ``null`` |
| `email` | `string` |
| `firstName` | `string` |
| `id?` | `string` |
| `surname` | `string` |

#### Defined in

[api/src/api/models/ContactModel.ts:5](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/models/ContactModel.ts#lines-5)
