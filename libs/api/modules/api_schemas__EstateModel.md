[client](../../../index.md) / [Exports](../modules.md) / api/schemas/$EstateModel

# Module: api/schemas/$EstateModel

## Table of contents

### Variables

- [$EstateModel](api_schemas__EstateModel.md#$estatemodel)

## Variables

### $EstateModel

• **$EstateModel**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `properties` | `Object` |
| `properties.address` | `Object` |
| `properties.address.isRequired` | ``true`` |
| `properties.address.type` | ``"EstateAddressModel"`` |
| `properties.contacts` | `Object` |
| `properties.contacts.contains` | `Object` |
| `properties.contacts.contains.type` | ``"EstateContactModel"`` |
| `properties.contacts.isRequired` | ``true`` |
| `properties.contacts.type` | ``"array"`` |
| `properties.id` | `Object` |
| `properties.id.format` | ``"uuid"`` |
| `properties.id.type` | ``"string"`` |
| `properties.name` | `Object` |
| `properties.name.isRequired` | ``true`` |
| `properties.name.type` | ``"string"`` |
| `properties.region` | `Object` |
| `properties.region.isRequired` | ``true`` |
| `properties.region.type` | ``"string"`` |

#### Defined in

[api/src/api/schemas/$EstateModel.ts:4](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/schemas/$EstateModel.ts#lines-4)
