[client](../../../index.md) / [Exports](../modules.md) / api/schemas/$ProblemDetails

# Module: api/schemas/$ProblemDetails

## Table of contents

### Variables

- [$ProblemDetails](api_schemas__ProblemDetails.md#$problemdetails)

## Variables

### $ProblemDetails

• **$ProblemDetails**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `contains` | `Object` |
| `contains.properties` | `Object` |
| `type` | ``"dictionary"`` |

#### Defined in

[api/src/api/schemas/$ProblemDetails.ts:4](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/schemas/$ProblemDetails.ts#lines-4)
