[client](../../../index.md) / [Exports](../modules.md) / api/schemas/$BuildingContactModel

# Module: api/schemas/$BuildingContactModel

## Table of contents

### Variables

- [$BuildingContactModel](api_schemas__BuildingContactModel.md#$buildingcontactmodel)

## Variables

### $BuildingContactModel

• **$BuildingContactModel**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `properties` | `Object` |
| `properties.contactNo` | `Object` |
| `properties.contactNo.isNullable` | ``true`` |
| `properties.contactNo.type` | ``"string"`` |
| `properties.email` | `Object` |
| `properties.email.format` | ``"email"`` |
| `properties.email.isRequired` | ``true`` |
| `properties.email.type` | ``"string"`` |
| `properties.title` | `Object` |
| `properties.title.isRequired` | ``true`` |
| `properties.title.type` | ``"string"`` |

#### Defined in

[api/src/api/schemas/$BuildingContactModel.ts:4](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/schemas/$BuildingContactModel.ts#lines-4)
