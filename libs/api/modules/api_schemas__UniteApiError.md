[client](../../../index.md) / [Exports](../modules.md) / api/schemas/$UniteApiError

# Module: api/schemas/$UniteApiError

## Table of contents

### Variables

- [$UniteApiError](api_schemas__UniteApiError.md#$uniteapierror)

## Variables

### $UniteApiError

• **$UniteApiError**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `description` | ``"Error class for all API responses"`` |
| `properties` | `Object` |
| `properties.code` | `Object` |
| `properties.code.description` | ``"Error Code"`` |
| `properties.code.format` | ``"int32"`` |
| `properties.code.isNullable` | ``true`` |
| `properties.code.type` | ``"number"`` |
| `properties.key` | `Object` |
| `properties.key.description` | ``"Error Key"`` |
| `properties.key.isNullable` | ``true`` |
| `properties.key.type` | ``"string"`` |
| `properties.message` | `Object` |
| `properties.message.description` | ``"A message describing the error"`` |
| `properties.message.isRequired` | ``true`` |
| `properties.message.type` | ``"string"`` |

#### Defined in

[api/src/api/schemas/$UniteApiError.ts:4](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/schemas/$UniteApiError.ts#lines-4)
