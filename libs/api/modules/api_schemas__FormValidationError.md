[client](../../../index.md) / [Exports](../modules.md) / api/schemas/$FormValidationError

# Module: api/schemas/$FormValidationError

## Table of contents

### Variables

- [$FormValidationError](api_schemas__FormValidationError.md#$formvalidationerror)

## Variables

### $FormValidationError

• **$FormValidationError**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `description` | ``"A type representing the API error when a form fails validation"`` |
| `properties` | `Object` |
| `properties.error` | `Object` |
| `properties.error.description` | ``"Get error identifier"`` |
| `properties.error.isNullable` | ``true`` |
| `properties.error.type` | ``"string"`` |

#### Defined in

[api/src/api/schemas/$FormValidationError.ts:4](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/schemas/$FormValidationError.ts#lines-4)
