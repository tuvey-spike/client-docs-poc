[client](../../../index.md) / [Exports](../modules.md) / api/core/OpenAPI

# Module: api/core/OpenAPI

## Table of contents

### Variables

- [OpenAPI](api_core_OpenAPI.md#openapi)

## Variables

### OpenAPI

• **OpenAPI**: `Config`

#### Defined in

[api/src/api/core/OpenAPI.ts:21](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/OpenAPI.ts#lines-21)
