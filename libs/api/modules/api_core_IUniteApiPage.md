[client](../../../index.md) / [Exports](../modules.md) / api/core/IUniteApiPage

# Module: api/core/IUniteApiPage

## Table of contents

### Type aliases

- [IUniteApiPage](api_core_IUniteApiPage.md#iuniteapipage)

## Type aliases

### IUniteApiPage

Ƭ **IUniteApiPage**<`T`\>: `Object`

#### Type parameters

| Name |
| :------ |
| `T` |

#### Type declaration

| Name | Type |
| :------ | :------ |
| `list` | `T`[] |
| `next` | `number` |
| `pageSize` | `number` |
| `start` | `number` |

#### Defined in

[api/src/api/core/IUniteApiPage.ts:5](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/IUniteApiPage.ts#lines-5)
