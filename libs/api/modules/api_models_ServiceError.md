[client](../../../index.md) / [Exports](../modules.md) / api/models/ServiceError

# Module: api/models/ServiceError

## Table of contents

### Type aliases

- [ServiceError](api_models_ServiceError.md#serviceerror)

## Type aliases

### ServiceError

Ƭ **ServiceError**: `Object`

A type representing the API error when there are problems with a service

#### Type declaration

| Name | Type | Description |
| :------ | :------ | :------ |
| `error?` | `string` \| ``null`` | Get error identifier |

#### Defined in

[api/src/api/models/ServiceError.ts:8](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/models/ServiceError.ts#lines-8)
