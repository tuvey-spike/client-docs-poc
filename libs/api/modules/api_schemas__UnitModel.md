[client](../../../index.md) / [Exports](../modules.md) / api/schemas/$UnitModel

# Module: api/schemas/$UnitModel

## Table of contents

### Variables

- [$UnitModel](api_schemas__UnitModel.md#$unitmodel)

## Variables

### $UnitModel

• **$UnitModel**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `properties` | `Object` |
| `properties.address` | `Object` |
| `properties.address.isRequired` | ``true`` |
| `properties.address.type` | ``"UnitAddressModel"`` |
| `properties.bathrooms` | `Object` |
| `properties.bathrooms.format` | ``"int32"`` |
| `properties.bathrooms.isRequired` | ``true`` |
| `properties.bathrooms.type` | ``"number"`` |
| `properties.beds` | `Object` |
| `properties.beds.format` | ``"int32"`` |
| `properties.beds.isRequired` | ``true`` |
| `properties.beds.type` | ``"number"`` |
| `properties.block` | `Object` |
| `properties.block.isNullable` | ``true`` |
| `properties.block.type` | ``"string"`` |
| `properties.floor` | `Object` |
| `properties.floor.format` | ``"int32"`` |
| `properties.floor.isRequired` | ``true`` |
| `properties.floor.type` | ``"number"`` |
| `properties.id` | `Object` |
| `properties.id.format` | ``"uuid"`` |
| `properties.id.type` | ``"string"`` |
| `properties.type` | `Object` |
| `properties.type.isRequired` | ``true`` |
| `properties.type.type` | ``"string"`` |
| `properties.unitNumber` | `Object` |
| `properties.unitNumber.isRequired` | ``true`` |
| `properties.unitNumber.type` | ``"string"`` |

#### Defined in

[api/src/api/schemas/$UnitModel.ts:4](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/schemas/$UnitModel.ts#lines-4)
