[client](../../../index.md) / [Exports](../modules.md) / api/schemas/$BuildingAddressModel

# Module: api/schemas/$BuildingAddressModel

## Table of contents

### Variables

- [$BuildingAddressModel](api_schemas__BuildingAddressModel.md#$buildingaddressmodel)

## Variables

### $BuildingAddressModel

• **$BuildingAddressModel**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `properties` | `Object` |
| `properties.cityTown` | `Object` |
| `properties.cityTown.isRequired` | ``true`` |
| `properties.cityTown.type` | ``"string"`` |
| `properties.country` | `Object` |
| `properties.country.isNullable` | ``true`` |
| `properties.country.type` | ``"string"`` |
| `properties.line1` | `Object` |
| `properties.line1.isRequired` | ``true`` |
| `properties.line1.type` | ``"string"`` |
| `properties.line2` | `Object` |
| `properties.line2.isNullable` | ``true`` |
| `properties.line2.type` | ``"string"`` |
| `properties.postCode` | `Object` |
| `properties.postCode.isRequired` | ``true`` |
| `properties.postCode.type` | ``"string"`` |

#### Defined in

[api/src/api/schemas/$BuildingAddressModel.ts:4](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/schemas/$BuildingAddressModel.ts#lines-4)
