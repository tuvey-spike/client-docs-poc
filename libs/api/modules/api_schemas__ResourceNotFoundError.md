[client](../../../index.md) / [Exports](../modules.md) / api/schemas/$ResourceNotFoundError

# Module: api/schemas/$ResourceNotFoundError

## Table of contents

### Variables

- [$ResourceNotFoundError](api_schemas__ResourceNotFoundError.md#$resourcenotfounderror)

## Variables

### $ResourceNotFoundError

• **$ResourceNotFoundError**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `description` | ``"A type representing the API error when resources not found"`` |
| `properties` | `Object` |
| `properties.error` | `Object` |
| `properties.error.description` | ``"Get error identifier"`` |
| `properties.error.isNullable` | ``true`` |
| `properties.error.type` | ``"string"`` |

#### Defined in

[api/src/api/schemas/$ResourceNotFoundError.ts:4](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/schemas/$ResourceNotFoundError.ts#lines-4)
