[client](../../../index.md) / [Exports](../modules.md) / api/models/BuildingContactModel

# Module: api/models/BuildingContactModel

## Table of contents

### Type aliases

- [BuildingContactModel](api_models_BuildingContactModel.md#buildingcontactmodel)

## Type aliases

### BuildingContactModel

Ƭ **BuildingContactModel**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `contactNo?` | `string` \| ``null`` |
| `email` | `string` |
| `title` | `string` |

#### Defined in

[api/src/api/models/BuildingContactModel.ts:5](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/models/BuildingContactModel.ts#lines-5)
