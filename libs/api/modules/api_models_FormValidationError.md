[client](../../../index.md) / [Exports](../modules.md) / api/models/FormValidationError

# Module: api/models/FormValidationError

## Table of contents

### Type aliases

- [FormValidationError](api_models_FormValidationError.md#formvalidationerror)

## Type aliases

### FormValidationError

Ƭ **FormValidationError**: `Object`

A type representing the API error when a form fails validation

#### Type declaration

| Name | Type | Description |
| :------ | :------ | :------ |
| `error?` | `string` \| ``null`` | Get error identifier |

#### Defined in

[api/src/api/models/FormValidationError.ts:8](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/models/FormValidationError.ts#lines-8)
