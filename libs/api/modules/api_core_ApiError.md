[client](../../../index.md) / [Exports](../modules.md) / api/core/ApiError

# Module: api/core/ApiError

## Table of contents

### Classes

- [ApiError](../classes/api_core_ApiError.ApiError.md)
