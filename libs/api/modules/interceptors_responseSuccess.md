[client](../../../index.md) / [Exports](../modules.md) / interceptors/responseSuccess

# Module: interceptors/responseSuccess

## Table of contents

### Functions

- [responseSuccess](interceptors_responseSuccess.md#responsesuccess)

## Functions

### responseSuccess

▸ **responseSuccess**(`response`): `AxiosResponse`<`any`, `any`\> \| `Promise`<`AxiosResponse`<`any`, `any`\>\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `response` | `AxiosResponse`<`any`, `any`\> |

#### Returns

`AxiosResponse`<`any`, `any`\> \| `Promise`<`AxiosResponse`<`any`, `any`\>\>

#### Defined in

[api/src/interceptors/responseSuccess.ts:3](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/interceptors/responseSuccess.ts#lines-3)
