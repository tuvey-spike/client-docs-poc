[client](../../../index.md) / [Exports](../modules.md) / api/schemas/$ServiceError

# Module: api/schemas/$ServiceError

## Table of contents

### Variables

- [$ServiceError](api_schemas__ServiceError.md#$serviceerror)

## Variables

### $ServiceError

• **$ServiceError**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `description` | ``"A type representing the API error when there are problems with a service"`` |
| `properties` | `Object` |
| `properties.error` | `Object` |
| `properties.error.description` | ``"Get error identifier"`` |
| `properties.error.isNullable` | ``true`` |
| `properties.error.type` | ``"string"`` |

#### Defined in

[api/src/api/schemas/$ServiceError.ts:4](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/schemas/$ServiceError.ts#lines-4)
