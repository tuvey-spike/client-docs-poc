[client](../../../index.md) / [Exports](../modules.md) / api/models/BuildingModel

# Module: api/models/BuildingModel

## Table of contents

### Type aliases

- [BuildingModel](api_models_BuildingModel.md#buildingmodel)

## Type aliases

### BuildingModel

Ƭ **BuildingModel**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `address` | [`BuildingAddressModel`](api_models_BuildingAddressModel.md#buildingaddressmodel) |
| `contacts?` | [`BuildingContactModel`](api_models_BuildingContactModel.md#buildingcontactmodel)[] \| ``null`` |
| `estateId` | `string` |
| `id?` | `string` |
| `name` | `string` |

#### Defined in

[api/src/api/models/BuildingModel.ts:8](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/models/BuildingModel.ts#lines-8)
