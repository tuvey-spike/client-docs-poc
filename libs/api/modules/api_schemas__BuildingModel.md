[client](../../../index.md) / [Exports](../modules.md) / api/schemas/$BuildingModel

# Module: api/schemas/$BuildingModel

## Table of contents

### Variables

- [$BuildingModel](api_schemas__BuildingModel.md#$buildingmodel)

## Variables

### $BuildingModel

• **$BuildingModel**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `properties` | `Object` |
| `properties.address` | `Object` |
| `properties.address.isRequired` | ``true`` |
| `properties.address.type` | ``"BuildingAddressModel"`` |
| `properties.contacts` | `Object` |
| `properties.contacts.contains` | `Object` |
| `properties.contacts.contains.type` | ``"BuildingContactModel"`` |
| `properties.contacts.isNullable` | ``true`` |
| `properties.contacts.type` | ``"array"`` |
| `properties.estateId` | `Object` |
| `properties.estateId.format` | ``"uuid"`` |
| `properties.estateId.isRequired` | ``true`` |
| `properties.estateId.type` | ``"string"`` |
| `properties.id` | `Object` |
| `properties.id.format` | ``"uuid"`` |
| `properties.id.type` | ``"string"`` |
| `properties.name` | `Object` |
| `properties.name.isRequired` | ``true`` |
| `properties.name.type` | ``"string"`` |

#### Defined in

[api/src/api/schemas/$BuildingModel.ts:4](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/schemas/$BuildingModel.ts#lines-4)
