[client](../../../index.md) / [Exports](../modules.md) / api/models/ResourceNotFoundError

# Module: api/models/ResourceNotFoundError

## Table of contents

### Type aliases

- [ResourceNotFoundError](api_models_ResourceNotFoundError.md#resourcenotfounderror)

## Type aliases

### ResourceNotFoundError

Ƭ **ResourceNotFoundError**: `Object`

A type representing the API error when resources not found

#### Type declaration

| Name | Type | Description |
| :------ | :------ | :------ |
| `error?` | `string` \| ``null`` | Get error identifier |

#### Defined in

[api/src/api/models/ResourceNotFoundError.ts:8](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/models/ResourceNotFoundError.ts#lines-8)
