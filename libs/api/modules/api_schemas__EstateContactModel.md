[client](../../../index.md) / [Exports](../modules.md) / api/schemas/$EstateContactModel

# Module: api/schemas/$EstateContactModel

## Table of contents

### Variables

- [$EstateContactModel](api_schemas__EstateContactModel.md#$estatecontactmodel)

## Variables

### $EstateContactModel

• **$EstateContactModel**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `properties` | `Object` |
| `properties.contactNo` | `Object` |
| `properties.contactNo.format` | ``"tel"`` |
| `properties.contactNo.isNullable` | ``true`` |
| `properties.contactNo.type` | ``"string"`` |
| `properties.email` | `Object` |
| `properties.email.format` | ``"email"`` |
| `properties.email.isRequired` | ``true`` |
| `properties.email.type` | ``"string"`` |
| `properties.title` | `Object` |
| `properties.title.isRequired` | ``true`` |
| `properties.title.type` | ``"string"`` |

#### Defined in

[api/src/api/schemas/$EstateContactModel.ts:4](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/schemas/$EstateContactModel.ts#lines-4)
