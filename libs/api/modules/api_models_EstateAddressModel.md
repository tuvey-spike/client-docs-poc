[client](../../../index.md) / [Exports](../modules.md) / api/models/EstateAddressModel

# Module: api/models/EstateAddressModel

## Table of contents

### Type aliases

- [EstateAddressModel](api_models_EstateAddressModel.md#estateaddressmodel)

## Type aliases

### EstateAddressModel

Ƭ **EstateAddressModel**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `cityTown` | `string` |
| `country?` | `string` \| ``null`` |
| `line1` | `string` |
| `line2?` | `string` \| ``null`` |
| `postCode` | `string` |

#### Defined in

[api/src/api/models/EstateAddressModel.ts:5](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/models/EstateAddressModel.ts#lines-5)
