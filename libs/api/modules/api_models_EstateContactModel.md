[client](../../../index.md) / [Exports](../modules.md) / api/models/EstateContactModel

# Module: api/models/EstateContactModel

## Table of contents

### Type aliases

- [EstateContactModel](api_models_EstateContactModel.md#estatecontactmodel)

## Type aliases

### EstateContactModel

Ƭ **EstateContactModel**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `contactNo?` | `string` \| ``null`` |
| `email` | `string` |
| `title` | `string` |

#### Defined in

[api/src/api/models/EstateContactModel.ts:5](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/models/EstateContactModel.ts#lines-5)
