[client](../../../index.md) / [Exports](../modules.md) / api/schemas/$UnitAddressModel

# Module: api/schemas/$UnitAddressModel

## Table of contents

### Variables

- [$UnitAddressModel](api_schemas__UnitAddressModel.md#$unitaddressmodel)

## Variables

### $UnitAddressModel

• **$UnitAddressModel**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `properties` | `Object` |
| `properties.cityTown` | `Object` |
| `properties.cityTown.isRequired` | ``true`` |
| `properties.cityTown.type` | ``"string"`` |
| `properties.country` | `Object` |
| `properties.country.isNullable` | ``true`` |
| `properties.country.type` | ``"string"`` |
| `properties.line1` | `Object` |
| `properties.line1.isRequired` | ``true`` |
| `properties.line1.type` | ``"string"`` |
| `properties.line2` | `Object` |
| `properties.line2.isNullable` | ``true`` |
| `properties.line2.type` | ``"string"`` |
| `properties.postCode` | `Object` |
| `properties.postCode.isRequired` | ``true`` |
| `properties.postCode.type` | ``"string"`` |

#### Defined in

[api/src/api/schemas/$UnitAddressModel.ts:4](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/schemas/$UnitAddressModel.ts#lines-4)
