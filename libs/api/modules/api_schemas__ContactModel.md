[client](../../../index.md) / [Exports](../modules.md) / api/schemas/$ContactModel

# Module: api/schemas/$ContactModel

## Table of contents

### Variables

- [$ContactModel](api_schemas__ContactModel.md#$contactmodel)

## Variables

### $ContactModel

• **$ContactModel**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `properties` | `Object` |
| `properties.contactNo` | `Object` |
| `properties.contactNo.isNullable` | ``true`` |
| `properties.contactNo.type` | ``"string"`` |
| `properties.email` | `Object` |
| `properties.email.format` | ``"email"`` |
| `properties.email.isRequired` | ``true`` |
| `properties.email.type` | ``"string"`` |
| `properties.firstName` | `Object` |
| `properties.firstName.isRequired` | ``true`` |
| `properties.firstName.type` | ``"string"`` |
| `properties.id` | `Object` |
| `properties.id.format` | ``"uuid"`` |
| `properties.id.type` | ``"string"`` |
| `properties.surname` | `Object` |
| `properties.surname.isRequired` | ``true`` |
| `properties.surname.type` | ``"string"`` |

#### Defined in

[api/src/api/schemas/$ContactModel.ts:4](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/schemas/$ContactModel.ts#lines-4)
