[client](../../../index.md) / [Exports](../modules.md) / api/core/ApiResult

# Module: api/core/ApiResult

## Table of contents

### Type aliases

- [ApiResult](api_core_ApiResult.md#apiresult)

## Type aliases

### ApiResult

Ƭ **ApiResult**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `body` | `any` |
| `headers` | `AxiosResponseHeaders` |
| `ok` | `boolean` |
| `status` | `number` |
| `statusText` | `string` |
| `url` | `string` |

#### Defined in

[api/src/api/core/ApiResult.ts:7](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/ApiResult.ts#lines-7)
