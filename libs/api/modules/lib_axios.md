[client](../../../index.md) / [Exports](../modules.md) / lib/axios

# Module: lib/axios

## Table of contents

### Variables

- [AxiosUniteHttp](lib_axios.md#axiosunitehttp)
- [uniteAxios](lib_axios.md#uniteaxios)

## Variables

### AxiosUniteHttp

• **AxiosUniteHttp**: [`IUniteHttp`](../interfaces/lib_http.IUniteHttp.md)

#### Defined in

[api/src/lib/axios.ts:60](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/lib/axios.ts#lines-60)

___

### uniteAxios

• **uniteAxios**: `AxiosInstance`

#### Defined in

[api/src/lib/axios.ts:6](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/lib/axios.ts#lines-6)
