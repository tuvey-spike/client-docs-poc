[client](../../../index.md) / [Exports](../modules.md) / interceptors/requestSuccess

# Module: interceptors/requestSuccess

## Table of contents

### Functions

- [requestSuccess](interceptors_requestSuccess.md#requestsuccess)

## Functions

### requestSuccess

▸ **requestSuccess**(`config`): `AxiosRequestConfig`<`any`\> \| `Promise`<`AxiosRequestConfig`<`any`\>\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `config` | `AxiosRequestConfig`<`any`\> |

#### Returns

`AxiosRequestConfig`<`any`\> \| `Promise`<`AxiosRequestConfig`<`any`\>\>

#### Defined in

[api/src/interceptors/requestSuccess.ts:3](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/interceptors/requestSuccess.ts#lines-3)
