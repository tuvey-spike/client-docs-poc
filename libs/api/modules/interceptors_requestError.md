[client](../../../index.md) / [Exports](../modules.md) / interceptors/requestError

# Module: interceptors/requestError

## Table of contents

### Functions

- [requestError](interceptors_requestError.md#requesterror)

## Functions

### requestError

▸ **requestError**(`error`): `any`

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | `any` |

#### Returns

`any`

#### Defined in

[api/src/interceptors/requestError.ts:1](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/interceptors/requestError.ts#lines-1)
