[client](../../../index.md) / [Exports](../modules.md) / api/core/UniteApiResult

# Module: api/core/UniteApiResult

## Table of contents

### Type aliases

- [UniteApiResult](api_core_UniteApiResult.md#uniteapiresult)

## Type aliases

### UniteApiResult

Ƭ **UniteApiResult**<`TData`\>: `Object`

Standard API Controller Response Data Model.
This is the only response model we will use in most cases.

#### Type parameters

| Name |
| :------ |
| `TData` |

#### Type declaration

| Name | Type | Description |
| :------ | :------ | :------ |
| `count` | `number` | Count of returned or affected items. |
| `errors?` | [`UniteApiError`](api_models_UniteApiError.md#uniteapierror)[] | Errors |
| `total` | `number` | Total of queried or affected items. |
| `value?` | `TData` | Payload. |

#### Defined in

[api/src/api/core/UniteApiResult.ts:7](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/UniteApiResult.ts#lines-7)
