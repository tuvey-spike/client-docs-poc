[client](../../../index.md) / [Exports](../modules.md) / api/core

# Module: api/core

## Table of contents

### References

- [ApiError](api_core.md#apierror)
- [ApiRequestOptions](api_core.md#apirequestoptions)
- [ApiResult](api_core.md#apiresult)
- [CancelablePromise](api_core.md#cancelablepromise)
- [OpenAPI](api_core.md#openapi)
- [UniteApiResult](api_core.md#uniteapiresult)

## References

### ApiError

Re-exports [ApiError](../classes/api_core_ApiError.ApiError.md)

___

### ApiRequestOptions

Re-exports [ApiRequestOptions](api_core_ApiRequestOptions.md#apirequestoptions)

___

### ApiResult

Re-exports [ApiResult](api_core_ApiResult.md#apiresult)

___

### CancelablePromise

Re-exports [CancelablePromise](../classes/api_core_CancelablePromise.CancelablePromise.md)

___

### OpenAPI

Re-exports [OpenAPI](api_core_OpenAPI.md#openapi)

___

### UniteApiResult

Re-exports [UniteApiResult](api_core_UniteApiResult.md#uniteapiresult)
