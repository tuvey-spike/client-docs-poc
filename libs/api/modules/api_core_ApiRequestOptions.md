[client](../../../index.md) / [Exports](../modules.md) / api/core/ApiRequestOptions

# Module: api/core/ApiRequestOptions

## Table of contents

### Type aliases

- [ApiRequestOptions](api_core_ApiRequestOptions.md#apirequestoptions)

## Type aliases

### ApiRequestOptions

Ƭ **ApiRequestOptions**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `body?` | `any` |
| `cookies?` | `Record`<`string`, `any`\> |
| `errors?` | `Record`<`number`, `string`\> |
| `formData?` | `Record`<`string`, `any`\> |
| `headers?` | `Record`<`string`, `any`\> |
| `mediaType?` | `string` |
| `method` | ``"GET"`` \| ``"PUT"`` \| ``"POST"`` \| ``"DELETE"`` \| ``"OPTIONS"`` \| ``"HEAD"`` \| ``"PATCH"`` |
| `path` | `string` |
| `query?` | `Record`<`string`, `any`\> |
| `responseHeader?` | `string` |

#### Defined in

[api/src/api/core/ApiRequestOptions.ts:4](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/ApiRequestOptions.ts#lines-4)
