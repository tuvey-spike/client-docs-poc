[client](../../../index.md) / [Exports](../modules.md) / api/models/UniteApiError

# Module: api/models/UniteApiError

## Table of contents

### Type aliases

- [UniteApiError](api_models_UniteApiError.md#uniteapierror)

## Type aliases

### UniteApiError

Ƭ **UniteApiError**: `Object`

Error class for all API responses

#### Type declaration

| Name | Type | Description |
| :------ | :------ | :------ |
| `code?` | `number` \| ``null`` | Error Code |
| `key?` | `string` \| ``null`` | Error Key |
| `message` | `string` | A message describing the error |

#### Defined in

[api/src/api/models/UniteApiError.ts:8](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/models/UniteApiError.ts#lines-8)
