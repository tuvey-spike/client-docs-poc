[client](../../../index.md) / [Exports](../modules.md) / interceptors/responseError

# Module: interceptors/responseError

## Table of contents

### Functions

- [responseError](interceptors_responseError.md#responseerror)

## Functions

### responseError

▸ **responseError**(`error`): `any`

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | `any` |

#### Returns

`any`

#### Defined in

[api/src/interceptors/responseError.ts:5](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/interceptors/responseError.ts#lines-5)
