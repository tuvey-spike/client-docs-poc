[client](../../../index.md) / [Exports](../modules.md) / api/core/CancelablePromise

# Module: api/core/CancelablePromise

## Table of contents

### Classes

- [CancelError](../classes/api_core_CancelablePromise.CancelError.md)
- [CancelablePromise](../classes/api_core_CancelablePromise.CancelablePromise.md)

### Interfaces

- [OnCancel](../interfaces/api_core_CancelablePromise.OnCancel.md)
