[client](../../../index.md) / [Exports](../modules.md) / index

# Module: index

## Table of contents

### References

- [$BuildingAddressModel](index.md#$buildingaddressmodel)
- [$BuildingContactModel](index.md#$buildingcontactmodel)
- [$BuildingModel](index.md#$buildingmodel)
- [$ContactModel](index.md#$contactmodel)
- [$EstateAddressModel](index.md#$estateaddressmodel)
- [$EstateContactModel](index.md#$estatecontactmodel)
- [$EstateModel](index.md#$estatemodel)
- [$FormValidationError](index.md#$formvalidationerror)
- [$ProblemDetails](index.md#$problemdetails)
- [$ResourceNotFoundError](index.md#$resourcenotfounderror)
- [$ServiceError](index.md#$serviceerror)
- [$UnitAddressModel](index.md#$unitaddressmodel)
- [$UnitModel](index.md#$unitmodel)
- [$UniteApiError](index.md#$uniteapierror)
- [ApiError](index.md#apierror)
- [ApiRequestOptions](index.md#apirequestoptions)
- [ApiResult](index.md#apiresult)
- [AxiosUniteHttp](index.md#axiosunitehttp)
- [BuildingAddressModel](index.md#buildingaddressmodel)
- [BuildingContactModel](index.md#buildingcontactmodel)
- [BuildingModel](index.md#buildingmodel)
- [BuildingV1Service](index.md#buildingv1service)
- [CancelablePromise](index.md#cancelablepromise)
- [ContactModel](index.md#contactmodel)
- [ContactV1Service](index.md#contactv1service)
- [EstateAddressModel](index.md#estateaddressmodel)
- [EstateContactModel](index.md#estatecontactmodel)
- [EstateModel](index.md#estatemodel)
- [EstateV1Service](index.md#estatev1service)
- [FormValidationError](index.md#formvalidationerror)
- [IUniteApiPage](index.md#iuniteapipage)
- [IUniteHttp](index.md#iunitehttp)
- [OpenAPI](index.md#openapi)
- [ProblemDetails](index.md#problemdetails)
- [ResourceNotFoundError](index.md#resourcenotfounderror)
- [ServiceError](index.md#serviceerror)
- [UnitAddressModel](index.md#unitaddressmodel)
- [UnitModel](index.md#unitmodel)
- [UnitV1Service](index.md#unitv1service)
- [UniteApiError](index.md#uniteapierror)
- [UniteApiResult](index.md#uniteapiresult)
- [UniteHttpError](index.md#unitehttperror)

## References

### $BuildingAddressModel

Re-exports [$BuildingAddressModel](api_schemas__BuildingAddressModel.md#$buildingaddressmodel)

___

### $BuildingContactModel

Re-exports [$BuildingContactModel](api_schemas__BuildingContactModel.md#$buildingcontactmodel)

___

### $BuildingModel

Re-exports [$BuildingModel](api_schemas__BuildingModel.md#$buildingmodel)

___

### $ContactModel

Re-exports [$ContactModel](api_schemas__ContactModel.md#$contactmodel)

___

### $EstateAddressModel

Re-exports [$EstateAddressModel](api_schemas__EstateAddressModel.md#$estateaddressmodel)

___

### $EstateContactModel

Re-exports [$EstateContactModel](api_schemas__EstateContactModel.md#$estatecontactmodel)

___

### $EstateModel

Re-exports [$EstateModel](api_schemas__EstateModel.md#$estatemodel)

___

### $FormValidationError

Re-exports [$FormValidationError](api_schemas__FormValidationError.md#$formvalidationerror)

___

### $ProblemDetails

Re-exports [$ProblemDetails](api_schemas__ProblemDetails.md#$problemdetails)

___

### $ResourceNotFoundError

Re-exports [$ResourceNotFoundError](api_schemas__ResourceNotFoundError.md#$resourcenotfounderror)

___

### $ServiceError

Re-exports [$ServiceError](api_schemas__ServiceError.md#$serviceerror)

___

### $UnitAddressModel

Re-exports [$UnitAddressModel](api_schemas__UnitAddressModel.md#$unitaddressmodel)

___

### $UnitModel

Re-exports [$UnitModel](api_schemas__UnitModel.md#$unitmodel)

___

### $UniteApiError

Re-exports [$UniteApiError](api_schemas__UniteApiError.md#$uniteapierror)

___

### ApiError

Re-exports [ApiError](../classes/api_core_ApiError.ApiError.md)

___

### ApiRequestOptions

Re-exports [ApiRequestOptions](api_core_ApiRequestOptions.md#apirequestoptions)

___

### ApiResult

Re-exports [ApiResult](api_core_ApiResult.md#apiresult)

___

### AxiosUniteHttp

Re-exports [AxiosUniteHttp](lib_axios.md#axiosunitehttp)

___

### BuildingAddressModel

Re-exports [BuildingAddressModel](api_models_BuildingAddressModel.md#buildingaddressmodel)

___

### BuildingContactModel

Re-exports [BuildingContactModel](api_models_BuildingContactModel.md#buildingcontactmodel)

___

### BuildingModel

Re-exports [BuildingModel](api_models_BuildingModel.md#buildingmodel)

___

### BuildingV1Service

Re-exports [BuildingV1Service](../classes/api_services_BuildingV1Service.BuildingV1Service.md)

___

### CancelablePromise

Re-exports [CancelablePromise](../classes/api_core_CancelablePromise.CancelablePromise.md)

___

### ContactModel

Re-exports [ContactModel](api_models_ContactModel.md#contactmodel)

___

### ContactV1Service

Re-exports [ContactV1Service](../classes/api_services_ContactV1Service.ContactV1Service.md)

___

### EstateAddressModel

Re-exports [EstateAddressModel](api_models_EstateAddressModel.md#estateaddressmodel)

___

### EstateContactModel

Re-exports [EstateContactModel](api_models_EstateContactModel.md#estatecontactmodel)

___

### EstateModel

Re-exports [EstateModel](api_models_EstateModel.md#estatemodel)

___

### EstateV1Service

Re-exports [EstateV1Service](../classes/api_services_EstateV1Service.EstateV1Service.md)

___

### FormValidationError

Re-exports [FormValidationError](api_models_FormValidationError.md#formvalidationerror)

___

### IUniteApiPage

Re-exports [IUniteApiPage](api_core_IUniteApiPage.md#iuniteapipage)

___

### IUniteHttp

Re-exports [IUniteHttp](../interfaces/lib_http.IUniteHttp.md)

___

### OpenAPI

Re-exports [OpenAPI](api_core_OpenAPI.md#openapi)

___

### ProblemDetails

Re-exports [ProblemDetails](api_models_ProblemDetails.md#problemdetails)

___

### ResourceNotFoundError

Re-exports [ResourceNotFoundError](api_models_ResourceNotFoundError.md#resourcenotfounderror)

___

### ServiceError

Re-exports [ServiceError](api_models_ServiceError.md#serviceerror)

___

### UnitAddressModel

Re-exports [UnitAddressModel](api_models_UnitAddressModel.md#unitaddressmodel)

___

### UnitModel

Re-exports [UnitModel](api_models_UnitModel.md#unitmodel)

___

### UnitV1Service

Re-exports [UnitV1Service](../classes/api_services_UnitV1Service.UnitV1Service.md)

___

### UniteApiError

Re-exports [UniteApiError](api_models_UniteApiError.md#uniteapierror)

___

### UniteApiResult

Re-exports [UniteApiResult](api_core_UniteApiResult.md#uniteapiresult)

___

### UniteHttpError

Re-exports [UniteHttpError](../classes/lib_http.UniteHttpError.md)
