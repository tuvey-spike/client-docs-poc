[client](../../../index.md) / [Exports](../modules.md) / api/models/UnitAddressModel

# Module: api/models/UnitAddressModel

## Table of contents

### Type aliases

- [UnitAddressModel](api_models_UnitAddressModel.md#unitaddressmodel)

## Type aliases

### UnitAddressModel

Ƭ **UnitAddressModel**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `cityTown` | `string` |
| `country?` | `string` \| ``null`` |
| `line1` | `string` |
| `line2?` | `string` \| ``null`` |
| `postCode` | `string` |

#### Defined in

[api/src/api/models/UnitAddressModel.ts:5](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/models/UnitAddressModel.ts#lines-5)
