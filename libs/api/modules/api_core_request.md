[client](../../../index.md) / [Exports](../modules.md) / api/core/request

# Module: api/core/request

## Table of contents

### Functions

- [request](api_core_request.md#request)

## Functions

### request

▸ **request**<`T`\>(`options`): [`CancelablePromise`](../classes/api_core_CancelablePromise.CancelablePromise.md)<`T`\>

Request using axios client

**`throws`** ApiError

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `options` | [`ApiRequestOptions`](api_core_ApiRequestOptions.md#apirequestoptions) | The request options from the the service |

#### Returns

[`CancelablePromise`](../classes/api_core_CancelablePromise.CancelablePromise.md)<`T`\>

CancelablePromise<T>

#### Defined in

[api/src/api/core/request.ts:238](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/request.ts#lines-238)
