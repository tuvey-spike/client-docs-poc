[client](../../../index.md) / [Exports](../modules.md) / api/models/ProblemDetails

# Module: api/models/ProblemDetails

## Table of contents

### Type aliases

- [ProblemDetails](api_models_ProblemDetails.md#problemdetails)

## Type aliases

### ProblemDetails

Ƭ **ProblemDetails**: `Record`<`string`, `any`\>

#### Defined in

[api/src/api/models/ProblemDetails.ts:5](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/models/ProblemDetails.ts#lines-5)
