[client](../../../index.md) / [Exports](../modules.md) / api

# Module: api

## Table of contents

### References

- [$BuildingAddressModel](api.md#$buildingaddressmodel)
- [$BuildingContactModel](api.md#$buildingcontactmodel)
- [$BuildingModel](api.md#$buildingmodel)
- [$ContactModel](api.md#$contactmodel)
- [$EstateAddressModel](api.md#$estateaddressmodel)
- [$EstateContactModel](api.md#$estatecontactmodel)
- [$EstateModel](api.md#$estatemodel)
- [$FormValidationError](api.md#$formvalidationerror)
- [$ProblemDetails](api.md#$problemdetails)
- [$ResourceNotFoundError](api.md#$resourcenotfounderror)
- [$ServiceError](api.md#$serviceerror)
- [$UnitAddressModel](api.md#$unitaddressmodel)
- [$UnitModel](api.md#$unitmodel)
- [$UniteApiError](api.md#$uniteapierror)
- [ApiError](api.md#apierror)
- [ApiRequestOptions](api.md#apirequestoptions)
- [ApiResult](api.md#apiresult)
- [BuildingAddressModel](api.md#buildingaddressmodel)
- [BuildingContactModel](api.md#buildingcontactmodel)
- [BuildingModel](api.md#buildingmodel)
- [BuildingV1Service](api.md#buildingv1service)
- [CancelablePromise](api.md#cancelablepromise)
- [ContactModel](api.md#contactmodel)
- [ContactV1Service](api.md#contactv1service)
- [EstateAddressModel](api.md#estateaddressmodel)
- [EstateContactModel](api.md#estatecontactmodel)
- [EstateModel](api.md#estatemodel)
- [EstateV1Service](api.md#estatev1service)
- [FormValidationError](api.md#formvalidationerror)
- [IUniteApiPage](api.md#iuniteapipage)
- [OpenAPI](api.md#openapi)
- [ProblemDetails](api.md#problemdetails)
- [ResourceNotFoundError](api.md#resourcenotfounderror)
- [ServiceError](api.md#serviceerror)
- [UnitAddressModel](api.md#unitaddressmodel)
- [UnitModel](api.md#unitmodel)
- [UnitV1Service](api.md#unitv1service)
- [UniteApiError](api.md#uniteapierror)
- [UniteApiResult](api.md#uniteapiresult)

## References

### $BuildingAddressModel

Re-exports [$BuildingAddressModel](api_schemas__BuildingAddressModel.md#$buildingaddressmodel)

___

### $BuildingContactModel

Re-exports [$BuildingContactModel](api_schemas__BuildingContactModel.md#$buildingcontactmodel)

___

### $BuildingModel

Re-exports [$BuildingModel](api_schemas__BuildingModel.md#$buildingmodel)

___

### $ContactModel

Re-exports [$ContactModel](api_schemas__ContactModel.md#$contactmodel)

___

### $EstateAddressModel

Re-exports [$EstateAddressModel](api_schemas__EstateAddressModel.md#$estateaddressmodel)

___

### $EstateContactModel

Re-exports [$EstateContactModel](api_schemas__EstateContactModel.md#$estatecontactmodel)

___

### $EstateModel

Re-exports [$EstateModel](api_schemas__EstateModel.md#$estatemodel)

___

### $FormValidationError

Re-exports [$FormValidationError](api_schemas__FormValidationError.md#$formvalidationerror)

___

### $ProblemDetails

Re-exports [$ProblemDetails](api_schemas__ProblemDetails.md#$problemdetails)

___

### $ResourceNotFoundError

Re-exports [$ResourceNotFoundError](api_schemas__ResourceNotFoundError.md#$resourcenotfounderror)

___

### $ServiceError

Re-exports [$ServiceError](api_schemas__ServiceError.md#$serviceerror)

___

### $UnitAddressModel

Re-exports [$UnitAddressModel](api_schemas__UnitAddressModel.md#$unitaddressmodel)

___

### $UnitModel

Re-exports [$UnitModel](api_schemas__UnitModel.md#$unitmodel)

___

### $UniteApiError

Re-exports [$UniteApiError](api_schemas__UniteApiError.md#$uniteapierror)

___

### ApiError

Re-exports [ApiError](../classes/api_core_ApiError.ApiError.md)

___

### ApiRequestOptions

Re-exports [ApiRequestOptions](api_core_ApiRequestOptions.md#apirequestoptions)

___

### ApiResult

Re-exports [ApiResult](api_core_ApiResult.md#apiresult)

___

### BuildingAddressModel

Re-exports [BuildingAddressModel](api_models_BuildingAddressModel.md#buildingaddressmodel)

___

### BuildingContactModel

Re-exports [BuildingContactModel](api_models_BuildingContactModel.md#buildingcontactmodel)

___

### BuildingModel

Re-exports [BuildingModel](api_models_BuildingModel.md#buildingmodel)

___

### BuildingV1Service

Re-exports [BuildingV1Service](../classes/api_services_BuildingV1Service.BuildingV1Service.md)

___

### CancelablePromise

Re-exports [CancelablePromise](../classes/api_core_CancelablePromise.CancelablePromise.md)

___

### ContactModel

Re-exports [ContactModel](api_models_ContactModel.md#contactmodel)

___

### ContactV1Service

Re-exports [ContactV1Service](../classes/api_services_ContactV1Service.ContactV1Service.md)

___

### EstateAddressModel

Re-exports [EstateAddressModel](api_models_EstateAddressModel.md#estateaddressmodel)

___

### EstateContactModel

Re-exports [EstateContactModel](api_models_EstateContactModel.md#estatecontactmodel)

___

### EstateModel

Re-exports [EstateModel](api_models_EstateModel.md#estatemodel)

___

### EstateV1Service

Re-exports [EstateV1Service](../classes/api_services_EstateV1Service.EstateV1Service.md)

___

### FormValidationError

Re-exports [FormValidationError](api_models_FormValidationError.md#formvalidationerror)

___

### IUniteApiPage

Re-exports [IUniteApiPage](api_core_IUniteApiPage.md#iuniteapipage)

___

### OpenAPI

Re-exports [OpenAPI](api_core_OpenAPI.md#openapi)

___

### ProblemDetails

Re-exports [ProblemDetails](api_models_ProblemDetails.md#problemdetails)

___

### ResourceNotFoundError

Re-exports [ResourceNotFoundError](api_models_ResourceNotFoundError.md#resourcenotfounderror)

___

### ServiceError

Re-exports [ServiceError](api_models_ServiceError.md#serviceerror)

___

### UnitAddressModel

Re-exports [UnitAddressModel](api_models_UnitAddressModel.md#unitaddressmodel)

___

### UnitModel

Re-exports [UnitModel](api_models_UnitModel.md#unitmodel)

___

### UnitV1Service

Re-exports [UnitV1Service](../classes/api_services_UnitV1Service.UnitV1Service.md)

___

### UniteApiError

Re-exports [UniteApiError](api_models_UniteApiError.md#uniteapierror)

___

### UniteApiResult

Re-exports [UniteApiResult](api_core_UniteApiResult.md#uniteapiresult)
