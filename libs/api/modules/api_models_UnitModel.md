[client](../../../index.md) / [Exports](../modules.md) / api/models/UnitModel

# Module: api/models/UnitModel

## Table of contents

### Type aliases

- [UnitModel](api_models_UnitModel.md#unitmodel)

## Type aliases

### UnitModel

Ƭ **UnitModel**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `address` | [`UnitAddressModel`](api_models_UnitAddressModel.md#unitaddressmodel) |
| `bathrooms` | `number` |
| `beds` | `number` |
| `block?` | `string` \| ``null`` |
| `floor` | `number` |
| `id?` | `string` |
| `type` | `string` |
| `unitNumber` | `string` |

#### Defined in

[api/src/api/models/UnitModel.ts:7](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/models/UnitModel.ts#lines-7)
