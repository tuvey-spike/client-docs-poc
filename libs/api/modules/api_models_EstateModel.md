[client](../../../index.md) / [Exports](../modules.md) / api/models/EstateModel

# Module: api/models/EstateModel

## Table of contents

### Type aliases

- [EstateModel](api_models_EstateModel.md#estatemodel)

## Type aliases

### EstateModel

Ƭ **EstateModel**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `address` | [`EstateAddressModel`](api_models_EstateAddressModel.md#estateaddressmodel) |
| `contacts` | [`EstateContactModel`](api_models_EstateContactModel.md#estatecontactmodel)[] |
| `id?` | `string` |
| `name` | `string` |
| `region` | `string` |

#### Defined in

[api/src/api/models/EstateModel.ts:8](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/models/EstateModel.ts#lines-8)
