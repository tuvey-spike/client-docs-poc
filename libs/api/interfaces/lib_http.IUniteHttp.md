[client](../../../index.md) / [Exports](../modules.md) / [lib/http](../modules/lib_http.md) / IUniteHttp

# Interface: IUniteHttp

[lib/http](../modules/lib_http.md).IUniteHttp

## Table of contents

### Methods

- [get](lib_http.IUniteHttp.md#get)
- [post](lib_http.IUniteHttp.md#post)
- [put](lib_http.IUniteHttp.md#put)

## Methods

### get

▸ **get**<`TResult`\>(`url`, `query?`): `Promise`<`Option`<`TResult`, [`UniteHttpError`](../classes/lib_http.UniteHttpError.md)\>\>

#### Type parameters

| Name |
| :------ |
| `TResult` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `url` | `string` |
| `query?` | `Object` |

#### Returns

`Promise`<`Option`<`TResult`, [`UniteHttpError`](../classes/lib_http.UniteHttpError.md)\>\>

#### Defined in

[api/src/lib/http.ts:14](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/lib/http.ts#lines-14)

___

### post

▸ **post**<`TPostData`, `TResult`\>(`url`, `data`): `Promise`<`Option`<`TResult`, [`UniteHttpError`](../classes/lib_http.UniteHttpError.md)\>\>

#### Type parameters

| Name |
| :------ |
| `TPostData` |
| `TResult` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `url` | `string` |
| `data` | `TPostData` |

#### Returns

`Promise`<`Option`<`TResult`, [`UniteHttpError`](../classes/lib_http.UniteHttpError.md)\>\>

#### Defined in

[api/src/lib/http.ts:15](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/lib/http.ts#lines-15)

___

### put

▸ **put**<`TPutData`, `TResult`\>(`url`, `data`): `Promise`<`Option`<`TResult`, [`UniteHttpError`](../classes/lib_http.UniteHttpError.md)\>\>

#### Type parameters

| Name |
| :------ |
| `TPutData` |
| `TResult` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `url` | `string` |
| `data` | `TPutData` |

#### Returns

`Promise`<`Option`<`TResult`, [`UniteHttpError`](../classes/lib_http.UniteHttpError.md)\>\>

#### Defined in

[api/src/lib/http.ts:16](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/lib/http.ts#lines-16)
