[client](../../../index.md) / [Exports](../modules.md) / [api/core/CancelablePromise](../modules/api_core_CancelablePromise.md) / OnCancel

# Interface: OnCancel

[api/core/CancelablePromise](../modules/api_core_CancelablePromise.md).OnCancel

## Callable

### OnCancel

▸ **OnCancel**(`cancelHandler`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `cancelHandler` | () => `void` |

#### Returns

`void`

#### Defined in

[api/src/api/core/CancelablePromise.ts:20](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/CancelablePromise.ts#lines-20)

## Table of contents

### Properties

- [isCancelled](api_core_CancelablePromise.OnCancel.md#iscancelled)
- [isPending](api_core_CancelablePromise.OnCancel.md#ispending)

## Properties

### isCancelled

• `Readonly` **isCancelled**: `boolean`

#### Defined in

[api/src/api/core/CancelablePromise.ts:18](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/CancelablePromise.ts#lines-18)

___

### isPending

• `Readonly` **isPending**: `boolean`

#### Defined in

[api/src/api/core/CancelablePromise.ts:17](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/CancelablePromise.ts#lines-17)
