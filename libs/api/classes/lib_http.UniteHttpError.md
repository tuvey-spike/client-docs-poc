[client](../../../index.md) / [Exports](../modules.md) / [lib/http](../modules/lib_http.md) / UniteHttpError

# Class: UniteHttpError

[lib/http](../modules/lib_http.md).UniteHttpError

## Hierarchy

- `OptionError`

  ↳ **`UniteHttpError`**

## Table of contents

### Constructors

- [constructor](lib_http.UniteHttpError.md#constructor)

### Properties

- [code](lib_http.UniteHttpError.md#code)
- [stack](lib_http.UniteHttpError.md#stack)
- [status](lib_http.UniteHttpError.md#status)

### Accessors

- [Message](lib_http.UniteHttpError.md#message)

## Constructors

### constructor

• **new UniteHttpError**(`status`, `code`, `stack`, `message?`)

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `status` | `undefined` \| [`HttpStatusCode`](../enums/lib_http.HttpStatusCode.md) | `undefined` |
| `code` | `undefined` \| `string` | `undefined` |
| `stack` | `undefined` \| `string` | `undefined` |
| `message` | `string` | `""` |

#### Overrides

OptionError.constructor

#### Defined in

[api/src/lib/http.ts:4](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/lib/http.ts#lines-4)

## Properties

### code

• **code**: `undefined` \| `string`

___

### stack

• **stack**: `undefined` \| `string`

___

### status

• **status**: `undefined` \| [`HttpStatusCode`](../enums/lib_http.HttpStatusCode.md)

## Accessors

### Message

• `get` **Message**(): `string`

#### Returns

`string`

#### Inherited from

OptionError.Message

#### Defined in

[options/src/lib/options.ts:93](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/options/src/lib/options.ts#lines-93)
