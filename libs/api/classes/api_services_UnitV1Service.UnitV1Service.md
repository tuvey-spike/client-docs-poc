[client](../../../index.md) / [Exports](../modules.md) / [api/services/UnitV1Service](../modules/api_services_UnitV1Service.md) / UnitV1Service

# Class: UnitV1Service

[api/services/UnitV1Service](../modules/api_services_UnitV1Service.md).UnitV1Service

## Table of contents

### Constructors

- [constructor](api_services_UnitV1Service.UnitV1Service.md#constructor)

### Methods

- [deleteUnitV1](api_services_UnitV1Service.UnitV1Service.md#deleteunitv1)
- [getUnitV1](api_services_UnitV1Service.UnitV1Service.md#getunitv1)
- [getUnitV11](api_services_UnitV1Service.UnitV1Service.md#getunitv11)
- [postUnitV1](api_services_UnitV1Service.UnitV1Service.md#postunitv1)
- [putUnitV1](api_services_UnitV1Service.UnitV1Service.md#putunitv1)

## Constructors

### constructor

• **new UnitV1Service**()

## Methods

### deleteUnitV1

▸ `Static` **deleteUnitV1**(`__namedParameters`): [`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<`Boolean`\>\>

Delete a single unit

**`throws`** ApiError

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `__namedParameters` | `Object` | - |
| `__namedParameters.id` | `string` | Id of the unit to delete |

#### Returns

[`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<`Boolean`\>\>

UniteApiResult<Boolean> Success

#### Defined in

[api/src/api/services/UnitV1Service.ts:119](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/services/UnitV1Service.ts#lines-119)

___

### getUnitV1

▸ `Static` **getUnitV1**(`__namedParameters`): [`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<[`IUniteApiPage`](../modules/api_core_IUniteApiPage.md#iuniteapipage)<[`UnitModel`](../modules/api_models_UnitModel.md#unitmodel)\>\>\>

Get a list of units with simple paging. There is intentionally no search functionality here because this is just a CRUD controller.

**`throws`** ApiError

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `__namedParameters` | `Object` | - |
| `__namedParameters.count?` | `number` | Page size |
| `__namedParameters.start?` | `number` | Page start index |

#### Returns

[`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<[`IUniteApiPage`](../modules/api_core_IUniteApiPage.md#iuniteapipage)<[`UnitModel`](../modules/api_models_UnitModel.md#unitmodel)\>\>\>

UniteApiResult<IUniteApiPage<UnitModel>> Success

#### Defined in

[api/src/api/services/UnitV1Service.ts:43](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/services/UnitV1Service.ts#lines-43)

___

### getUnitV11

▸ `Static` **getUnitV11**(`__namedParameters`): [`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<[`UnitModel`](../modules/api_models_UnitModel.md#unitmodel)\>\>

Get a single unit

**`throws`** ApiError

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `__namedParameters` | `Object` | - |
| `__namedParameters.id` | `string` | Id of an existing unit |

#### Returns

[`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<[`UnitModel`](../modules/api_models_UnitModel.md#unitmodel)\>\>

UniteApiResult<UnitModel> Success

#### Defined in

[api/src/api/services/UnitV1Service.ts:71](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/services/UnitV1Service.ts#lines-71)

___

### postUnitV1

▸ `Static` **postUnitV1**(`__namedParameters`): [`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<`any`\>

Create a unit

**`throws`** ApiError

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `__namedParameters` | `Object` | - |
| `__namedParameters.requestBody?` | [`UnitModel`](../modules/api_models_UnitModel.md#unitmodel) | The POC data transfer object for a unit, which must adhere in terms of fields and rules (in the form of data annotations) to the following application model design. https://spikeglobal.atlassian.net/wiki/spaces/SU2/pages/2052784135/U2+Admin+Create+New+Unit#Unit-Form |

#### Returns

[`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<`any`\>

ProblemDetails Error

#### Defined in

[api/src/api/services/UnitV1Service.ts:19](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/services/UnitV1Service.ts#lines-19)

___

### putUnitV1

▸ `Static` **putUnitV1**(`__namedParameters`): [`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<[`UnitModel`](../modules/api_models_UnitModel.md#unitmodel)\>\>

Update a unit

**`throws`** ApiError

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `__namedParameters` | `Object` | - |
| `__namedParameters.id` | `string` | Id of the unit to update |
| `__namedParameters.requestBody?` | [`UnitModel`](../modules/api_models_UnitModel.md#unitmodel) | The POC data transfer object for a unit containing the new state of the existing unit. See https://spikeglobal.atlassian.net/wiki/spaces/SU2/pages/2052784135/U2+Admin+Create+New+Unit#Unit-Form |

#### Returns

[`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<[`UnitModel`](../modules/api_models_UnitModel.md#unitmodel)\>\>

UniteApiResult<UnitModel> Success

#### Defined in

[api/src/api/services/UnitV1Service.ts:92](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/services/UnitV1Service.ts#lines-92)
