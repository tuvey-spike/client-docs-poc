[client](../../../index.md) / [Exports](../modules.md) / [api/services/ContactV1Service](../modules/api_services_ContactV1Service.md) / ContactV1Service

# Class: ContactV1Service

[api/services/ContactV1Service](../modules/api_services_ContactV1Service.md).ContactV1Service

## Table of contents

### Constructors

- [constructor](api_services_ContactV1Service.ContactV1Service.md#constructor)

### Methods

- [deleteContactV1](api_services_ContactV1Service.ContactV1Service.md#deletecontactv1)
- [getContactV1](api_services_ContactV1Service.ContactV1Service.md#getcontactv1)
- [getContactV11](api_services_ContactV1Service.ContactV1Service.md#getcontactv11)
- [postContactV1](api_services_ContactV1Service.ContactV1Service.md#postcontactv1)
- [putContactV1](api_services_ContactV1Service.ContactV1Service.md#putcontactv1)

## Constructors

### constructor

• **new ContactV1Service**()

## Methods

### deleteContactV1

▸ `Static` **deleteContactV1**(`__namedParameters`): [`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<`Boolean`\>\>

Delete a single contact

**`throws`** ApiError

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `__namedParameters` | `Object` | - |
| `__namedParameters.id` | `string` | Id of the contact to delete |

#### Returns

[`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<`Boolean`\>\>

UniteApiResult<Boolean> Success

#### Defined in

[api/src/api/services/ContactV1Service.ts:119](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/services/ContactV1Service.ts#lines-119)

___

### getContactV1

▸ `Static` **getContactV1**(`__namedParameters`): [`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<[`IUniteApiPage`](../modules/api_core_IUniteApiPage.md#iuniteapipage)<[`ContactModel`](../modules/api_models_ContactModel.md#contactmodel)\>\>\>

Get a list of contacts with simple paging. There is intentionally no search functionality here because this is just a CRUD controller.

**`throws`** ApiError

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `__namedParameters` | `Object` | - |
| `__namedParameters.count?` | `number` | Page size |
| `__namedParameters.start?` | `number` | Page start index |

#### Returns

[`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<[`IUniteApiPage`](../modules/api_core_IUniteApiPage.md#iuniteapipage)<[`ContactModel`](../modules/api_models_ContactModel.md#contactmodel)\>\>\>

UniteApiResult<IUniteApiPage<ContactModel>> Success

#### Defined in

[api/src/api/services/ContactV1Service.ts:43](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/services/ContactV1Service.ts#lines-43)

___

### getContactV11

▸ `Static` **getContactV11**(`__namedParameters`): [`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<[`ContactModel`](../modules/api_models_ContactModel.md#contactmodel)\>\>

Get a single contact

**`throws`** ApiError

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `__namedParameters` | `Object` | - |
| `__namedParameters.id` | `string` | Id of an existing contact |

#### Returns

[`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<[`ContactModel`](../modules/api_models_ContactModel.md#contactmodel)\>\>

UniteApiResult<ContactModel> Success

#### Defined in

[api/src/api/services/ContactV1Service.ts:71](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/services/ContactV1Service.ts#lines-71)

___

### postContactV1

▸ `Static` **postContactV1**(`__namedParameters`): [`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<`any`\>

Create a contact

**`throws`** ApiError

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `__namedParameters` | `Object` | - |
| `__namedParameters.requestBody?` | [`ContactModel`](../modules/api_models_ContactModel.md#contactmodel) | The POC data transfer object for a contact, which must adhere in terms of fields and rules (in the form of data annotations) to the following application model design. https://spikeglobal.atlassian.net/wiki/spaces/SU2/pages/2049900758/U2+Admin+Create+New+Contact#Contact-Form |

#### Returns

[`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<`any`\>

ProblemDetails Error

#### Defined in

[api/src/api/services/ContactV1Service.ts:19](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/services/ContactV1Service.ts#lines-19)

___

### putContactV1

▸ `Static` **putContactV1**(`__namedParameters`): [`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<[`ContactModel`](../modules/api_models_ContactModel.md#contactmodel)\>\>

Update a contact

**`throws`** ApiError

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `__namedParameters` | `Object` | - |
| `__namedParameters.id` | `string` | Id of the contact to update |
| `__namedParameters.requestBody?` | [`ContactModel`](../modules/api_models_ContactModel.md#contactmodel) | The POC data transfer object for an contact containing the new state of the existing contact. See https://spikeglobal.atlassian.net/wiki/spaces/SU2/pages/2049900758/U2+Admin+Create+New+Contact#Contact-Form |

#### Returns

[`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<[`ContactModel`](../modules/api_models_ContactModel.md#contactmodel)\>\>

UniteApiResult<ContactModel> Success

#### Defined in

[api/src/api/services/ContactV1Service.ts:92](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/services/ContactV1Service.ts#lines-92)
