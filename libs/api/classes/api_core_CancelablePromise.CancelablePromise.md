[client](../../../index.md) / [Exports](../modules.md) / [api/core/CancelablePromise](../modules/api_core_CancelablePromise.md) / CancelablePromise

# Class: CancelablePromise<T\>

[api/core/CancelablePromise](../modules/api_core_CancelablePromise.md).CancelablePromise

## Type parameters

| Name |
| :------ |
| `T` |

## Implements

- `Promise`<`T`\>

## Table of contents

### Constructors

- [constructor](api_core_CancelablePromise.CancelablePromise.md#constructor)

### Properties

- [#cancelHandlers](api_core_CancelablePromise.CancelablePromise.md##cancelhandlers)
- [#isCancelled](api_core_CancelablePromise.CancelablePromise.md##iscancelled)
- [#isPending](api_core_CancelablePromise.CancelablePromise.md##ispending)
- [#promise](api_core_CancelablePromise.CancelablePromise.md##promise)
- [#reject](api_core_CancelablePromise.CancelablePromise.md##reject)
- [#resolve](api_core_CancelablePromise.CancelablePromise.md##resolve)
- [[toStringTag]](api_core_CancelablePromise.CancelablePromise.md#[tostringtag])

### Accessors

- [isCancelled](api_core_CancelablePromise.CancelablePromise.md#iscancelled)

### Methods

- [cancel](api_core_CancelablePromise.CancelablePromise.md#cancel)
- [catch](api_core_CancelablePromise.CancelablePromise.md#catch)
- [finally](api_core_CancelablePromise.CancelablePromise.md#finally)
- [then](api_core_CancelablePromise.CancelablePromise.md#then)

## Constructors

### constructor

• **new CancelablePromise**<`T`\>(`executor`)

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `executor` | (`resolve`: (`value`: `T` \| `PromiseLike`<`T`\>) => `void`, `reject`: (`reason?`: `any`) => `void`, `onCancel`: [`OnCancel`](../interfaces/api_core_CancelablePromise.OnCancel.md)) => `void` |

#### Defined in

[api/src/api/core/CancelablePromise.ts:33](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/CancelablePromise.ts#lines-33)

## Properties

### #cancelHandlers

• `Private` `Readonly` **#cancelHandlers**: () => `void`[]

#### Defined in

[api/src/api/core/CancelablePromise.ts:28](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/CancelablePromise.ts#lines-28)

___

### #isCancelled

• `Private` **#isCancelled**: `boolean`

#### Defined in

[api/src/api/core/CancelablePromise.ts:27](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/CancelablePromise.ts#lines-27)

___

### #isPending

• `Private` **#isPending**: `boolean`

#### Defined in

[api/src/api/core/CancelablePromise.ts:26](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/CancelablePromise.ts#lines-26)

___

### #promise

• `Private` `Readonly` **#promise**: `Promise`<`T`\>

#### Defined in

[api/src/api/core/CancelablePromise.ts:29](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/CancelablePromise.ts#lines-29)

___

### #reject

• `Private` `Optional` **#reject**: (`reason?`: `any`) => `void`

#### Type declaration

▸ (`reason?`): `void`

##### Parameters

| Name | Type |
| :------ | :------ |
| `reason?` | `any` |

##### Returns

`void`

#### Defined in

[api/src/api/core/CancelablePromise.ts:31](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/CancelablePromise.ts#lines-31)

___

### #resolve

• `Private` `Optional` **#resolve**: (`value`: `T` \| `PromiseLike`<`T`\>) => `void`

#### Type declaration

▸ (`value`): `void`

##### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `T` \| `PromiseLike`<`T`\> |

##### Returns

`void`

#### Defined in

[api/src/api/core/CancelablePromise.ts:30](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/CancelablePromise.ts#lines-30)

___

### [toStringTag]

• `Readonly` **[toStringTag]**: `string`

#### Implementation of

Promise.\_\_@toStringTag@737

#### Defined in

[api/src/api/core/CancelablePromise.ts:24](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/CancelablePromise.ts#lines-24)

## Accessors

### isCancelled

• `get` **isCancelled**(): `boolean`

#### Returns

`boolean`

#### Defined in

[api/src/api/core/CancelablePromise.ts:111](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/CancelablePromise.ts#lines-111)

## Methods

### cancel

▸ **cancel**(): `void`

#### Returns

`void`

#### Defined in

[api/src/api/core/CancelablePromise.ts:94](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/CancelablePromise.ts#lines-94)

___

### catch

▸ **catch**<`TResult`\>(`onRejected?`): `Promise`<`T` \| `TResult`\>

#### Type parameters

| Name | Type |
| :------ | :------ |
| `TResult` | `never` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `onRejected?` | ``null`` \| (`reason`: `any`) => `TResult` \| `PromiseLike`<`TResult`\> |

#### Returns

`Promise`<`T` \| `TResult`\>

#### Implementation of

Promise.catch

#### Defined in

[api/src/api/core/CancelablePromise.ts:84](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/CancelablePromise.ts#lines-84)

___

### finally

▸ **finally**(`onFinally?`): `Promise`<`T`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `onFinally?` | ``null`` \| () => `void` |

#### Returns

`Promise`<`T`\>

#### Implementation of

Promise.finally

#### Defined in

[api/src/api/core/CancelablePromise.ts:90](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/CancelablePromise.ts#lines-90)

___

### then

▸ **then**<`TResult1`, `TResult2`\>(`onFulfilled?`, `onRejected?`): `Promise`<`TResult1` \| `TResult2`\>

#### Type parameters

| Name | Type |
| :------ | :------ |
| `TResult1` | `T` |
| `TResult2` | `never` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `onFulfilled?` | ``null`` \| (`value`: `T`) => `TResult1` \| `PromiseLike`<`TResult1`\> |
| `onRejected?` | ``null`` \| (`reason`: `any`) => `TResult2` \| `PromiseLike`<`TResult2`\> |

#### Returns

`Promise`<`TResult1` \| `TResult2`\>

#### Implementation of

Promise.then

#### Defined in

[api/src/api/core/CancelablePromise.ts:77](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/CancelablePromise.ts#lines-77)
