[client](../../../index.md) / [Exports](../modules.md) / [api/core/CancelablePromise](../modules/api_core_CancelablePromise.md) / CancelError

# Class: CancelError

[api/core/CancelablePromise](../modules/api_core_CancelablePromise.md).CancelError

## Hierarchy

- `Error`

  ↳ **`CancelError`**

## Table of contents

### Constructors

- [constructor](api_core_CancelablePromise.CancelError.md#constructor)

### Accessors

- [isCancelled](api_core_CancelablePromise.CancelError.md#iscancelled)

## Constructors

### constructor

• **new CancelError**(`reason?`)

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `reason` | `string` | `'Promise was canceled'` |

#### Overrides

Error.constructor

#### Defined in

[api/src/api/core/CancelablePromise.ts:6](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/CancelablePromise.ts#lines-6)

## Accessors

### isCancelled

• `get` **isCancelled**(): `boolean`

#### Returns

`boolean`

#### Defined in

[api/src/api/core/CancelablePromise.ts:11](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/CancelablePromise.ts#lines-11)
