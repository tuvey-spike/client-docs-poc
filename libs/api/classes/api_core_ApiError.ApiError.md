[client](../../../index.md) / [Exports](../modules.md) / [api/core/ApiError](../modules/api_core_ApiError.md) / ApiError

# Class: ApiError

[api/core/ApiError](../modules/api_core_ApiError.md).ApiError

## Hierarchy

- `Error`

  ↳ **`ApiError`**

## Table of contents

### Constructors

- [constructor](api_core_ApiError.ApiError.md#constructor)

### Properties

- [body](api_core_ApiError.ApiError.md#body)
- [status](api_core_ApiError.ApiError.md#status)
- [statusText](api_core_ApiError.ApiError.md#statustext)
- [url](api_core_ApiError.ApiError.md#url)

## Constructors

### constructor

• **new ApiError**(`response`, `message`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `response` | [`ApiResult`](../modules/api_core_ApiResult.md#apiresult) |
| `message` | `string` |

#### Overrides

Error.constructor

#### Defined in

[api/src/api/core/ApiError.ts:12](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/ApiError.ts#lines-12)

## Properties

### body

• `Readonly` **body**: `any`

#### Defined in

[api/src/api/core/ApiError.ts:10](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/ApiError.ts#lines-10)

___

### status

• `Readonly` **status**: `number`

#### Defined in

[api/src/api/core/ApiError.ts:8](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/ApiError.ts#lines-8)

___

### statusText

• `Readonly` **statusText**: `string`

#### Defined in

[api/src/api/core/ApiError.ts:9](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/ApiError.ts#lines-9)

___

### url

• `Readonly` **url**: `string`

#### Defined in

[api/src/api/core/ApiError.ts:7](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/core/ApiError.ts#lines-7)
