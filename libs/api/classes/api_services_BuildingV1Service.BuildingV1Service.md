[client](../../../index.md) / [Exports](../modules.md) / [api/services/BuildingV1Service](../modules/api_services_BuildingV1Service.md) / BuildingV1Service

# Class: BuildingV1Service

[api/services/BuildingV1Service](../modules/api_services_BuildingV1Service.md).BuildingV1Service

## Table of contents

### Constructors

- [constructor](api_services_BuildingV1Service.BuildingV1Service.md#constructor)

### Methods

- [deleteBuildingV1](api_services_BuildingV1Service.BuildingV1Service.md#deletebuildingv1)
- [getBuildingV1](api_services_BuildingV1Service.BuildingV1Service.md#getbuildingv1)
- [getBuildingV11](api_services_BuildingV1Service.BuildingV1Service.md#getbuildingv11)
- [postBuildingV1](api_services_BuildingV1Service.BuildingV1Service.md#postbuildingv1)
- [putBuildingV1](api_services_BuildingV1Service.BuildingV1Service.md#putbuildingv1)

## Constructors

### constructor

• **new BuildingV1Service**()

## Methods

### deleteBuildingV1

▸ `Static` **deleteBuildingV1**(`__namedParameters`): [`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<`Boolean`\>\>

Delete a single building

**`throws`** ApiError

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `__namedParameters` | `Object` | - |
| `__namedParameters.id` | `string` | Id of the building to delete |

#### Returns

[`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<`Boolean`\>\>

UniteApiResult<Boolean> Success

#### Defined in

[api/src/api/services/BuildingV1Service.ts:119](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/services/BuildingV1Service.ts#lines-119)

___

### getBuildingV1

▸ `Static` **getBuildingV1**(`__namedParameters`): [`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<[`IUniteApiPage`](../modules/api_core_IUniteApiPage.md#iuniteapipage)<[`BuildingModel`](../modules/api_models_BuildingModel.md#buildingmodel)\>\>\>

Get a list of buildings with simple paging. There is intentionally no search functionality here because this is just a CRUD controller.

**`throws`** ApiError

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `__namedParameters` | `Object` | - |
| `__namedParameters.count?` | `number` | Page size |
| `__namedParameters.start?` | `number` | Page start index |

#### Returns

[`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<[`IUniteApiPage`](../modules/api_core_IUniteApiPage.md#iuniteapipage)<[`BuildingModel`](../modules/api_models_BuildingModel.md#buildingmodel)\>\>\>

UniteApiResult<IUniteApiPage<BuildingModel>> Success

#### Defined in

[api/src/api/services/BuildingV1Service.ts:43](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/services/BuildingV1Service.ts#lines-43)

___

### getBuildingV11

▸ `Static` **getBuildingV11**(`__namedParameters`): [`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<[`BuildingModel`](../modules/api_models_BuildingModel.md#buildingmodel)\>\>

Get a single building

**`throws`** ApiError

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `__namedParameters` | `Object` | - |
| `__namedParameters.id` | `string` | Id of an existing building |

#### Returns

[`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<[`BuildingModel`](../modules/api_models_BuildingModel.md#buildingmodel)\>\>

UniteApiResult<BuildingModel> Success

#### Defined in

[api/src/api/services/BuildingV1Service.ts:71](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/services/BuildingV1Service.ts#lines-71)

___

### postBuildingV1

▸ `Static` **postBuildingV1**(`__namedParameters`): [`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<`any`\>

Create a building

**`throws`** ApiError

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `__namedParameters` | `Object` | - |
| `__namedParameters.requestBody?` | [`BuildingModel`](../modules/api_models_BuildingModel.md#buildingmodel) | The POC data transfer object for a building, which must adhere in terms of fields and rules (in the form of data annotations) to the following application model design. https://spikeglobal.atlassian.net/wiki/spaces/SU2/pages/2024636417/U2+Admin+Create+New+Building#Building-Form |

#### Returns

[`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<`any`\>

ProblemDetails Error

#### Defined in

[api/src/api/services/BuildingV1Service.ts:19](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/services/BuildingV1Service.ts#lines-19)

___

### putBuildingV1

▸ `Static` **putBuildingV1**(`__namedParameters`): [`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<[`BuildingModel`](../modules/api_models_BuildingModel.md#buildingmodel)\>\>

Update a building

**`throws`** ApiError

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `__namedParameters` | `Object` | - |
| `__namedParameters.id` | `string` | Id of the building to update |
| `__namedParameters.requestBody?` | [`BuildingModel`](../modules/api_models_BuildingModel.md#buildingmodel) | The POC data transfer object for a unit containing the new state of the existing building. See https://spikeglobal.atlassian.net/wiki/spaces/SU2/pages/2024636417/U2+Admin+Create+New+Building#Building-Form |

#### Returns

[`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<[`BuildingModel`](../modules/api_models_BuildingModel.md#buildingmodel)\>\>

UniteApiResult<BuildingModel> Success

#### Defined in

[api/src/api/services/BuildingV1Service.ts:92](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/services/BuildingV1Service.ts#lines-92)
