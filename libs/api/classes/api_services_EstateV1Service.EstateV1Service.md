[client](../../../index.md) / [Exports](../modules.md) / [api/services/EstateV1Service](../modules/api_services_EstateV1Service.md) / EstateV1Service

# Class: EstateV1Service

[api/services/EstateV1Service](../modules/api_services_EstateV1Service.md).EstateV1Service

## Table of contents

### Constructors

- [constructor](api_services_EstateV1Service.EstateV1Service.md#constructor)

### Methods

- [deleteEstateV1](api_services_EstateV1Service.EstateV1Service.md#deleteestatev1)
- [getEstateV1](api_services_EstateV1Service.EstateV1Service.md#getestatev1)
- [getEstateV11](api_services_EstateV1Service.EstateV1Service.md#getestatev11)
- [postEstateV1](api_services_EstateV1Service.EstateV1Service.md#postestatev1)
- [putEstateV1](api_services_EstateV1Service.EstateV1Service.md#putestatev1)

## Constructors

### constructor

• **new EstateV1Service**()

## Methods

### deleteEstateV1

▸ `Static` **deleteEstateV1**(`__namedParameters`): [`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<`Boolean`\>\>

Delete a single estate

**`throws`** ApiError

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `__namedParameters` | `Object` | - |
| `__namedParameters.id` | `string` | Id of the estate to delete |

#### Returns

[`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<`Boolean`\>\>

UniteApiResult<Boolean> Success

#### Defined in

[api/src/api/services/EstateV1Service.ts:119](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/services/EstateV1Service.ts#lines-119)

___

### getEstateV1

▸ `Static` **getEstateV1**(`__namedParameters`): [`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<[`IUniteApiPage`](../modules/api_core_IUniteApiPage.md#iuniteapipage)<[`EstateModel`](../modules/api_models_EstateModel.md#estatemodel)\>\>\>

Get a list of estates with simple paging. There is intentionally no search functionality here because this is just a CRUD controller.

**`throws`** ApiError

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `__namedParameters` | `Object` | - |
| `__namedParameters.count?` | `number` | Page size |
| `__namedParameters.start?` | `number` | Page start index |

#### Returns

[`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<[`IUniteApiPage`](../modules/api_core_IUniteApiPage.md#iuniteapipage)<[`EstateModel`](../modules/api_models_EstateModel.md#estatemodel)\>\>\>

UniteApiResult<IUniteApiPage<EstateModel>> Success

#### Defined in

[api/src/api/services/EstateV1Service.ts:43](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/services/EstateV1Service.ts#lines-43)

___

### getEstateV11

▸ `Static` **getEstateV11**(`__namedParameters`): [`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<[`EstateModel`](../modules/api_models_EstateModel.md#estatemodel)\>\>

Get a single estate

**`throws`** ApiError

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `__namedParameters` | `Object` | - |
| `__namedParameters.id` | `string` | Id of an existing estate |

#### Returns

[`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<[`EstateModel`](../modules/api_models_EstateModel.md#estatemodel)\>\>

UniteApiResult<EstateModel> Success

#### Defined in

[api/src/api/services/EstateV1Service.ts:71](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/services/EstateV1Service.ts#lines-71)

___

### postEstateV1

▸ `Static` **postEstateV1**(`__namedParameters`): [`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<`any`\>

Create an estate

**`throws`** ApiError

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `__namedParameters` | `Object` | - |
| `__namedParameters.requestBody?` | [`EstateModel`](../modules/api_models_EstateModel.md#estatemodel) | The POC data transfer object for a unit, which must adhere in terms of fields and rules (in the form of data annotations) to the following application model design. https://spikeglobal.atlassian.net/wiki/spaces/SU2/pages/2049900758/U2+Admin+Create+New+Contact#Contact-Form |

#### Returns

[`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<`any`\>

ProblemDetails Error

#### Defined in

[api/src/api/services/EstateV1Service.ts:19](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/services/EstateV1Service.ts#lines-19)

___

### putEstateV1

▸ `Static` **putEstateV1**(`__namedParameters`): [`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<[`EstateModel`](../modules/api_models_EstateModel.md#estatemodel)\>\>

Update an estate

**`throws`** ApiError

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `__namedParameters` | `Object` | - |
| `__namedParameters.id` | `string` | Id of the estate to update |
| `__namedParameters.requestBody?` | [`EstateModel`](../modules/api_models_EstateModel.md#estatemodel) | The POC data transfer object for an estate containing the new state of the existing estate. See https://spikeglobal.atlassian.net/wiki/spaces/SU2/pages/2049900758/U2+Admin+Create+New+Contact#Contact-Form |

#### Returns

[`CancelablePromise`](api_core_CancelablePromise.CancelablePromise.md)<[`ProblemDetails`](../modules/api_models_ProblemDetails.md#problemdetails) \| [`UniteApiResult`](../modules/api_core_UniteApiResult.md#uniteapiresult)<[`EstateModel`](../modules/api_models_EstateModel.md#estatemodel)\>\>

UniteApiResult<EstateModel> Success

#### Defined in

[api/src/api/services/EstateV1Service.ts:92](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/api/src/api/services/EstateV1Service.ts#lines-92)
