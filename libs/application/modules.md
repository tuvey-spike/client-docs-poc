[client](../../index.md) / Exports

# client

## Table of contents

### Modules

- [index](modules/index.md)
- [lib/Building](modules/lib_Building.md)
- [lib/Building/CreateBuilding](modules/lib_Building_CreateBuilding.md)
- [lib/Building/GetBuilding](modules/lib_Building_GetBuilding.md)
- [lib/Building/GetBuildings](modules/lib_Building_GetBuildings.md)
- [lib/Building/UpdateBuilding](modules/lib_Building_UpdateBuilding.md)
- [lib/Building/ValidateBuilding](modules/lib_Building_ValidateBuilding.md)
- [lib/Estate](modules/lib_Estate.md)
- [lib/Estate/CreateEstate](modules/lib_Estate_CreateEstate.md)
- [lib/Estate/GetEstate](modules/lib_Estate_GetEstate.md)
- [lib/Estate/GetEstates](modules/lib_Estate_GetEstates.md)
- [lib/Estate/UpdateEstate](modules/lib_Estate_UpdateEstate.md)
- [lib/Estate/ValidateEstate](modules/lib_Estate_ValidateEstate.md)
- [lib/Types](modules/lib_Types.md)
- [lib/Types/Building](modules/lib_Types_Building.md)
- [lib/Types/ErrorUnion](modules/lib_Types_ErrorUnion.md)
- [lib/Types/Estate](modules/lib_Types_Estate.md)
- [lib/Types/PagedResult](modules/lib_Types_PagedResult.md)
- [lib/Types/SuccessResult](modules/lib_Types_SuccessResult.md)
- [lib/Types/Unit](modules/lib_Types_Unit.md)
- [lib/Types/ValidationError](modules/lib_Types_ValidationError.md)
- [lib/Unit](modules/lib_Unit.md)
- [lib/Unit/CreateUnit](modules/lib_Unit_CreateUnit.md)
- [lib/Unit/GetUnit](modules/lib_Unit_GetUnit.md)
- [lib/Unit/GetUnits](modules/lib_Unit_GetUnits.md)
- [lib/Unit/UpdateUnit](modules/lib_Unit_UpdateUnit.md)
- [lib/Unit/ValidateUnit](modules/lib_Unit_ValidateUnit.md)
- [lib/utils](modules/lib_utils.md)
