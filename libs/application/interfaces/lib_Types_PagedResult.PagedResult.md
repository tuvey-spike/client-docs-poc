[client](../../../index.md) / [Exports](../modules.md) / [lib/Types/PagedResult](../modules/lib_Types_PagedResult.md) / PagedResult

# Interface: PagedResult<T\>

[lib/Types/PagedResult](../modules/lib_Types_PagedResult.md).PagedResult

## Type parameters

| Name |
| :------ |
| `T` |

## Table of contents

### Properties

- [list](lib_Types_PagedResult.PagedResult.md#list)
- [next](lib_Types_PagedResult.PagedResult.md#next)
- [pageSize](lib_Types_PagedResult.PagedResult.md#pagesize)
- [start](lib_Types_PagedResult.PagedResult.md#start)
- [total](lib_Types_PagedResult.PagedResult.md#total)

## Properties

### list

• **list**: `T`[]

#### Defined in

[application/src/lib/Types/PagedResult.ts:2](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Types/PagedResult.ts#lines-2)

___

### next

• **next**: `number`

#### Defined in

[application/src/lib/Types/PagedResult.ts:6](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Types/PagedResult.ts#lines-6)

___

### pageSize

• **pageSize**: `number`

#### Defined in

[application/src/lib/Types/PagedResult.ts:5](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Types/PagedResult.ts#lines-5)

___

### start

• **start**: `number`

#### Defined in

[application/src/lib/Types/PagedResult.ts:4](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Types/PagedResult.ts#lines-4)

___

### total

• **total**: `number`

#### Defined in

[application/src/lib/Types/PagedResult.ts:3](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Types/PagedResult.ts#lines-3)
