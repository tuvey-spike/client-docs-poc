[client](../../../index.md) / [Exports](../modules.md) / lib/Unit/CreateUnit

# Module: lib/Unit/CreateUnit

## Table of contents

### Type aliases

- [CreateUnitEvent](lib_Unit_CreateUnit.md#createunitevent)

### Functions

- [CreateUnit](lib_Unit_CreateUnit.md#createunit)

## Type aliases

### CreateUnitEvent

Ƭ **CreateUnitEvent**: { `type`: ``"data"`` ; `unit`: `UnitModel`  } \| [`ErrorUnion`](lib_Types_ErrorUnion.md#errorunion)

Event with details of the result of the CreateUnit
workflow.

#### Defined in

[application/src/lib/Unit/CreateUnit.ts:10](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Unit/CreateUnit.ts#lines-10)

## Functions

### CreateUnit

▸ **CreateUnit**(`unit`): `Promise`<[`CreateUnitEvent`](lib_Unit_CreateUnit.md#createunitevent)\>

Workflow to create a new unit.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `unit` | `UnitModel` | The unit. |

#### Returns

`Promise`<[`CreateUnitEvent`](lib_Unit_CreateUnit.md#createunitevent)\>

CreateUnitEvent with details of if creating the unit was successful.

#### Defined in

[application/src/lib/Unit/CreateUnit.ts:41](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Unit/CreateUnit.ts#lines-41)
