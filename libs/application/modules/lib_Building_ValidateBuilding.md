[client](../../../index.md) / [Exports](../modules.md) / lib/Building/ValidateBuilding

# Module: lib/Building/ValidateBuilding

## Table of contents

### Functions

- [ValidateBuilding](lib_Building_ValidateBuilding.md#validatebuilding)

## Functions

### ValidateBuilding

▸ **ValidateBuilding**(`building`): `Promise`<`Option`<`BuildingModel`, [`ValidationError`](../classes/lib_Types_ValidationError.ValidationError.md)\>\>

Validate the given building.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `building` | `BuildingModel` | The building to validate. |

#### Returns

`Promise`<`Option`<`BuildingModel`, [`ValidationError`](../classes/lib_Types_ValidationError.ValidationError.md)\>\>

Option value with the valid building or validation error.

#### Defined in

[application/src/lib/Building/ValidateBuilding.ts:11](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Building/ValidateBuilding.ts#lines-11)
