[client](../../../index.md) / [Exports](../modules.md) / lib/Building/GetBuildings

# Module: lib/Building/GetBuildings

## Table of contents

### Type aliases

- [GetBuildingsEvent](lib_Building_GetBuildings.md#getbuildingsevent)

### Functions

- [GetBuildings](lib_Building_GetBuildings.md#getbuildings)

## Type aliases

### GetBuildingsEvent

Ƭ **GetBuildingsEvent**: { `buildings`: [`PagedResult`](../interfaces/lib_Types_PagedResult.PagedResult.md)<`BuildingModel`\> ; `type`: ``"data"``  } \| [`ErrorUnion`](lib_Types_ErrorUnion.md#errorunion)

Event with details of the result of the GetBuildings
workflow.

#### Defined in

[application/src/lib/Building/GetBuildings.ts:9](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Building/GetBuildings.ts#lines-9)

## Functions

### GetBuildings

▸ `Const` **GetBuildings**(`start`, `count`): `Promise`<[`GetBuildingsEvent`](lib_Building_GetBuildings.md#getbuildingsevent)\>

Workflow to get collection of Buildings.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `start` | `number` | The start index to retrieve from. |
| `count` | `number` | - |

#### Returns

`Promise`<[`GetBuildingsEvent`](lib_Building_GetBuildings.md#getbuildingsevent)\>

GetBuildingsEvent with details of if getting the Buildings was successful.

#### Defined in

[application/src/lib/Building/GetBuildings.ts:48](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Building/GetBuildings.ts#lines-48)
