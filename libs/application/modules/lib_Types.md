[client](../../../index.md) / [Exports](../modules.md) / lib/Types

# Module: lib/Types

## Table of contents

### References

- [ErrorUnion](lib_Types.md#errorunion)
- [PagedResult](lib_Types.md#pagedresult)
- [SuccessResult](lib_Types.md#successresult)
- [ValidationError](lib_Types.md#validationerror)
- [buildingAddressSchema](lib_Types.md#buildingaddressschema)
- [buildingContactSchema](lib_Types.md#buildingcontactschema)
- [buildingSchema](lib_Types.md#buildingschema)
- [estateAddressSchema](lib_Types.md#estateaddressschema)
- [estateContactSchema](lib_Types.md#estatecontactschema)
- [estateSchema](lib_Types.md#estateschema)
- [unitAddressSchema](lib_Types.md#unitaddressschema)
- [unitSchema](lib_Types.md#unitschema)

## References

### ErrorUnion

Re-exports [ErrorUnion](lib_Types_ErrorUnion.md#errorunion)

___

### PagedResult

Re-exports [PagedResult](../interfaces/lib_Types_PagedResult.PagedResult.md)

___

### SuccessResult

Re-exports [SuccessResult](lib_Types_SuccessResult.md#successresult)

___

### ValidationError

Re-exports [ValidationError](../classes/lib_Types_ValidationError.ValidationError.md)

___

### buildingAddressSchema

Re-exports [buildingAddressSchema](lib_Types_Building.md#buildingaddressschema)

___

### buildingContactSchema

Re-exports [buildingContactSchema](lib_Types_Building.md#buildingcontactschema)

___

### buildingSchema

Re-exports [buildingSchema](lib_Types_Building.md#buildingschema)

___

### estateAddressSchema

Re-exports [estateAddressSchema](lib_Types_Estate.md#estateaddressschema)

___

### estateContactSchema

Re-exports [estateContactSchema](lib_Types_Estate.md#estatecontactschema)

___

### estateSchema

Re-exports [estateSchema](lib_Types_Estate.md#estateschema)

___

### unitAddressSchema

Re-exports [unitAddressSchema](lib_Types_Unit.md#unitaddressschema)

___

### unitSchema

Re-exports [unitSchema](lib_Types_Unit.md#unitschema)
