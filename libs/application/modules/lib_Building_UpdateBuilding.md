[client](../../../index.md) / [Exports](../modules.md) / lib/Building/UpdateBuilding

# Module: lib/Building/UpdateBuilding

## Table of contents

### Type aliases

- [UpdateBuildingEvent](lib_Building_UpdateBuilding.md#updatebuildingevent)

### Functions

- [UpdateBuilding](lib_Building_UpdateBuilding.md#updatebuilding)

## Type aliases

### UpdateBuildingEvent

Ƭ **UpdateBuildingEvent**: { `building`: `BuildingModel` ; `type`: ``"data"``  } \| [`ErrorUnion`](lib_Types_ErrorUnion.md#errorunion)

Event with details of the result of the UpdateEstate workflow.

#### Defined in

[application/src/lib/Building/UpdateBuilding.ts:10](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Building/UpdateBuilding.ts#lines-10)

## Functions

### UpdateBuilding

▸ **UpdateBuilding**(`building`): `Promise`<[`UpdateBuildingEvent`](lib_Building_UpdateBuilding.md#updatebuildingevent)\>

Workflow to update a building.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `building` | `BuildingModel` | Workflow to update a building. |

#### Returns

`Promise`<[`UpdateBuildingEvent`](lib_Building_UpdateBuilding.md#updatebuildingevent)\>

UpdateBuildingEvent with details of if updating the building was successful.

#### Defined in

[application/src/lib/Building/UpdateBuilding.ts:44](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Building/UpdateBuilding.ts#lines-44)
