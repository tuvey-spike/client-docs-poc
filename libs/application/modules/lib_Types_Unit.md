[client](../../../index.md) / [Exports](../modules.md) / lib/Types/Unit

# Module: lib/Types/Unit

## Table of contents

### Variables

- [unitAddressSchema](lib_Types_Unit.md#unitaddressschema)
- [unitSchema](lib_Types_Unit.md#unitschema)

## Variables

### unitAddressSchema

• **unitAddressSchema**: `OptionalObjectSchema`<{ `cityTown`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `country`: `default`<`string`, `AnyObject`, `string`\> ; `line1`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `line2`: `default`<`string`, `AnyObject`, `string`\> ; `postCode`: `RequiredStringSchema`<`string`, `AnyObject`\>  }, `AnyObject`, `TypeOfShape`<{ `cityTown`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `country`: `default`<`string`, `AnyObject`, `string`\> ; `line1`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `line2`: `default`<`string`, `AnyObject`, `string`\> ; `postCode`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>\>

#### Defined in

[application/src/lib/Types/Unit.ts:3](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Types/Unit.ts#lines-3)

___

### unitSchema

• **unitSchema**: `OptionalObjectSchema`<{ `address`: `default`<{ `cityTown`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `country`: `default`<`string`, `AnyObject`, `string`\> ; `line1`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `line2`: `default`<`string`, `AnyObject`, `string`\> ; `postCode`: `RequiredStringSchema`<`string`, `AnyObject`\>  }, `AnyObject`, `TypeOfShape`<{ `cityTown`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `country`: `default`<`string`, `AnyObject`, `string`\> ; `line1`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `line2`: `default`<`string`, `AnyObject`, `string`\> ; `postCode`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>, `AssertsShape`<{ `cityTown`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `country`: `default`<`string`, `AnyObject`, `string`\> ; `line1`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `line2`: `default`<`string`, `AnyObject`, `string`\> ; `postCode`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>\> ; `bathrooms`: `RequiredNumberSchema`<`undefined` \| `number`, `AnyObject`\> ; `beds`: `RequiredNumberSchema`<`undefined` \| `number`, `AnyObject`\> ; `block`: `default`<`string`, `AnyObject`, `string`\> ; `floor`: `RequiredNumberSchema`<`undefined` \| `number`, `AnyObject`\> ; `id`: `default`<`string`, `AnyObject`, `string`\> ; `type`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `unitNumber`: `RequiredStringSchema`<`string`, `AnyObject`\>  }, `AnyObject`, `TypeOfShape`<{ `address`: `default`<{ `cityTown`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `country`: `default`<`string`, `AnyObject`, `string`\> ; `line1`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `line2`: `default`<`string`, `AnyObject`, `string`\> ; `postCode`: `RequiredStringSchema`<`string`, `AnyObject`\>  }, `AnyObject`, `TypeOfShape`<{ `cityTown`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `country`: `default`<`string`, `AnyObject`, `string`\> ; `line1`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `line2`: `default`<`string`, `AnyObject`, `string`\> ; `postCode`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>, `AssertsShape`<{ `cityTown`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `country`: `default`<`string`, `AnyObject`, `string`\> ; `line1`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `line2`: `default`<`string`, `AnyObject`, `string`\> ; `postCode`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>\> ; `bathrooms`: `RequiredNumberSchema`<`undefined` \| `number`, `AnyObject`\> ; `beds`: `RequiredNumberSchema`<`undefined` \| `number`, `AnyObject`\> ; `block`: `default`<`string`, `AnyObject`, `string`\> ; `floor`: `RequiredNumberSchema`<`undefined` \| `number`, `AnyObject`\> ; `id`: `default`<`string`, `AnyObject`, `string`\> ; `type`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `unitNumber`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>\>

#### Defined in

[application/src/lib/Types/Unit.ts:11](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Types/Unit.ts#lines-11)
