[client](../../../index.md) / [Exports](../modules.md) / lib/Estate/GetEstates

# Module: lib/Estate/GetEstates

## Table of contents

### Type aliases

- [GetEstatesEvent](lib_Estate_GetEstates.md#getestatesevent)

### Functions

- [GetEstates](lib_Estate_GetEstates.md#getestates)

## Type aliases

### GetEstatesEvent

Ƭ **GetEstatesEvent**: { `estates`: [`PagedResult`](../interfaces/lib_Types_PagedResult.PagedResult.md)<`EstateModel`\> ; `type`: ``"data"``  } \| [`ErrorUnion`](lib_Types_ErrorUnion.md#errorunion)

Event with details of the result of the GetEstates
workflow.

#### Defined in

[application/src/lib/Estate/GetEstates.ts:9](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Estate/GetEstates.ts#lines-9)

## Functions

### GetEstates

▸ `Const` **GetEstates**(`start`, `count`): `Promise`<[`GetEstatesEvent`](lib_Estate_GetEstates.md#getestatesevent)\>

Workflow to get collection of estates.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `start` | `number` | The start index to retrieve from. |
| `count` | `number` | - |

#### Returns

`Promise`<[`GetEstatesEvent`](lib_Estate_GetEstates.md#getestatesevent)\>

GetEstatesEvent with details of if getting the estates was successful.

#### Defined in

[application/src/lib/Estate/GetEstates.ts:48](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Estate/GetEstates.ts#lines-48)
