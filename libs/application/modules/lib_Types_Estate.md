[client](../../../index.md) / [Exports](../modules.md) / lib/Types/Estate

# Module: lib/Types/Estate

## Table of contents

### Variables

- [estateAddressSchema](lib_Types_Estate.md#estateaddressschema)
- [estateContactSchema](lib_Types_Estate.md#estatecontactschema)
- [estateSchema](lib_Types_Estate.md#estateschema)

## Variables

### estateAddressSchema

• **estateAddressSchema**: `OptionalObjectSchema`<{ `cityTown`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `country`: `default`<`string`, `AnyObject`, `string`\> ; `line1`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `line2`: `default`<`string`, `AnyObject`, `string`\> ; `postCode`: `RequiredStringSchema`<`string`, `AnyObject`\>  }, `AnyObject`, `TypeOfShape`<{ `cityTown`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `country`: `default`<`string`, `AnyObject`, `string`\> ; `line1`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `line2`: `default`<`string`, `AnyObject`, `string`\> ; `postCode`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>\>

#### Defined in

[application/src/lib/Types/Estate.ts:5](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Types/Estate.ts#lines-5)

___

### estateContactSchema

• **estateContactSchema**: `OptionalObjectSchema`<{ `contactNo`: `default`<`string`, `AnyObject`, `string`\> ; `email`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `title`: `RequiredStringSchema`<`string`, `AnyObject`\>  }, `AnyObject`, `TypeOfShape`<{ `contactNo`: `default`<`string`, `AnyObject`, `string`\> ; `email`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `title`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>\>

#### Defined in

[application/src/lib/Types/Estate.ts:13](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Types/Estate.ts#lines-13)

___

### estateSchema

• **estateSchema**: `OptionalObjectSchema`<{ `address`: `default`<{ `cityTown`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `country`: `default`<`string`, `AnyObject`, `string`\> ; `line1`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `line2`: `default`<`string`, `AnyObject`, `string`\> ; `postCode`: `RequiredStringSchema`<`string`, `AnyObject`\>  }, `AnyObject`, `TypeOfShape`<{ `cityTown`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `country`: `default`<`string`, `AnyObject`, `string`\> ; `line1`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `line2`: `default`<`string`, `AnyObject`, `string`\> ; `postCode`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>, `AssertsShape`<{ `cityTown`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `country`: `default`<`string`, `AnyObject`, `string`\> ; `line1`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `line2`: `default`<`string`, `AnyObject`, `string`\> ; `postCode`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>\> ; `contacts`: `default`<`OptionalObjectSchema`<{ `contactNo`: `default`<`string`, `AnyObject`, `string`\> ; `email`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `title`: `RequiredStringSchema`<`string`, `AnyObject`\>  }, `AnyObject`, `TypeOfShape`<{ `contactNo`: `default`<`string`, `AnyObject`, `string`\> ; `email`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `title`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>\>, `AnyObject`, `undefined` \| `TypeOfShape`<{ `contactNo`: `default`<`string`, `AnyObject`, `string`\> ; `email`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `title`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>[], `undefined` \| `AssertsShape`<{ `contactNo`: `default`<`string`, `AnyObject`, `string`\> ; `email`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `title`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>[]\> ; `id`: `default`<`string`, `AnyObject`, `string`\> ; `name`: `RequiredStringSchema`<`undefined` \| `string`, `AnyObject`\> ; `region`: `default`<`string`, `AnyObject`, `string`\>  }, `AnyObject`, `TypeOfShape`<{ `address`: `default`<{ `cityTown`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `country`: `default`<`string`, `AnyObject`, `string`\> ; `line1`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `line2`: `default`<`string`, `AnyObject`, `string`\> ; `postCode`: `RequiredStringSchema`<`string`, `AnyObject`\>  }, `AnyObject`, `TypeOfShape`<{ `cityTown`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `country`: `default`<`string`, `AnyObject`, `string`\> ; `line1`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `line2`: `default`<`string`, `AnyObject`, `string`\> ; `postCode`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>, `AssertsShape`<{ `cityTown`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `country`: `default`<`string`, `AnyObject`, `string`\> ; `line1`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `line2`: `default`<`string`, `AnyObject`, `string`\> ; `postCode`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>\> ; `contacts`: `default`<`OptionalObjectSchema`<{ `contactNo`: `default`<`string`, `AnyObject`, `string`\> ; `email`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `title`: `RequiredStringSchema`<`string`, `AnyObject`\>  }, `AnyObject`, `TypeOfShape`<{ `contactNo`: `default`<`string`, `AnyObject`, `string`\> ; `email`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `title`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>\>, `AnyObject`, `undefined` \| `TypeOfShape`<{ `contactNo`: `default`<`string`, `AnyObject`, `string`\> ; `email`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `title`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>[], `undefined` \| `AssertsShape`<{ `contactNo`: `default`<`string`, `AnyObject`, `string`\> ; `email`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `title`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>[]\> ; `id`: `default`<`string`, `AnyObject`, `string`\> ; `name`: `RequiredStringSchema`<`undefined` \| `string`, `AnyObject`\> ; `region`: `default`<`string`, `AnyObject`, `string`\>  }\>\>

#### Defined in

[application/src/lib/Types/Estate.ts:19](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Types/Estate.ts#lines-19)
