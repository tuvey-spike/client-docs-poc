[client](../../../index.md) / [Exports](../modules.md) / lib/Estate/GetEstate

# Module: lib/Estate/GetEstate

## Table of contents

### Type aliases

- [GetEstateEvent](lib_Estate_GetEstate.md#getestateevent)

### Functions

- [GetEstate](lib_Estate_GetEstate.md#getestate)

## Type aliases

### GetEstateEvent

Ƭ **GetEstateEvent**: { `estate`: `EstateModel` ; `type`: ``"data"``  } \| [`ErrorUnion`](lib_Types_ErrorUnion.md#errorunion)

Event with details of the result of the GetEstate
workflow.

#### Defined in

[application/src/lib/Estate/GetEstate.ts:10](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Estate/GetEstate.ts#lines-10)

## Functions

### GetEstate

▸ **GetEstate**(`id`): `Promise`<[`GetEstateEvent`](lib_Estate_GetEstate.md#getestateevent)\>

Workflow to get an existing estate.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `id` | `string` | The estate id. |

#### Returns

`Promise`<[`GetEstateEvent`](lib_Estate_GetEstate.md#getestateevent)\>

GetEstateEvent with details of getting the estate.

#### Defined in

[application/src/lib/Estate/GetEstate.ts:51](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Estate/GetEstate.ts#lines-51)
