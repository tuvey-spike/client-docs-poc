[client](../../../index.md) / [Exports](../modules.md) / lib/Types/ValidationError

# Module: lib/Types/ValidationError

## Table of contents

### Classes

- [ValidationError](../classes/lib_Types_ValidationError.ValidationError.md)
