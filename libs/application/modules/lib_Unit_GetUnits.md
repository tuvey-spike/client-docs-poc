[client](../../../index.md) / [Exports](../modules.md) / lib/Unit/GetUnits

# Module: lib/Unit/GetUnits

## Table of contents

### Type aliases

- [GetUnitsEvent](lib_Unit_GetUnits.md#getunitsevent)

### Functions

- [GetUnits](lib_Unit_GetUnits.md#getunits)

## Type aliases

### GetUnitsEvent

Ƭ **GetUnitsEvent**: { `type`: ``"data"`` ; `units`: [`PagedResult`](../interfaces/lib_Types_PagedResult.PagedResult.md)<`UnitModel`\>  } \| [`ErrorUnion`](lib_Types_ErrorUnion.md#errorunion)

Event with details of the result of the GetUnits
workflow.

#### Defined in

[application/src/lib/Unit/GetUnits.ts:9](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Unit/GetUnits.ts#lines-9)

## Functions

### GetUnits

▸ `Const` **GetUnits**(`start`, `count`): `Promise`<[`GetUnitsEvent`](lib_Unit_GetUnits.md#getunitsevent)\>

Workflow to get collection of units.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `start` | `number` | The start index to retrieve from. |
| `count` | `number` | - |

#### Returns

`Promise`<[`GetUnitsEvent`](lib_Unit_GetUnits.md#getunitsevent)\>

GetUnitsEvent with details of if getting the units was successful.

#### Defined in

[application/src/lib/Unit/GetUnits.ts:48](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Unit/GetUnits.ts#lines-48)
