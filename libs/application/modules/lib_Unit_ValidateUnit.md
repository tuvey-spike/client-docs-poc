[client](../../../index.md) / [Exports](../modules.md) / lib/Unit/ValidateUnit

# Module: lib/Unit/ValidateUnit

## Table of contents

### Functions

- [ValidateUnit](lib_Unit_ValidateUnit.md#validateunit)

## Functions

### ValidateUnit

▸ **ValidateUnit**(`unit`): `Promise`<`Option`<`UnitModel`, [`ValidationError`](../classes/lib_Types_ValidationError.ValidationError.md)\>\>

Validate the given unit.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `unit` | `UnitModel` | The unit to validate. |

#### Returns

`Promise`<`Option`<`UnitModel`, [`ValidationError`](../classes/lib_Types_ValidationError.ValidationError.md)\>\>

Option value with the valid unit or validation error.

#### Defined in

[application/src/lib/Unit/ValidateUnit.ts:12](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Unit/ValidateUnit.ts#lines-12)
