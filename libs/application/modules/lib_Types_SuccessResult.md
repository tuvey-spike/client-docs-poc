[client](../../../index.md) / [Exports](../modules.md) / lib/Types/SuccessResult

# Module: lib/Types/SuccessResult

## Table of contents

### Type aliases

- [SuccessResult](lib_Types_SuccessResult.md#successresult)

## Type aliases

### SuccessResult

Ƭ **SuccessResult**<`TData`\>: `Object`

#### Type parameters

| Name |
| :------ |
| `TData` |

#### Type declaration

| Name | Type | Description |
| :------ | :------ | :------ |
| `count` | `number` | Count of returned or affected items. |
| `total` | `number` | Total of queried or affected items. |
| `value` | `TData` | Payload. |

#### Defined in

[application/src/lib/Types/SuccessResult.ts:1](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Types/SuccessResult.ts#lines-1)
