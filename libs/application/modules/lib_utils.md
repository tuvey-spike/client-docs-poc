[client](../../../index.md) / [Exports](../modules.md) / lib/utils

# Module: lib/utils

## Table of contents

### Functions

- [GetErrorOrSuccess](lib_utils.md#geterrororsuccess)
- [GetServerError](lib_utils.md#getservererror)
- [GetUnknownError](lib_utils.md#getunknownerror)
- [GetValidationError](lib_utils.md#getvalidationerror)
- [StringHasContent](lib_utils.md#stringhascontent)
- [ToApiResult](lib_utils.md#toapiresult)
- [Validate](lib_utils.md#validate)

## Functions

### GetErrorOrSuccess

▸ **GetErrorOrSuccess**<`InputT`, `OutputT`\>(`result`, `success`): `OutputT` \| [`ErrorUnion`](lib_Types_ErrorUnion.md#errorunion)

#### Type parameters

| Name |
| :------ |
| `InputT` |
| `OutputT` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `result` | `UniteApiResult`<`InputT`\> |
| `success` | (`r`: [`SuccessResult`](lib_Types_SuccessResult.md#successresult)<`InputT`\>) => `OutputT` |

#### Returns

`OutputT` \| [`ErrorUnion`](lib_Types_ErrorUnion.md#errorunion)

#### Defined in

[application/src/lib/utils.ts:32](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/utils.ts#lines-32)

___

### GetServerError

▸ **GetServerError**(`errors`): [`ErrorUnion`](lib_Types_ErrorUnion.md#errorunion)

#### Parameters

| Name | Type |
| :------ | :------ |
| `errors` | `UniteApiError`[] |

#### Returns

[`ErrorUnion`](lib_Types_ErrorUnion.md#errorunion)

#### Defined in

[application/src/lib/utils.ts:60](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/utils.ts#lines-60)

___

### GetUnknownError

▸ **GetUnknownError**(`message`): [`ErrorUnion`](lib_Types_ErrorUnion.md#errorunion)

#### Parameters

| Name | Type |
| :------ | :------ |
| `message` | `string` |

#### Returns

[`ErrorUnion`](lib_Types_ErrorUnion.md#errorunion)

#### Defined in

[application/src/lib/utils.ts:64](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/utils.ts#lines-64)

___

### GetValidationError

▸ **GetValidationError**(`error`): [`ErrorUnion`](lib_Types_ErrorUnion.md#errorunion)

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | [`ValidationError`](../classes/lib_Types_ValidationError.ValidationError.md) |

#### Returns

[`ErrorUnion`](lib_Types_ErrorUnion.md#errorunion)

#### Defined in

[application/src/lib/utils.ts:56](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/utils.ts#lines-56)

___

### StringHasContent

▸ `Const` **StringHasContent**(`value`): `boolean`

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `string` |

#### Returns

`boolean`

#### Defined in

[application/src/lib/utils.ts:5](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/utils.ts#lines-5)

___

### ToApiResult

▸ **ToApiResult**<`InputT`\>(`result`): `UniteApiResult`<`InputT`\>

#### Type parameters

| Name |
| :------ |
| `InputT` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `result` | `UniteApiResult`<`InputT`\> \| `ProblemDetails` |

#### Returns

`UniteApiResult`<`InputT`\>

#### Defined in

[application/src/lib/utils.ts:25](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/utils.ts#lines-25)

___

### Validate

▸ **Validate**<`TValue`, `TError`\>(`value`, `validators`): `ValidateResult`<`TValue`, `TError`\>

#### Type parameters

| Name | Type |
| :------ | :------ |
| `TValue` | `TValue` |
| `TError` | extends `OptionError`<`TError`\> |

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `TValue` |
| `validators` | (`form`: `TValue`) => `Option`<`TValue`, `TError`\>[][] |

#### Returns

`ValidateResult`<`TValue`, `TError`\>

#### Defined in

[application/src/lib/utils.ts:13](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/utils.ts#lines-13)
