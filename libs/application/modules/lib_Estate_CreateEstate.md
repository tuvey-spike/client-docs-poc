[client](../../../index.md) / [Exports](../modules.md) / lib/Estate/CreateEstate

# Module: lib/Estate/CreateEstate

## Table of contents

### Type aliases

- [CreateEstateEvent](lib_Estate_CreateEstate.md#createestateevent)

### Functions

- [CreateEstate](lib_Estate_CreateEstate.md#createestate)

## Type aliases

### CreateEstateEvent

Ƭ **CreateEstateEvent**: { `estate`: `EstateModel` ; `type`: ``"data"``  } \| [`ErrorUnion`](lib_Types_ErrorUnion.md#errorunion)

Event with details of the result of the CreateEstate
workflow.

#### Defined in

[application/src/lib/Estate/CreateEstate.ts:10](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Estate/CreateEstate.ts#lines-10)

## Functions

### CreateEstate

▸ **CreateEstate**(`estate`): `Promise`<[`CreateEstateEvent`](lib_Estate_CreateEstate.md#createestateevent)\>

Workflow to create a new estate.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `estate` | `EstateModel` | The estate. |

#### Returns

`Promise`<[`CreateEstateEvent`](lib_Estate_CreateEstate.md#createestateevent)\>

CreateEstateEvent with details of if creating the estate was successful.

#### Defined in

[application/src/lib/Estate/CreateEstate.ts:41](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Estate/CreateEstate.ts#lines-41)
