[client](../../../index.md) / [Exports](../modules.md) / lib/Types/PagedResult

# Module: lib/Types/PagedResult

## Table of contents

### Interfaces

- [PagedResult](../interfaces/lib_Types_PagedResult.PagedResult.md)
