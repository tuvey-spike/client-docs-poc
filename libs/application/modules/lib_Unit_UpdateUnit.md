[client](../../../index.md) / [Exports](../modules.md) / lib/Unit/UpdateUnit

# Module: lib/Unit/UpdateUnit

## Table of contents

### Type aliases

- [UpdateUnitEvent](lib_Unit_UpdateUnit.md#updateunitevent)

### Functions

- [UpdateUnit](lib_Unit_UpdateUnit.md#updateunit)

## Type aliases

### UpdateUnitEvent

Ƭ **UpdateUnitEvent**: { `type`: ``"data"`` ; `unit`: `UnitModel`  } \| [`ErrorUnion`](lib_Types_ErrorUnion.md#errorunion)

Event with details of the result of the UpdateUnit workflow.

#### Defined in

[application/src/lib/Unit/UpdateUnit.ts:10](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Unit/UpdateUnit.ts#lines-10)

## Functions

### UpdateUnit

▸ **UpdateUnit**(`unit`): `Promise`<[`UpdateUnitEvent`](lib_Unit_UpdateUnit.md#updateunitevent)\>

Workflow to update a unit.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `unit` | `UnitModel` | The unit. |

#### Returns

`Promise`<[`UpdateUnitEvent`](lib_Unit_UpdateUnit.md#updateunitevent)\>

UpdateUnitEvent with details of if updating the unit was successful.

#### Defined in

[application/src/lib/Unit/UpdateUnit.ts:44](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Unit/UpdateUnit.ts#lines-44)
