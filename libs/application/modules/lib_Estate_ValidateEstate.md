[client](../../../index.md) / [Exports](../modules.md) / lib/Estate/ValidateEstate

# Module: lib/Estate/ValidateEstate

## Table of contents

### Functions

- [ValidateEstate](lib_Estate_ValidateEstate.md#validateestate)

## Functions

### ValidateEstate

▸ **ValidateEstate**(`estate`): `Promise`<`Option`<`EstateModel`, [`ValidationError`](../classes/lib_Types_ValidationError.ValidationError.md)\>\>

Validate the given estate.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `estate` | `EstateModel` | The estate to validate. |

#### Returns

`Promise`<`Option`<`EstateModel`, [`ValidationError`](../classes/lib_Types_ValidationError.ValidationError.md)\>\>

Option value with the valid estate or a validation error.

#### Defined in

[application/src/lib/Estate/ValidateEstate.ts:20](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Estate/ValidateEstate.ts#lines-20)
