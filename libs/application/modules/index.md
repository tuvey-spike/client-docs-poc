[client](../../../index.md) / [Exports](../modules.md) / index

# Module: index

## Table of contents

### References

- [CreateBuilding](index.md#createbuilding)
- [CreateBuildingEvent](index.md#createbuildingevent)
- [CreateEstate](index.md#createestate)
- [CreateEstateEvent](index.md#createestateevent)
- [CreateUnit](index.md#createunit)
- [CreateUnitEvent](index.md#createunitevent)
- [ErrorUnion](index.md#errorunion)
- [GetBuilding](index.md#getbuilding)
- [GetBuildingEvent](index.md#getbuildingevent)
- [GetBuildings](index.md#getbuildings)
- [GetBuildingsEvent](index.md#getbuildingsevent)
- [GetEstate](index.md#getestate)
- [GetEstateEvent](index.md#getestateevent)
- [GetEstates](index.md#getestates)
- [GetEstatesEvent](index.md#getestatesevent)
- [GetUnit](index.md#getunit)
- [GetUnitEvent](index.md#getunitevent)
- [GetUnits](index.md#getunits)
- [GetUnitsEvent](index.md#getunitsevent)
- [PagedResult](index.md#pagedresult)
- [SuccessResult](index.md#successresult)
- [UpdateBuilding](index.md#updatebuilding)
- [UpdateBuildingEvent](index.md#updatebuildingevent)
- [UpdateEstate](index.md#updateestate)
- [UpdateEstateEvent](index.md#updateestateevent)
- [UpdateUnit](index.md#updateunit)
- [UpdateUnitEvent](index.md#updateunitevent)
- [ValidateEstate](index.md#validateestate)
- [ValidateUnit](index.md#validateunit)
- [ValidationError](index.md#validationerror)
- [buildingAddressSchema](index.md#buildingaddressschema)
- [buildingContactSchema](index.md#buildingcontactschema)
- [buildingSchema](index.md#buildingschema)
- [estateAddressSchema](index.md#estateaddressschema)
- [estateContactSchema](index.md#estatecontactschema)
- [estateSchema](index.md#estateschema)
- [unitAddressSchema](index.md#unitaddressschema)
- [unitSchema](index.md#unitschema)

## References

### CreateBuilding

Re-exports [CreateBuilding](lib_Building_CreateBuilding.md#createbuilding)

___

### CreateBuildingEvent

Re-exports [CreateBuildingEvent](lib_Building_CreateBuilding.md#createbuildingevent)

___

### CreateEstate

Re-exports [CreateEstate](lib_Estate_CreateEstate.md#createestate)

___

### CreateEstateEvent

Re-exports [CreateEstateEvent](lib_Estate_CreateEstate.md#createestateevent)

___

### CreateUnit

Re-exports [CreateUnit](lib_Unit_CreateUnit.md#createunit)

___

### CreateUnitEvent

Re-exports [CreateUnitEvent](lib_Unit_CreateUnit.md#createunitevent)

___

### ErrorUnion

Re-exports [ErrorUnion](lib_Types_ErrorUnion.md#errorunion)

___

### GetBuilding

Re-exports [GetBuilding](lib_Building_GetBuilding.md#getbuilding)

___

### GetBuildingEvent

Re-exports [GetBuildingEvent](lib_Building_GetBuilding.md#getbuildingevent)

___

### GetBuildings

Re-exports [GetBuildings](lib_Building_GetBuildings.md#getbuildings)

___

### GetBuildingsEvent

Re-exports [GetBuildingsEvent](lib_Building_GetBuildings.md#getbuildingsevent)

___

### GetEstate

Re-exports [GetEstate](lib_Estate_GetEstate.md#getestate)

___

### GetEstateEvent

Re-exports [GetEstateEvent](lib_Estate_GetEstate.md#getestateevent)

___

### GetEstates

Re-exports [GetEstates](lib_Estate_GetEstates.md#getestates)

___

### GetEstatesEvent

Re-exports [GetEstatesEvent](lib_Estate_GetEstates.md#getestatesevent)

___

### GetUnit

Re-exports [GetUnit](lib_Unit_GetUnit.md#getunit)

___

### GetUnitEvent

Re-exports [GetUnitEvent](lib_Unit_GetUnit.md#getunitevent)

___

### GetUnits

Re-exports [GetUnits](lib_Unit_GetUnits.md#getunits)

___

### GetUnitsEvent

Re-exports [GetUnitsEvent](lib_Unit_GetUnits.md#getunitsevent)

___

### PagedResult

Re-exports [PagedResult](../interfaces/lib_Types_PagedResult.PagedResult.md)

___

### SuccessResult

Re-exports [SuccessResult](lib_Types_SuccessResult.md#successresult)

___

### UpdateBuilding

Re-exports [UpdateBuilding](lib_Building_UpdateBuilding.md#updatebuilding)

___

### UpdateBuildingEvent

Re-exports [UpdateBuildingEvent](lib_Building_UpdateBuilding.md#updatebuildingevent)

___

### UpdateEstate

Re-exports [UpdateEstate](lib_Estate_UpdateEstate.md#updateestate)

___

### UpdateEstateEvent

Re-exports [UpdateEstateEvent](lib_Estate_UpdateEstate.md#updateestateevent)

___

### UpdateUnit

Re-exports [UpdateUnit](lib_Unit_UpdateUnit.md#updateunit)

___

### UpdateUnitEvent

Re-exports [UpdateUnitEvent](lib_Unit_UpdateUnit.md#updateunitevent)

___

### ValidateEstate

Re-exports [ValidateEstate](lib_Estate_ValidateEstate.md#validateestate)

___

### ValidateUnit

Re-exports [ValidateUnit](lib_Unit_ValidateUnit.md#validateunit)

___

### ValidationError

Re-exports [ValidationError](../classes/lib_Types_ValidationError.ValidationError.md)

___

### buildingAddressSchema

Re-exports [buildingAddressSchema](lib_Types_Building.md#buildingaddressschema)

___

### buildingContactSchema

Re-exports [buildingContactSchema](lib_Types_Building.md#buildingcontactschema)

___

### buildingSchema

Re-exports [buildingSchema](lib_Types_Building.md#buildingschema)

___

### estateAddressSchema

Re-exports [estateAddressSchema](lib_Types_Estate.md#estateaddressschema)

___

### estateContactSchema

Re-exports [estateContactSchema](lib_Types_Estate.md#estatecontactschema)

___

### estateSchema

Re-exports [estateSchema](lib_Types_Estate.md#estateschema)

___

### unitAddressSchema

Re-exports [unitAddressSchema](lib_Types_Unit.md#unitaddressschema)

___

### unitSchema

Re-exports [unitSchema](lib_Types_Unit.md#unitschema)
