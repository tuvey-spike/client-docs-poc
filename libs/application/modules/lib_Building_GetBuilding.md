[client](../../../index.md) / [Exports](../modules.md) / lib/Building/GetBuilding

# Module: lib/Building/GetBuilding

## Table of contents

### Type aliases

- [GetBuildingEvent](lib_Building_GetBuilding.md#getbuildingevent)

### Functions

- [GetBuilding](lib_Building_GetBuilding.md#getbuilding)

## Type aliases

### GetBuildingEvent

Ƭ **GetBuildingEvent**: { `building`: `BuildingModel` \| ``null`` ; `type`: ``"data"``  } \| [`ErrorUnion`](lib_Types_ErrorUnion.md#errorunion)

Event with details of the result of the GetBuilding
workflow.

#### Defined in

[application/src/lib/Building/GetBuilding.ts:10](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Building/GetBuilding.ts#lines-10)

## Functions

### GetBuilding

▸ **GetBuilding**(`id`): `Promise`<[`GetBuildingEvent`](lib_Building_GetBuilding.md#getbuildingevent)\>

Workflow to get an existing building.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `id` | `string` | The building id. |

#### Returns

`Promise`<[`GetBuildingEvent`](lib_Building_GetBuilding.md#getbuildingevent)\>

GetBuildingEvent with details of getting the building.

#### Defined in

[application/src/lib/Building/GetBuilding.ts:51](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Building/GetBuilding.ts#lines-51)
