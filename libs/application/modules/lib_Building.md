[client](../../../index.md) / [Exports](../modules.md) / lib/Building

# Module: lib/Building

## Table of contents

### References

- [CreateBuilding](lib_Building.md#createbuilding)
- [CreateBuildingEvent](lib_Building.md#createbuildingevent)
- [GetBuilding](lib_Building.md#getbuilding)
- [GetBuildingEvent](lib_Building.md#getbuildingevent)
- [GetBuildings](lib_Building.md#getbuildings)
- [GetBuildingsEvent](lib_Building.md#getbuildingsevent)
- [UpdateBuilding](lib_Building.md#updatebuilding)
- [UpdateBuildingEvent](lib_Building.md#updatebuildingevent)

## References

### CreateBuilding

Re-exports [CreateBuilding](lib_Building_CreateBuilding.md#createbuilding)

___

### CreateBuildingEvent

Re-exports [CreateBuildingEvent](lib_Building_CreateBuilding.md#createbuildingevent)

___

### GetBuilding

Re-exports [GetBuilding](lib_Building_GetBuilding.md#getbuilding)

___

### GetBuildingEvent

Re-exports [GetBuildingEvent](lib_Building_GetBuilding.md#getbuildingevent)

___

### GetBuildings

Re-exports [GetBuildings](lib_Building_GetBuildings.md#getbuildings)

___

### GetBuildingsEvent

Re-exports [GetBuildingsEvent](lib_Building_GetBuildings.md#getbuildingsevent)

___

### UpdateBuilding

Re-exports [UpdateBuilding](lib_Building_UpdateBuilding.md#updatebuilding)

___

### UpdateBuildingEvent

Re-exports [UpdateBuildingEvent](lib_Building_UpdateBuilding.md#updatebuildingevent)
