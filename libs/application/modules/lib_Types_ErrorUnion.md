[client](../../../index.md) / [Exports](../modules.md) / lib/Types/ErrorUnion

# Module: lib/Types/ErrorUnion

## Table of contents

### Type aliases

- [ErrorUnion](lib_Types_ErrorUnion.md#errorunion)

## Type aliases

### ErrorUnion

Ƭ **ErrorUnion**: { `errors`: `UniteApiError`[] ; `type`: ``"serverError"``  } \| { `error`: [`ValidationError`](../classes/lib_Types_ValidationError.ValidationError.md) ; `type`: ``"validationError"``  } \| { `error`: `OptionError` ; `type`: ``"unknownError"``  }

#### Defined in

[application/src/lib/Types/ErrorUnion.ts:5](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Types/ErrorUnion.ts#lines-5)
