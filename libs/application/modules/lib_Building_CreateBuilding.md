[client](../../../index.md) / [Exports](../modules.md) / lib/Building/CreateBuilding

# Module: lib/Building/CreateBuilding

## Table of contents

### Type aliases

- [CreateBuildingEvent](lib_Building_CreateBuilding.md#createbuildingevent)

### Functions

- [CreateBuilding](lib_Building_CreateBuilding.md#createbuilding)

## Type aliases

### CreateBuildingEvent

Ƭ **CreateBuildingEvent**: { `building`: `BuildingModel` ; `type`: ``"data"``  } \| [`ErrorUnion`](lib_Types_ErrorUnion.md#errorunion)

Event with details of the result of the CreateBuilding
workflow.

#### Defined in

[application/src/lib/Building/CreateBuilding.ts:10](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Building/CreateBuilding.ts#lines-10)

## Functions

### CreateBuilding

▸ **CreateBuilding**(`building`): `Promise`<[`CreateBuildingEvent`](lib_Building_CreateBuilding.md#createbuildingevent)\>

Workflow to create a new building.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `building` | `BuildingModel` | The building. |

#### Returns

`Promise`<[`CreateBuildingEvent`](lib_Building_CreateBuilding.md#createbuildingevent)\>

CreateBuildingEvent with details of if creating the building was successful.

#### Defined in

[application/src/lib/Building/CreateBuilding.ts:41](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Building/CreateBuilding.ts#lines-41)
