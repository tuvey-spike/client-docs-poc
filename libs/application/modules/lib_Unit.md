[client](../../../index.md) / [Exports](../modules.md) / lib/Unit

# Module: lib/Unit

## Table of contents

### References

- [CreateUnit](lib_Unit.md#createunit)
- [CreateUnitEvent](lib_Unit.md#createunitevent)
- [GetUnit](lib_Unit.md#getunit)
- [GetUnitEvent](lib_Unit.md#getunitevent)
- [GetUnits](lib_Unit.md#getunits)
- [GetUnitsEvent](lib_Unit.md#getunitsevent)
- [UpdateUnit](lib_Unit.md#updateunit)
- [UpdateUnitEvent](lib_Unit.md#updateunitevent)
- [ValidateUnit](lib_Unit.md#validateunit)

## References

### CreateUnit

Re-exports [CreateUnit](lib_Unit_CreateUnit.md#createunit)

___

### CreateUnitEvent

Re-exports [CreateUnitEvent](lib_Unit_CreateUnit.md#createunitevent)

___

### GetUnit

Re-exports [GetUnit](lib_Unit_GetUnit.md#getunit)

___

### GetUnitEvent

Re-exports [GetUnitEvent](lib_Unit_GetUnit.md#getunitevent)

___

### GetUnits

Re-exports [GetUnits](lib_Unit_GetUnits.md#getunits)

___

### GetUnitsEvent

Re-exports [GetUnitsEvent](lib_Unit_GetUnits.md#getunitsevent)

___

### UpdateUnit

Re-exports [UpdateUnit](lib_Unit_UpdateUnit.md#updateunit)

___

### UpdateUnitEvent

Re-exports [UpdateUnitEvent](lib_Unit_UpdateUnit.md#updateunitevent)

___

### ValidateUnit

Re-exports [ValidateUnit](lib_Unit_ValidateUnit.md#validateunit)
