[client](../../../index.md) / [Exports](../modules.md) / lib/Types/Building

# Module: lib/Types/Building

## Table of contents

### Variables

- [buildingAddressSchema](lib_Types_Building.md#buildingaddressschema)
- [buildingContactSchema](lib_Types_Building.md#buildingcontactschema)
- [buildingSchema](lib_Types_Building.md#buildingschema)

## Variables

### buildingAddressSchema

• **buildingAddressSchema**: `OptionalObjectSchema`<{ `cityTown`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `country`: `default`<`string`, `AnyObject`, `string`\> ; `line1`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `line2`: `default`<`string`, `AnyObject`, `string`\> ; `postCode`: `RequiredStringSchema`<`string`, `AnyObject`\>  }, `AnyObject`, `TypeOfShape`<{ `cityTown`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `country`: `default`<`string`, `AnyObject`, `string`\> ; `line1`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `line2`: `default`<`string`, `AnyObject`, `string`\> ; `postCode`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>\>

#### Defined in

[application/src/lib/Types/Building.ts:3](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Types/Building.ts#lines-3)

___

### buildingContactSchema

• **buildingContactSchema**: `OptionalObjectSchema`<{ `contactNo`: `default`<`string`, `AnyObject`, `string`\> ; `email`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `title`: `RequiredStringSchema`<`string`, `AnyObject`\>  }, `AnyObject`, `TypeOfShape`<{ `contactNo`: `default`<`string`, `AnyObject`, `string`\> ; `email`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `title`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>\>

#### Defined in

[application/src/lib/Types/Building.ts:11](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Types/Building.ts#lines-11)

___

### buildingSchema

• **buildingSchema**: `OptionalObjectSchema`<{ `address`: `default`<{ `cityTown`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `country`: `default`<`string`, `AnyObject`, `string`\> ; `line1`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `line2`: `default`<`string`, `AnyObject`, `string`\> ; `postCode`: `RequiredStringSchema`<`string`, `AnyObject`\>  }, `AnyObject`, `TypeOfShape`<{ `cityTown`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `country`: `default`<`string`, `AnyObject`, `string`\> ; `line1`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `line2`: `default`<`string`, `AnyObject`, `string`\> ; `postCode`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>, `AssertsShape`<{ `cityTown`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `country`: `default`<`string`, `AnyObject`, `string`\> ; `line1`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `line2`: `default`<`string`, `AnyObject`, `string`\> ; `postCode`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>\> ; `contacts`: `default`<`OptionalObjectSchema`<{ `contactNo`: `default`<`string`, `AnyObject`, `string`\> ; `email`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `title`: `RequiredStringSchema`<`string`, `AnyObject`\>  }, `AnyObject`, `TypeOfShape`<{ `contactNo`: `default`<`string`, `AnyObject`, `string`\> ; `email`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `title`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>\>, `AnyObject`, `undefined` \| `TypeOfShape`<{ `contactNo`: `default`<`string`, `AnyObject`, `string`\> ; `email`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `title`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>[], `undefined` \| `AssertsShape`<{ `contactNo`: `default`<`string`, `AnyObject`, `string`\> ; `email`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `title`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>[]\> ; `id`: `default`<`string`, `AnyObject`, `string`\> ; `name`: `RequiredStringSchema`<`string`, `AnyObject`\>  }, `AnyObject`, `TypeOfShape`<{ `address`: `default`<{ `cityTown`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `country`: `default`<`string`, `AnyObject`, `string`\> ; `line1`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `line2`: `default`<`string`, `AnyObject`, `string`\> ; `postCode`: `RequiredStringSchema`<`string`, `AnyObject`\>  }, `AnyObject`, `TypeOfShape`<{ `cityTown`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `country`: `default`<`string`, `AnyObject`, `string`\> ; `line1`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `line2`: `default`<`string`, `AnyObject`, `string`\> ; `postCode`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>, `AssertsShape`<{ `cityTown`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `country`: `default`<`string`, `AnyObject`, `string`\> ; `line1`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `line2`: `default`<`string`, `AnyObject`, `string`\> ; `postCode`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>\> ; `contacts`: `default`<`OptionalObjectSchema`<{ `contactNo`: `default`<`string`, `AnyObject`, `string`\> ; `email`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `title`: `RequiredStringSchema`<`string`, `AnyObject`\>  }, `AnyObject`, `TypeOfShape`<{ `contactNo`: `default`<`string`, `AnyObject`, `string`\> ; `email`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `title`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>\>, `AnyObject`, `undefined` \| `TypeOfShape`<{ `contactNo`: `default`<`string`, `AnyObject`, `string`\> ; `email`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `title`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>[], `undefined` \| `AssertsShape`<{ `contactNo`: `default`<`string`, `AnyObject`, `string`\> ; `email`: `RequiredStringSchema`<`string`, `AnyObject`\> ; `title`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>[]\> ; `id`: `default`<`string`, `AnyObject`, `string`\> ; `name`: `RequiredStringSchema`<`string`, `AnyObject`\>  }\>\>

#### Defined in

[application/src/lib/Types/Building.ts:17](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Types/Building.ts#lines-17)
