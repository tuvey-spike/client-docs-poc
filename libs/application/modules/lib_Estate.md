[client](../../../index.md) / [Exports](../modules.md) / lib/Estate

# Module: lib/Estate

## Table of contents

### References

- [CreateEstate](lib_Estate.md#createestate)
- [CreateEstateEvent](lib_Estate.md#createestateevent)
- [GetEstate](lib_Estate.md#getestate)
- [GetEstateEvent](lib_Estate.md#getestateevent)
- [GetEstates](lib_Estate.md#getestates)
- [GetEstatesEvent](lib_Estate.md#getestatesevent)
- [UpdateEstate](lib_Estate.md#updateestate)
- [UpdateEstateEvent](lib_Estate.md#updateestateevent)
- [ValidateEstate](lib_Estate.md#validateestate)

## References

### CreateEstate

Re-exports [CreateEstate](lib_Estate_CreateEstate.md#createestate)

___

### CreateEstateEvent

Re-exports [CreateEstateEvent](lib_Estate_CreateEstate.md#createestateevent)

___

### GetEstate

Re-exports [GetEstate](lib_Estate_GetEstate.md#getestate)

___

### GetEstateEvent

Re-exports [GetEstateEvent](lib_Estate_GetEstate.md#getestateevent)

___

### GetEstates

Re-exports [GetEstates](lib_Estate_GetEstates.md#getestates)

___

### GetEstatesEvent

Re-exports [GetEstatesEvent](lib_Estate_GetEstates.md#getestatesevent)

___

### UpdateEstate

Re-exports [UpdateEstate](lib_Estate_UpdateEstate.md#updateestate)

___

### UpdateEstateEvent

Re-exports [UpdateEstateEvent](lib_Estate_UpdateEstate.md#updateestateevent)

___

### ValidateEstate

Re-exports [ValidateEstate](lib_Estate_ValidateEstate.md#validateestate)
