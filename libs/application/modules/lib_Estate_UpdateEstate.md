[client](../../../index.md) / [Exports](../modules.md) / lib/Estate/UpdateEstate

# Module: lib/Estate/UpdateEstate

## Table of contents

### Type aliases

- [UpdateEstateEvent](lib_Estate_UpdateEstate.md#updateestateevent)

### Functions

- [UpdateEstate](lib_Estate_UpdateEstate.md#updateestate)

## Type aliases

### UpdateEstateEvent

Ƭ **UpdateEstateEvent**: { `estate`: `EstateModel` ; `type`: ``"data"``  } \| [`ErrorUnion`](lib_Types_ErrorUnion.md#errorunion)

Event with details of the result of the UpdateEstate
workflow.

#### Defined in

[application/src/lib/Estate/UpdateEstate.ts:11](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Estate/UpdateEstate.ts#lines-11)

## Functions

### UpdateEstate

▸ **UpdateEstate**(`estate`): `Promise`<[`UpdateEstateEvent`](lib_Estate_UpdateEstate.md#updateestateevent)\>

Workflow to update an estate.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `estate` | `EstateModel` | The estate. |

#### Returns

`Promise`<[`UpdateEstateEvent`](lib_Estate_UpdateEstate.md#updateestateevent)\>

UpdateEstateEvent with details of if updating the estate was successful.

#### Defined in

[application/src/lib/Estate/UpdateEstate.ts:45](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Estate/UpdateEstate.ts#lines-45)
