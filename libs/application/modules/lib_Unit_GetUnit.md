[client](../../../index.md) / [Exports](../modules.md) / lib/Unit/GetUnit

# Module: lib/Unit/GetUnit

## Table of contents

### Type aliases

- [GetUnitEvent](lib_Unit_GetUnit.md#getunitevent)

### Functions

- [GetUnit](lib_Unit_GetUnit.md#getunit)

## Type aliases

### GetUnitEvent

Ƭ **GetUnitEvent**: { `type`: ``"data"`` ; `unit`: `UnitModel` \| ``null``  } \| [`ErrorUnion`](lib_Types_ErrorUnion.md#errorunion)

Event with details of the result of the GetUnit
workflow.

#### Defined in

[application/src/lib/Unit/GetUnit.ts:10](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Unit/GetUnit.ts#lines-10)

## Functions

### GetUnit

▸ **GetUnit**(`id`): `Promise`<[`GetUnitEvent`](lib_Unit_GetUnit.md#getunitevent)\>

Workflow to get an existing unit.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `id` | `string` | The unit id. |

#### Returns

`Promise`<[`GetUnitEvent`](lib_Unit_GetUnit.md#getunitevent)\>

GetUnitEvent with details of getting the building.

#### Defined in

[application/src/lib/Unit/GetUnit.ts:51](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Unit/GetUnit.ts#lines-51)
