[client](../../../index.md) / [Exports](../modules.md) / [lib/Types/ValidationError](../modules/lib_Types_ValidationError.md) / ValidationError

# Class: ValidationError

[lib/Types/ValidationError](../modules/lib_Types_ValidationError.md).ValidationError

## Hierarchy

- `OptionError`

  ↳ **`ValidationError`**

## Table of contents

### Constructors

- [constructor](lib_Types_ValidationError.ValidationError.md#constructor)

### Accessors

- [FieldErrors](lib_Types_ValidationError.ValidationError.md#fielderrors)
- [Message](lib_Types_ValidationError.ValidationError.md#message)

## Constructors

### constructor

• **new ValidationError**(`message?`, `_fieldErrors?`)

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `message` | `string` | `""` |
| `_fieldErrors` | `OptionError`[] | `[]` |

#### Overrides

OptionError.constructor

#### Defined in

[application/src/lib/Types/ValidationError.ts:4](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Types/ValidationError.ts#lines-4)

## Accessors

### FieldErrors

• `get` **FieldErrors**(): `OptionError`[]

#### Returns

`OptionError`[]

#### Defined in

[application/src/lib/Types/ValidationError.ts:8](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/application/src/lib/Types/ValidationError.ts#lines-8)

___

### Message

• `get` **Message**(): `string`

#### Returns

`string`

#### Inherited from

OptionError.Message

#### Defined in

[options/src/lib/options.ts:93](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/options/src/lib/options.ts#lines-93)
