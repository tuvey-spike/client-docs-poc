[client](../../index.md) / Exports

# client

## Table of contents

### Modules

- [jest.config](modules/jest_config.md)
- [src](modules/src.md)
- [src/lib/Admin](modules/src_lib_Admin.md)
- [src/lib/Admin/Hooks](modules/src_lib_Admin_Hooks.md)
- [src/lib/Admin/Hooks/useBuilding](modules/src_lib_Admin_Hooks_useBuilding.md)
- [src/lib/Admin/Hooks/useBuildings](modules/src_lib_Admin_Hooks_useBuildings.md)
- [src/lib/Admin/Hooks/useEstate](modules/src_lib_Admin_Hooks_useEstate.md)
- [src/lib/Admin/Hooks/useEstates](modules/src_lib_Admin_Hooks_useEstates.md)
- [src/lib/Admin/Hooks/useUnit](modules/src_lib_Admin_Hooks_useUnit.md)
- [src/lib/Admin/Hooks/useUnits](modules/src_lib_Admin_Hooks_useUnits.md)
- [src/lib/Layout](modules/src_lib_Layout.md)
- [src/lib/Layout/DataGrid](modules/src_lib_Layout_DataGrid.md)
- [src/lib/Layout/DataGrid/DataGridColumn](modules/src_lib_Layout_DataGrid_DataGridColumn.md)
- [src/lib/Layout/DataGrid/DataGridRow](modules/src_lib_Layout_DataGrid_DataGridRow.md)
