[client](../../../index.md) / [Exports](../modules.md) / src/lib/Layout/DataGrid/DataGridRow

# Module: src/lib/Layout/DataGrid/DataGridRow

## Table of contents

### Interfaces

- [IDataGridRowProps](../interfaces/src_lib_Layout_DataGrid_DataGridRow.IDataGridRowProps.md)

### Functions

- [DataGridRow](src_lib_Layout_DataGrid_DataGridRow.md#datagridrow)

## Functions

### DataGridRow

▸ **DataGridRow**<`TData`\>(`props`): `Element`

#### Type parameters

| Name |
| :------ |
| `TData` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `props` | [`IDataGridRowProps`](../interfaces/src_lib_Layout_DataGrid_DataGridRow.IDataGridRowProps.md)<`TData`\> |

#### Returns

`Element`

#### Defined in

[src/lib/Layout/DataGrid/DataGridRow/index.tsx:10](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/react-application/src/lib/Layout/DataGrid/DataGridRow/index.tsx#lines-10)
