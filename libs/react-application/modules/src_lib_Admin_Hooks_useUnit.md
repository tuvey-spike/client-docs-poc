[client](../../../index.md) / [Exports](../modules.md) / src/lib/Admin/Hooks/useUnit

# Module: src/lib/Admin/Hooks/useUnit

## Table of contents

### Functions

- [useUnit](src_lib_Admin_Hooks_useUnit.md#useunit)

## Functions

### useUnit

▸ **useUnit**(`id?`): `Object`

#### Parameters

| Name | Type |
| :------ | :------ |
| `id?` | `string` |

#### Returns

`Object`

| Name | Type |
| :------ | :------ |
| `createUnit` | (`unit`: `UnitModel`) => `Promise`<`CreateUnitEvent`\> |
| `getUnit` | (`id`: `string`) => `Promise`<`GetUnitEvent`\> |
| `unit` | ``null`` \| `UnitModel` |
| `updateUnit` | (`unit`: `UnitModel`) => `Promise`<`UpdateUnitEvent`\> |

#### Defined in

[src/lib/Admin/Hooks/useUnit.ts:22](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/react-application/src/lib/Admin/Hooks/useUnit.ts#lines-22)
