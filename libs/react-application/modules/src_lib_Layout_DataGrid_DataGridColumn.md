[client](../../../index.md) / [Exports](../modules.md) / src/lib/Layout/DataGrid/DataGridColumn

# Module: src/lib/Layout/DataGrid/DataGridColumn

## Table of contents

### Interfaces

- [IDataGridColumn](../interfaces/src_lib_Layout_DataGrid_DataGridColumn.IDataGridColumn.md)
