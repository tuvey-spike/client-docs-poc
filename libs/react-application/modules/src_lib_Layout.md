[client](../../../index.md) / [Exports](../modules.md) / src/lib/Layout

# Module: src/lib/Layout

## Table of contents

### References

- [DataGrid](src_lib_Layout.md#datagrid)
- [IDataGridColumn](src_lib_Layout.md#idatagridcolumn)
- [IDataGridProps](src_lib_Layout.md#idatagridprops)

## References

### DataGrid

Re-exports [DataGrid](src_lib_Layout_DataGrid.md#datagrid)

___

### IDataGridColumn

Re-exports [IDataGridColumn](../interfaces/src_lib_Layout_DataGrid_DataGridColumn.IDataGridColumn.md)

___

### IDataGridProps

Re-exports [IDataGridProps](../interfaces/src_lib_Layout_DataGrid.IDataGridProps.md)
