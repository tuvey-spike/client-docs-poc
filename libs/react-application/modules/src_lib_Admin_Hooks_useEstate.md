[client](../../../index.md) / [Exports](../modules.md) / src/lib/Admin/Hooks/useEstate

# Module: src/lib/Admin/Hooks/useEstate

## Table of contents

### Functions

- [useEstate](src_lib_Admin_Hooks_useEstate.md#useestate)

## Functions

### useEstate

▸ `Const` **useEstate**(`id`): `Object`

#### Parameters

| Name | Type |
| :------ | :------ |
| `id` | `string` |

#### Returns

`Object`

| Name | Type |
| :------ | :------ |
| `createEstate` | (`estate`: `EstateModel`) => `Promise`<`CreateEstateEvent`\> |
| `estate` | ``null`` \| `EstateModel` |
| `getEstate` | (`id`: `string`) => `Promise`<`GetEstateEvent`\> |
| `updateEstate` | (`estate`: `EstateModel`) => `Promise`<`UpdateEstateEvent`\> |

#### Defined in

[src/lib/Admin/Hooks/useEstate.ts:16](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/react-application/src/lib/Admin/Hooks/useEstate.ts#lines-16)
