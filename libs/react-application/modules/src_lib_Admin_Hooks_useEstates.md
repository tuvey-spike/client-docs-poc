[client](../../../index.md) / [Exports](../modules.md) / src/lib/Admin/Hooks/useEstates

# Module: src/lib/Admin/Hooks/useEstates

## Table of contents

### Functions

- [useEstates](src_lib_Admin_Hooks_useEstates.md#useestates)

## Functions

### useEstates

▸ `Const` **useEstates**(): `Object`

#### Returns

`Object`

| Name | Type |
| :------ | :------ |
| `estates` | `PagedResult`<`EstateModel`\> |
| `getEstates` | (`start`: `number`, `count`: `number`) => `Promise`<`GetEstatesEvent`\> |
| `nextPage` | () => `void` |

#### Defined in

[src/lib/Admin/Hooks/useEstates.ts:14](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/react-application/src/lib/Admin/Hooks/useEstates.ts#lines-14)
