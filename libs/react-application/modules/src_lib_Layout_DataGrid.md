[client](../../../index.md) / [Exports](../modules.md) / src/lib/Layout/DataGrid

# Module: src/lib/Layout/DataGrid

## Table of contents

### References

- [IDataGridColumn](src_lib_Layout_DataGrid.md#idatagridcolumn)

### Interfaces

- [IDataGridProps](../interfaces/src_lib_Layout_DataGrid.IDataGridProps.md)

### Functions

- [DataGrid](src_lib_Layout_DataGrid.md#datagrid)

## References

### IDataGridColumn

Re-exports [IDataGridColumn](../interfaces/src_lib_Layout_DataGrid_DataGridColumn.IDataGridColumn.md)

## Functions

### DataGrid

▸ **DataGrid**<`TData`\>(`props`): `Element`

#### Type parameters

| Name |
| :------ |
| `TData` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `props` | [`IDataGridProps`](../interfaces/src_lib_Layout_DataGrid.IDataGridProps.md)<`TData`\> |

#### Returns

`Element`

#### Defined in

[src/lib/Layout/DataGrid/index.tsx:25](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/react-application/src/lib/Layout/DataGrid/index.tsx#lines-25)
