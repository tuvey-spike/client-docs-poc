[client](../../../index.md) / [Exports](../modules.md) / src/lib/Admin/Hooks/useBuilding

# Module: src/lib/Admin/Hooks/useBuilding

## Table of contents

### Functions

- [useBuilding](src_lib_Admin_Hooks_useBuilding.md#usebuilding)

## Functions

### useBuilding

▸ **useBuilding**(`id?`): `Object`

#### Parameters

| Name | Type |
| :------ | :------ |
| `id?` | `string` |

#### Returns

`Object`

| Name | Type |
| :------ | :------ |
| `building` | ``null`` \| `BuildingModel` |
| `createBuilding` | (`building`: `BuildingModel`) => `Promise`<`CreateBuildingEvent`\> |
| `getBuilding` | (`id`: `string`) => `Promise`<`GetBuildingEvent`\> |
| `updateBuilding` | (`building`: `BuildingModel`) => `Promise`<`UpdateBuildingEvent`\> |

#### Defined in

[src/lib/Admin/Hooks/useBuilding.ts:17](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/react-application/src/lib/Admin/Hooks/useBuilding.ts#lines-17)
