[client](../../../index.md) / [Exports](../modules.md) / src/lib/Admin

# Module: src/lib/Admin

## Table of contents

### References

- [useBuilding](src_lib_Admin.md#usebuilding)
- [useBuildingsForEstate](src_lib_Admin.md#usebuildingsforestate)
- [useEstate](src_lib_Admin.md#useestate)
- [useEstates](src_lib_Admin.md#useestates)
- [useUnit](src_lib_Admin.md#useunit)
- [useUnitsForBuilding](src_lib_Admin.md#useunitsforbuilding)

## References

### useBuilding

Re-exports [useBuilding](src_lib_Admin_Hooks_useBuilding.md#usebuilding)

___

### useBuildingsForEstate

Re-exports [useBuildingsForEstate](src_lib_Admin_Hooks_useBuildings.md#usebuildingsforestate)

___

### useEstate

Re-exports [useEstate](src_lib_Admin_Hooks_useEstate.md#useestate)

___

### useEstates

Re-exports [useEstates](src_lib_Admin_Hooks_useEstates.md#useestates)

___

### useUnit

Re-exports [useUnit](src_lib_Admin_Hooks_useUnit.md#useunit)

___

### useUnitsForBuilding

Re-exports [useUnitsForBuilding](src_lib_Admin_Hooks_useUnits.md#useunitsforbuilding)
