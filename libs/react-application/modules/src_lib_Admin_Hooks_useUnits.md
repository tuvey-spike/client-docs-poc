[client](../../../index.md) / [Exports](../modules.md) / src/lib/Admin/Hooks/useUnits

# Module: src/lib/Admin/Hooks/useUnits

## Table of contents

### Functions

- [useUnitsForBuilding](src_lib_Admin_Hooks_useUnits.md#useunitsforbuilding)

## Functions

### useUnitsForBuilding

▸ `Const` **useUnitsForBuilding**(`buildingId`): `Object`

#### Parameters

| Name | Type |
| :------ | :------ |
| `buildingId` | `undefined` \| ``null`` \| `string` |

#### Returns

`Object`

| Name | Type |
| :------ | :------ |
| `getUnits` | (`start`: `number`, `count`: `number`) => `Promise`<`GetUnitsEvent`\> |
| `nextPage` | () => `void` |
| `units` | `PagedResult`<`UnitModel`\> |

#### Defined in

[src/lib/Admin/Hooks/useUnits.ts:14](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/react-application/src/lib/Admin/Hooks/useUnits.ts#lines-14)
