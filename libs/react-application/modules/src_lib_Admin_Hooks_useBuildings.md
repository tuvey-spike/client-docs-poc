[client](../../../index.md) / [Exports](../modules.md) / src/lib/Admin/Hooks/useBuildings

# Module: src/lib/Admin/Hooks/useBuildings

## Table of contents

### Functions

- [useBuildingsForEstate](src_lib_Admin_Hooks_useBuildings.md#usebuildingsforestate)

## Functions

### useBuildingsForEstate

▸ `Const` **useBuildingsForEstate**(`estateId`): `Object`

#### Parameters

| Name | Type |
| :------ | :------ |
| `estateId` | `undefined` \| ``null`` \| `string` |

#### Returns

`Object`

| Name | Type |
| :------ | :------ |
| `buildings` | `PagedResult`<`BuildingModel`\> |
| `getBuildings` | (`start`: `number`, `count`: `number`) => `Promise`<`GetBuildingsEvent`\> |
| `nextPage` | () => `void` |

#### Defined in

[src/lib/Admin/Hooks/useBuildings.ts:14](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/react-application/src/lib/Admin/Hooks/useBuildings.ts#lines-14)
