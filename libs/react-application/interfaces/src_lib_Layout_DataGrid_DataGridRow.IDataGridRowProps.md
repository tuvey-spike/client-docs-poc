[client](../../../index.md) / [Exports](../modules.md) / [src/lib/Layout/DataGrid/DataGridRow](../modules/src_lib_Layout_DataGrid_DataGridRow.md) / IDataGridRowProps

# Interface: IDataGridRowProps<TData\>

[src/lib/Layout/DataGrid/DataGridRow](../modules/src_lib_Layout_DataGrid_DataGridRow.md).IDataGridRowProps

## Type parameters

| Name |
| :------ |
| `TData` |

## Table of contents

### Properties

- [columns](src_lib_Layout_DataGrid_DataGridRow.IDataGridRowProps.md#columns)
- [row](src_lib_Layout_DataGrid_DataGridRow.IDataGridRowProps.md#row)
- [selected](src_lib_Layout_DataGrid_DataGridRow.IDataGridRowProps.md#selected)

### Methods

- [onClick](src_lib_Layout_DataGrid_DataGridRow.IDataGridRowProps.md#onclick)

## Properties

### columns

• **columns**: [`IDataGridColumn`](src_lib_Layout_DataGrid_DataGridColumn.IDataGridColumn.md)<`TData`\>[]

#### Defined in

[src/lib/Layout/DataGrid/DataGridRow/index.tsx:5](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/react-application/src/lib/Layout/DataGrid/DataGridRow/index.tsx#lines-5)

___

### row

• **row**: `TData`

#### Defined in

[src/lib/Layout/DataGrid/DataGridRow/index.tsx:4](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/react-application/src/lib/Layout/DataGrid/DataGridRow/index.tsx#lines-4)

___

### selected

• **selected**: `boolean`

#### Defined in

[src/lib/Layout/DataGrid/DataGridRow/index.tsx:7](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/react-application/src/lib/Layout/DataGrid/DataGridRow/index.tsx#lines-7)

## Methods

### onClick

▸ `Optional` **onClick**(`row`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `row` | `TData` |

#### Returns

`void`

#### Defined in

[src/lib/Layout/DataGrid/DataGridRow/index.tsx:6](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/react-application/src/lib/Layout/DataGrid/DataGridRow/index.tsx#lines-6)
