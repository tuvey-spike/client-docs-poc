[client](../../../index.md) / [Exports](../modules.md) / [src/lib/Layout/DataGrid](../modules/src_lib_Layout_DataGrid.md) / IDataGridProps

# Interface: IDataGridProps<TData\>

[src/lib/Layout/DataGrid](../modules/src_lib_Layout_DataGrid.md).IDataGridProps

## Type parameters

| Name |
| :------ |
| `TData` |

## Table of contents

### Properties

- [columns](src_lib_Layout_DataGrid.IDataGridProps.md#columns)
- [data](src_lib_Layout_DataGrid.IDataGridProps.md#data)

### Methods

- [action](src_lib_Layout_DataGrid.IDataGridProps.md#action)
- [onPageEnd](src_lib_Layout_DataGrid.IDataGridProps.md#onpageend)

## Properties

### columns

• **columns**: [`IDataGridColumn`](src_lib_Layout_DataGrid_DataGridColumn.IDataGridColumn.md)<`TData`\>[]

#### Defined in

[src/lib/Layout/DataGrid/index.tsx:20](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/react-application/src/lib/Layout/DataGrid/index.tsx#lines-20)

___

### data

• **data**: `TData`[]

#### Defined in

[src/lib/Layout/DataGrid/index.tsx:19](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/react-application/src/lib/Layout/DataGrid/index.tsx#lines-19)

## Methods

### action

▸ `Optional` **action**(`row`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `row` | `TData` |

#### Returns

`void`

#### Defined in

[src/lib/Layout/DataGrid/index.tsx:21](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/react-application/src/lib/Layout/DataGrid/index.tsx#lines-21)

___

### onPageEnd

▸ `Optional` **onPageEnd**(): `void`

#### Returns

`void`

#### Defined in

[src/lib/Layout/DataGrid/index.tsx:22](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/react-application/src/lib/Layout/DataGrid/index.tsx#lines-22)
