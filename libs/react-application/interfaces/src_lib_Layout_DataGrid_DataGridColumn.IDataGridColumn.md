[client](../../../index.md) / [Exports](../modules.md) / [src/lib/Layout/DataGrid/DataGridColumn](../modules/src_lib_Layout_DataGrid_DataGridColumn.md) / IDataGridColumn

# Interface: IDataGridColumn<TData\>

[src/lib/Layout/DataGrid/DataGridColumn](../modules/src_lib_Layout_DataGrid_DataGridColumn.md).IDataGridColumn

## Type parameters

| Name |
| :------ |
| `TData` |

## Table of contents

### Properties

- [field](src_lib_Layout_DataGrid_DataGridColumn.IDataGridColumn.md#field)
- [hide](src_lib_Layout_DataGrid_DataGridColumn.IDataGridColumn.md#hide)
- [name](src_lib_Layout_DataGrid_DataGridColumn.IDataGridColumn.md#name)
- [width](src_lib_Layout_DataGrid_DataGridColumn.IDataGridColumn.md#width)

### Methods

- [renderCell](src_lib_Layout_DataGrid_DataGridColumn.IDataGridColumn.md#rendercell)

## Properties

### field

• **field**: `string`

#### Defined in

[src/lib/Layout/DataGrid/DataGridColumn.ts:3](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/react-application/src/lib/Layout/DataGrid/DataGridColumn.ts#lines-3)

___

### hide

• `Optional` **hide**: `boolean`

#### Defined in

[src/lib/Layout/DataGrid/DataGridColumn.ts:2](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/react-application/src/lib/Layout/DataGrid/DataGridColumn.ts#lines-2)

___

### name

• **name**: `string`

#### Defined in

[src/lib/Layout/DataGrid/DataGridColumn.ts:4](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/react-application/src/lib/Layout/DataGrid/DataGridColumn.ts#lines-4)

___

### width

• `Optional` **width**: `number`

#### Defined in

[src/lib/Layout/DataGrid/DataGridColumn.ts:5](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/react-application/src/lib/Layout/DataGrid/DataGridColumn.ts#lines-5)

## Methods

### renderCell

▸ `Optional` **renderCell**(`row`): `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `row` | `TData` |

#### Returns

`Element`

#### Defined in

[src/lib/Layout/DataGrid/DataGridColumn.ts:6](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/react-application/src/lib/Layout/DataGrid/DataGridColumn.ts#lines-6)
