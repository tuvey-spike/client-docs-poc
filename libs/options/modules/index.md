[client](../../../index.md) / [Exports](../modules.md) / index

# Module: index

## Table of contents

### References

- [Option](index.md#option)
- [OptionError](index.md#optionerror)
- [OptionErrorException](index.md#optionerrorexception)
- [OptionValueException](index.md#optionvalueexception)

## References

### Option

Re-exports [Option](../classes/lib_options.Option.md)

___

### OptionError

Re-exports [OptionError](../classes/lib_options.OptionError.md)

___

### OptionErrorException

Re-exports [OptionErrorException](../classes/lib_options.OptionErrorException.md)

___

### OptionValueException

Re-exports [OptionValueException](../classes/lib_options.OptionValueException.md)
