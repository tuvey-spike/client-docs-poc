[client](../../../index.md) / [Exports](../modules.md) / lib/options

# Module: lib/options

## Table of contents

### Classes

- [Option](../classes/lib_options.Option.md)
- [OptionError](../classes/lib_options.OptionError.md)
- [OptionErrorException](../classes/lib_options.OptionErrorException.md)
- [OptionValueException](../classes/lib_options.OptionValueException.md)
