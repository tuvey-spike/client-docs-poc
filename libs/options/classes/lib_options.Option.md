[client](../../../index.md) / [Exports](../modules.md) / [lib/options](../modules/lib_options.md) / Option

# Class: Option<TValue, TError\>

[lib/options](../modules/lib_options.md).Option

## Type parameters

| Name | Type |
| :------ | :------ |
| `TValue` | `TValue` |
| `TError` | extends [`OptionError`](lib_options.OptionError.md) |

## Table of contents

### Constructors

- [constructor](lib_options.Option.md#constructor)

### Properties

- [\_error](lib_options.Option.md#_error)
- [\_hasValue](lib_options.Option.md#_hasvalue)
- [\_value](lib_options.Option.md#_value)

### Accessors

- [Error](lib_options.Option.md#error)
- [Success](lib_options.Option.md#success)
- [Value](lib_options.Option.md#value)

### Methods

- [Finally](lib_options.Option.md#finally)
- [Then](lib_options.Option.md#then)
- [TryGetValue](lib_options.Option.md#trygetvalue)
- [ValueOrThrow](lib_options.Option.md#valueorthrow)
- [Fail](lib_options.Option.md#fail)
- [Ok](lib_options.Option.md#ok)

## Constructors

### constructor

• **new Option**<`TValue`, `TError`\>(`value`, `error`, `hasValue`)

#### Type parameters

| Name | Type |
| :------ | :------ |
| `TValue` | `TValue` |
| `TError` | extends [`OptionError`](lib_options.OptionError.md)<`TError`\> |

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | ``null`` \| `TValue` |
| `error` | ``null`` \| `TError` |
| `hasValue` | `boolean` |

#### Defined in

[lib/options.ts:7](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/options/src/lib/options.ts#lines-7)

## Properties

### \_error

• `Private` **\_error**: ``null`` \| `TError` = `null`

#### Defined in

[lib/options.ts:4](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/options/src/lib/options.ts#lines-4)

___

### \_hasValue

• `Private` **\_hasValue**: `boolean` = `false`

#### Defined in

[lib/options.ts:5](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/options/src/lib/options.ts#lines-5)

___

### \_value

• `Private` **\_value**: ``null`` \| `TValue` = `null`

#### Defined in

[lib/options.ts:3](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/options/src/lib/options.ts#lines-3)

## Accessors

### Error

• `get` **Error**(): `TError`

#### Returns

`TError`

#### Defined in

[lib/options.ts:24](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/options/src/lib/options.ts#lines-24)

___

### Success

• `get` **Success**(): `boolean`

#### Returns

`boolean`

#### Defined in

[lib/options.ts:20](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/options/src/lib/options.ts#lines-20)

___

### Value

• `get` **Value**(): ``null`` \| `TValue`

#### Returns

``null`` \| `TValue`

#### Defined in

[lib/options.ts:31](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/options/src/lib/options.ts#lines-31)

## Methods

### Finally

▸ **Finally**<`TResult`\>(`some`, `none`): `TResult`

#### Type parameters

| Name |
| :------ |
| `TResult` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `some` | (`_`: `TValue`) => `TResult` |
| `none` | (`_`: `TError`) => `TResult` |

#### Returns

`TResult`

#### Defined in

[lib/options.ts:59](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/options/src/lib/options.ts#lines-59)

___

### Then

▸ **Then**<`TOut`\>(`next`): `any`

#### Type parameters

| Name |
| :------ |
| `TOut` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `next` | (`_`: `TValue`) => [`Option`](lib_options.Option.md)<`TOut`, `TError`\> |

#### Returns

`any`

#### Defined in

[lib/options.ts:46](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/options/src/lib/options.ts#lines-46)

___

### TryGetValue

▸ **TryGetValue**(`out`): `boolean`

#### Parameters

| Name | Type |
| :------ | :------ |
| `out` | (`value`: ``null`` \| `TValue`) => `void` |

#### Returns

`boolean`

#### Defined in

[lib/options.ts:71](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/options/src/lib/options.ts#lines-71)

___

### ValueOrThrow

▸ **ValueOrThrow**(`message?`): ``null`` \| `TValue`

#### Parameters

| Name | Type |
| :------ | :------ |
| `message?` | `string` |

#### Returns

``null`` \| `TValue`

#### Defined in

[lib/options.ts:80](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/options/src/lib/options.ts#lines-80)

___

### Fail

▸ `Static` **Fail**<`TValue`, `TError`\>(`error`): [`Option`](lib_options.Option.md)<`TValue`, `TError`\>

#### Type parameters

| Name | Type |
| :------ | :------ |
| `TValue` | `TValue` |
| `TError` | extends [`OptionError`](lib_options.OptionError.md)<`TError`\> |

#### Parameters

| Name | Type |
| :------ | :------ |
| `error` | `TError` |

#### Returns

[`Option`](lib_options.Option.md)<`TValue`, `TError`\>

#### Defined in

[lib/options.ts:42](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/options/src/lib/options.ts#lines-42)

___

### Ok

▸ `Static` **Ok**<`TValue`, `TError`\>(`value`): [`Option`](lib_options.Option.md)<`TValue`, `TError`\>

#### Type parameters

| Name | Type |
| :------ | :------ |
| `TValue` | `TValue` |
| `TError` | extends [`OptionError`](lib_options.OptionError.md)<`TError`\> |

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `TValue` |

#### Returns

[`Option`](lib_options.Option.md)<`TValue`, `TError`\>

#### Defined in

[lib/options.ts:38](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/options/src/lib/options.ts#lines-38)
