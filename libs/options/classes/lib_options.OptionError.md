[client](../../../index.md) / [Exports](../modules.md) / [lib/options](../modules/lib_options.md) / OptionError

# Class: OptionError

[lib/options](../modules/lib_options.md).OptionError

## Table of contents

### Constructors

- [constructor](lib_options.OptionError.md#constructor)

### Accessors

- [Message](lib_options.OptionError.md#message)

## Constructors

### constructor

• **new OptionError**(`_message?`)

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `_message` | `string` | `""` |

#### Defined in

[lib/options.ts:90](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/options/src/lib/options.ts#lines-90)

## Accessors

### Message

• `get` **Message**(): `string`

#### Returns

`string`

#### Defined in

[lib/options.ts:93](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/options/src/lib/options.ts#lines-93)
