[client](../../../index.md) / [Exports](../modules.md) / [lib/options](../modules/lib_options.md) / OptionErrorException

# Class: OptionErrorException

[lib/options](../modules/lib_options.md).OptionErrorException

## Hierarchy

- `Error`

  ↳ **`OptionErrorException`**

## Table of contents

### Constructors

- [constructor](lib_options.OptionErrorException.md#constructor)

## Constructors

### constructor

• **new OptionErrorException**(`message?`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `message?` | `string` |

#### Overrides

Error.constructor

#### Defined in

[lib/options.ts:105](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/options/src/lib/options.ts#lines-105)
