[client](../../../index.md) / [Exports](../modules.md) / [lib/options](../modules/lib_options.md) / OptionValueException

# Class: OptionValueException

[lib/options](../modules/lib_options.md).OptionValueException

## Hierarchy

- `Error`

  ↳ **`OptionValueException`**

## Table of contents

### Constructors

- [constructor](lib_options.OptionValueException.md#constructor)

## Constructors

### constructor

• **new OptionValueException**(`message?`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `message?` | `string` |

#### Overrides

Error.constructor

#### Defined in

[lib/options.ts:99](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/libs/options/src/lib/options.ts#lines-99)
