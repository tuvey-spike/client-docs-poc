
# Unite 2 Nx Client Workspace

Unite 2 client code is arranged in an Nx Workspace, allowing the development of packages alongside apps. Multiple apps exists in the apps/ folder. Supporting libraries and packages shared between the apps existing in libs/

See https://nx.dev/ for more info on Nx.

# Client Docker

### How to run

```yarn docker```

### Reference for setting up

https://medium.com/swlh/nx-nestjs-react-docker-deploys-928a55fc19fd


# Libraries
- [api](libs/api/modules.md)
- [application](libs/application/modules.md)
- [dashboard](apps/dashboard/modules.md)
- [foundational-react-components](libs/foundational-react-components/modules.md)
- [options](libs/options/modules.md)
- [react-application](libs/react-application/modules.md)
