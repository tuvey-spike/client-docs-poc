[client](../../../index.md) / [Exports](../modules.md) / src/app/Components/Layout/Tabs

# Module: src/app/Components/Layout/Tabs

## Table of contents

### References

- [ITabProps](src_app_Components_Layout_Tabs.md#itabprops)
- [ITabsProps](src_app_Components_Layout_Tabs.md#itabsprops)
- [Tab](src_app_Components_Layout_Tabs.md#tab)
- [Tabs](src_app_Components_Layout_Tabs.md#tabs)

## References

### ITabProps

Re-exports [ITabProps](../interfaces/src_app_Components_Layout_Tabs_Tab.ITabProps.md)

___

### ITabsProps

Re-exports [ITabsProps](../interfaces/src_app_Components_Layout_Tabs_Tabs.ITabsProps.md)

___

### Tab

Re-exports [Tab](src_app_Components_Layout_Tabs_Tab.md#tab)

___

### Tabs

Re-exports [Tabs](src_app_Components_Layout_Tabs_Tabs.md#tabs)
