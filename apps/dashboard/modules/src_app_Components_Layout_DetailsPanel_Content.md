[client](../../../index.md) / [Exports](../modules.md) / src/app/Components/Layout/DetailsPanel/Content

# Module: src/app/Components/Layout/DetailsPanel/Content

## Table of contents

### References

- [GetEstateContent](src_app_Components_Layout_DetailsPanel_Content.md#getestatecontent)

## References

### GetEstateContent

Re-exports [GetEstateContent](src_app_Components_Layout_DetailsPanel_Content_EstateContent.md#getestatecontent)
