[client](../../../index.md) / [Exports](../modules.md) / src/app/Components/Layout/Header/HeaderLink

# Module: src/app/Components/Layout/Header/HeaderLink

## Table of contents

### Interfaces

- [IHeaderLinkMenuItem](../interfaces/src_app_Components_Layout_Header_HeaderLink.IHeaderLinkMenuItem.md)
- [IHeaderLinkProps](../interfaces/src_app_Components_Layout_Header_HeaderLink.IHeaderLinkProps.md)

### Functions

- [HeaderLink](src_app_Components_Layout_Header_HeaderLink.md#headerlink)

## Functions

### HeaderLink

▸ **HeaderLink**(`props`): `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `props` | [`IHeaderLinkProps`](../interfaces/src_app_Components_Layout_Header_HeaderLink.IHeaderLinkProps.md) |

#### Returns

`Element`

#### Defined in

[src/app/Components/Layout/Header/HeaderLink/index.tsx:26](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Header/HeaderLink/index.tsx#lines-26)
