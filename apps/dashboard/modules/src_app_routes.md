[client](../../../index.md) / [Exports](../modules.md) / src/app/routes

# Module: src/app/routes

## Table of contents

### References

- [default](src_app_routes.md#default)

### Functions

- [Routes](src_app_routes.md#routes)

## References

### default

Renames and re-exports [Routes](src_app_routes.md#routes)

## Functions

### Routes

▸ **Routes**(): `Element`

#### Returns

`Element`

#### Defined in

[src/app/routes.tsx:24](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/routes.tsx#lines-24)
