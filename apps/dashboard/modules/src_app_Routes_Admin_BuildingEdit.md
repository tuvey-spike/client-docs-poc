[client](../../../index.md) / [Exports](../modules.md) / src/app/Routes/Admin/BuildingEdit

# Module: src/app/Routes/Admin/BuildingEdit

## Table of contents

### Interfaces

- [IBuildingEditLocationState](../interfaces/src_app_Routes_Admin_BuildingEdit.IBuildingEditLocationState.md)

### Functions

- [BuildingEdit](src_app_Routes_Admin_BuildingEdit.md#buildingedit)

## Functions

### BuildingEdit

▸ **BuildingEdit**(): `Element`

#### Returns

`Element`

#### Defined in

[src/app/Routes/Admin/BuildingEdit/index.tsx:23](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Admin/BuildingEdit/index.tsx#lines-23)
