[client](../../../index.md) / [Exports](../modules.md) / src/app/app

# Module: src/app/app

## Table of contents

### References

- [default](src_app_app.md#default)

### Functions

- [App](src_app_app.md#app)

## References

### default

Renames and re-exports [App](src_app_app.md#app)

## Functions

### App

▸ **App**(): `Element`

#### Returns

`Element`

#### Defined in

[src/app/app.tsx:17](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/app.tsx#lines-17)
