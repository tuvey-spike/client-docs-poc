[client](../../../index.md) / [Exports](../modules.md) / src/app/Components/Layout/Tabs/Tabs

# Module: src/app/Components/Layout/Tabs/Tabs

## Table of contents

### Interfaces

- [ITabsProps](../interfaces/src_app_Components_Layout_Tabs_Tabs.ITabsProps.md)

### Functions

- [Tabs](src_app_Components_Layout_Tabs_Tabs.md#tabs)

## Functions

### Tabs

▸ **Tabs**(`props`): `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `props` | [`ITabsProps`](../interfaces/src_app_Components_Layout_Tabs_Tabs.ITabsProps.md) |

#### Returns

`Element`

#### Defined in

[src/app/Components/Layout/Tabs/Tabs.tsx:34](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Tabs/Tabs.tsx#lines-34)
