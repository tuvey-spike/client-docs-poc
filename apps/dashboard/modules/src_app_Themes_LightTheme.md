[client](../../../index.md) / [Exports](../modules.md) / src/app/Themes/LightTheme

# Module: src/app/Themes/LightTheme

## Table of contents

### Variables

- [default](src_app_Themes_LightTheme.md#default)
- [lightPalette](src_app_Themes_LightTheme.md#lightpalette)

## Variables

### default

• **default**: `Theme`

#### Defined in

[src/app/Themes/LightTheme.ts:21](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Themes/LightTheme.ts#lines-21)

___

### lightPalette

• **lightPalette**: [`UnitePaletteOptions`](../interfaces/src_app_Themes_UniteTheme.UnitePaletteOptions.md)

#### Defined in

[src/app/Themes/LightTheme.ts:6](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Themes/LightTheme.ts#lines-6)
