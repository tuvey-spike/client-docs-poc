[client](../../../index.md) / [Exports](../modules.md) / src/app/Components/Layout/Containers/AppContainer

# Module: src/app/Components/Layout/Containers/AppContainer

## Table of contents

### Interfaces

- [IAppContainerProps](../interfaces/src_app_Components_Layout_Containers_AppContainer.IAppContainerProps.md)

### Functions

- [AppContainer](src_app_Components_Layout_Containers_AppContainer.md#appcontainer)

## Functions

### AppContainer

▸ **AppContainer**(`props`): `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `props` | [`IAppContainerProps`](../interfaces/src_app_Components_Layout_Containers_AppContainer.IAppContainerProps.md) |

#### Returns

`Element`

#### Defined in

[src/app/Components/Layout/Containers/AppContainer.tsx:17](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Containers/AppContainer.tsx#lines-17)
