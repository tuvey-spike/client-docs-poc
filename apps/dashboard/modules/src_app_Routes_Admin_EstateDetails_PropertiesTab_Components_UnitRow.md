[client](../../../index.md) / [Exports](../modules.md) / src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/UnitRow

# Module: src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/UnitRow

## Table of contents

### Interfaces

- [IUnitRowProps](../interfaces/src_app_Routes_Admin_EstateDetails_PropertiesTab_Components_UnitRow.IUnitRowProps.md)

### Functions

- [UnitRow](src_app_Routes_Admin_EstateDetails_PropertiesTab_Components_UnitRow.md#unitrow)

## Functions

### UnitRow

▸ **UnitRow**(`__namedParameters`): `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | [`IUnitRowProps`](../interfaces/src_app_Routes_Admin_EstateDetails_PropertiesTab_Components_UnitRow.IUnitRowProps.md) |

#### Returns

`Element`

#### Defined in

[src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/UnitRow/index.tsx:13](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/UnitRow/index.tsx#lines-13)
