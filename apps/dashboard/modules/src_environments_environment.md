[client](../../../index.md) / [Exports](../modules.md) / src/environments/environment

# Module: src/environments/environment

## Table of contents

### Variables

- [environment](src_environments_environment.md#environment)

## Variables

### environment

• **environment**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `production` | `boolean` |

#### Defined in

[src/environments/environment.ts:4](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/environments/environment.ts#lines-4)
