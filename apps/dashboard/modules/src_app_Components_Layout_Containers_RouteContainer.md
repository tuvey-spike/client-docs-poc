[client](../../../index.md) / [Exports](../modules.md) / src/app/Components/Layout/Containers/RouteContainer

# Module: src/app/Components/Layout/Containers/RouteContainer

## Table of contents

### Interfaces

- [IRouteContainerProps](../interfaces/src_app_Components_Layout_Containers_RouteContainer.IRouteContainerProps.md)

### Functions

- [RouteContainer](src_app_Components_Layout_Containers_RouteContainer.md#routecontainer)

## Functions

### RouteContainer

▸ **RouteContainer**(`props`): `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `props` | [`IRouteContainerProps`](../interfaces/src_app_Components_Layout_Containers_RouteContainer.IRouteContainerProps.md) |

#### Returns

`Element`

#### Defined in

[src/app/Components/Layout/Containers/RouteContainer.tsx:23](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Containers/RouteContainer.tsx#lines-23)
