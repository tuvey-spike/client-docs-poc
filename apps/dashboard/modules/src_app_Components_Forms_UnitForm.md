[client](../../../index.md) / [Exports](../modules.md) / src/app/Components/Forms/UnitForm

# Module: src/app/Components/Forms/UnitForm

## Table of contents

### Interfaces

- [IUnitFormProps](../interfaces/src_app_Components_Forms_UnitForm.IUnitFormProps.md)

### Functions

- [UnitForm](src_app_Components_Forms_UnitForm.md#unitform)

## Functions

### UnitForm

▸ **UnitForm**(`props`): `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `props` | [`IUnitFormProps`](../interfaces/src_app_Components_Forms_UnitForm.IUnitFormProps.md) |

#### Returns

`Element`

#### Defined in

[src/app/Components/Forms/UnitForm/index.tsx:51](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Forms/UnitForm/index.tsx#lines-51)
