[client](../../../index.md) / [Exports](../modules.md) / postcss.config

# Module: postcss.config

## Table of contents

### Namespaces

- [export&#x3D;](postcss_config.export_.md)

### Properties

- [export&#x3D;](postcss_config.md#export&#x3D;)

## Properties

### export&#x3D;

• **export=**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `plugins` | `Object` |
| `plugins.autoprefixer` | `Object` |

#### Defined in

[postcss.config.js:1](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/postcss.config.js#lines-1)
