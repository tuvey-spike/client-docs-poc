[client](../../../index.md) / [Exports](../modules.md) / src/app/Components/Layout/Sidebar

# Module: src/app/Components/Layout/Sidebar

## Table of contents

### Interfaces

- [ISidebarProps](../interfaces/src_app_Components_Layout_Sidebar.ISidebarProps.md)
- [SidebarLink](../interfaces/src_app_Components_Layout_Sidebar.SidebarLink.md)

### Functions

- [Sidebar](src_app_Components_Layout_Sidebar.md#sidebar)

## Functions

### Sidebar

▸ **Sidebar**(`props`): `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `props` | [`ISidebarProps`](../interfaces/src_app_Components_Layout_Sidebar.ISidebarProps.md) |

#### Returns

`Element`

#### Defined in

[src/app/Components/Layout/Sidebar/index.tsx:21](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Sidebar/index.tsx#lines-21)
