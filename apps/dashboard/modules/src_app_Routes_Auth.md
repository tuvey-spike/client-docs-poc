[client](../../../index.md) / [Exports](../modules.md) / src/app/Routes/Auth

# Module: src/app/Routes/Auth

## Table of contents

### Functions

- [default](src_app_Routes_Auth.md#default)

## Functions

### default

▸ **default**(): `Element`

#### Returns

`Element`

#### Defined in

[src/app/Routes/Auth/index.tsx:11](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Auth/index.tsx#lines-11)
