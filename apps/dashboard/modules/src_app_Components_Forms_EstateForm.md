[client](../../../index.md) / [Exports](../modules.md) / src/app/Components/Forms/EstateForm

# Module: src/app/Components/Forms/EstateForm

## Table of contents

### Interfaces

- [IEstateFormProps](../interfaces/src_app_Components_Forms_EstateForm.IEstateFormProps.md)

### Functions

- [EstateForm](src_app_Components_Forms_EstateForm.md#estateform)

## Functions

### EstateForm

▸ `Const` **EstateForm**(`__namedParameters`): `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | [`IEstateFormProps`](../interfaces/src_app_Components_Forms_EstateForm.IEstateFormProps.md) |

#### Returns

`Element`

#### Defined in

[src/app/Components/Forms/EstateForm/index.tsx:48](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Forms/EstateForm/index.tsx#lines-48)
