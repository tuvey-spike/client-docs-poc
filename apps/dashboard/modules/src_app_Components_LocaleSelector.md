[client](../../../index.md) / [Exports](../modules.md) / src/app/Components/LocaleSelector

# Module: src/app/Components/LocaleSelector

## Table of contents

### Functions

- [LocaleSelector](src_app_Components_LocaleSelector.md#localeselector)

## Functions

### LocaleSelector

▸ **LocaleSelector**(): `Element`

#### Returns

`Element`

#### Defined in

[src/app/Components/LocaleSelector/index.tsx:12](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/LocaleSelector/index.tsx#lines-12)
