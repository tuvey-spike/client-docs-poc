[client](../../../index.md) / [Exports](../modules.md) / src/app/Routes/Properties

# Module: src/app/Routes/Properties

## Table of contents

### Functions

- [default](src_app_Routes_Properties.md#default)

## Functions

### default

▸ **default**(): `Element`

#### Returns

`Element`

#### Defined in

[src/app/Routes/Properties/index.tsx:10](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Properties/index.tsx#lines-10)
