[client](../../../index.md) / [Exports](../modules.md) / src/app/Components/Layout/DetailsPanel/DetailsPanelSection

# Module: src/app/Components/Layout/DetailsPanel/DetailsPanelSection

## Table of contents

### Interfaces

- [IDetailsPanelSectionProps](../interfaces/src_app_Components_Layout_DetailsPanel_DetailsPanelSection.IDetailsPanelSectionProps.md)

### Functions

- [DetailsPanelSection](src_app_Components_Layout_DetailsPanel_DetailsPanelSection.md#detailspanelsection)

## Functions

### DetailsPanelSection

▸ **DetailsPanelSection**(`props`): `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `props` | [`IDetailsPanelSectionProps`](../interfaces/src_app_Components_Layout_DetailsPanel_DetailsPanelSection.IDetailsPanelSectionProps.md) |

#### Returns

`Element`

#### Defined in

[src/app/Components/Layout/DetailsPanel/DetailsPanelSection.tsx:18](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/DetailsPanel/DetailsPanelSection.tsx#lines-18)
