[client](../../../index.md) / [Exports](../modules.md) / src/app/Context/ThemeContext

# Module: src/app/Context/ThemeContext

## Table of contents

### Interfaces

- [IThemeContext](../interfaces/src_app_Context_ThemeContext.IThemeContext.md)
- [IThemeContextProviderProps](../interfaces/src_app_Context_ThemeContext.IThemeContextProviderProps.md)

### Type aliases

- [ThemeType](src_app_Context_ThemeContext.md#themetype)

### Functions

- [ThemeContextProvider](src_app_Context_ThemeContext.md#themecontextprovider)
- [useThemeContext](src_app_Context_ThemeContext.md#usethemecontext)

## Type aliases

### ThemeType

Ƭ **ThemeType**: ``"light"`` \| ``"dark"``

#### Defined in

[src/app/Context/ThemeContext.tsx:37](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Context/ThemeContext.tsx#lines-37)

## Functions

### ThemeContextProvider

▸ **ThemeContextProvider**(`__namedParameters`): `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | [`IThemeContextProviderProps`](../interfaces/src_app_Context_ThemeContext.IThemeContextProviderProps.md) |

#### Returns

`Element`

#### Defined in

[src/app/Context/ThemeContext.tsx:41](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Context/ThemeContext.tsx#lines-41)

___

### useThemeContext

▸ **useThemeContext**(): [`IThemeContext`](../interfaces/src_app_Context_ThemeContext.IThemeContext.md)

#### Returns

[`IThemeContext`](../interfaces/src_app_Context_ThemeContext.IThemeContext.md)

#### Defined in

[src/app/Context/ThemeContext.tsx:29](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Context/ThemeContext.tsx#lines-29)
