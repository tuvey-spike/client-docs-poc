[client](../../../index.md) / [Exports](../modules.md) / src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/BuildingRow

# Module: src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/BuildingRow

## Table of contents

### Interfaces

- [IBuildingRowProps](../interfaces/src_app_Routes_Admin_EstateDetails_PropertiesTab_Components_BuildingRow.IBuildingRowProps.md)

### Functions

- [BuildingRow](src_app_Routes_Admin_EstateDetails_PropertiesTab_Components_BuildingRow.md#buildingrow)

## Functions

### BuildingRow

▸ **BuildingRow**(`__namedParameters`): `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | [`IBuildingRowProps`](../interfaces/src_app_Routes_Admin_EstateDetails_PropertiesTab_Components_BuildingRow.IBuildingRowProps.md) |

#### Returns

`Element`

#### Defined in

[src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/BuildingRow/index.tsx:18](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/BuildingRow/index.tsx#lines-18)
