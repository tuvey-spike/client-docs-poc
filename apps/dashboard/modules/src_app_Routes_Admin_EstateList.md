[client](../../../index.md) / [Exports](../modules.md) / src/app/Routes/Admin/EstateList

# Module: src/app/Routes/Admin/EstateList

## Table of contents

### Functions

- [AdminEstateList](src_app_Routes_Admin_EstateList.md#adminestatelist)

## Functions

### AdminEstateList

▸ **AdminEstateList**(): `Element`

#### Returns

`Element`

#### Defined in

[src/app/Routes/Admin/EstateList/index.tsx:18](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Admin/EstateList/index.tsx#lines-18)
