[client](../../../index.md) / [Exports](../modules.md) / src/app/Routes/Admin/EstateDetails

# Module: src/app/Routes/Admin/EstateDetails

## Table of contents

### Interfaces

- [EstateDetailsParams](../interfaces/src_app_Routes_Admin_EstateDetails.EstateDetailsParams.md)

### Functions

- [EstateDetails](src_app_Routes_Admin_EstateDetails.md#estatedetails)

## Functions

### EstateDetails

▸ **EstateDetails**(): `Element`

#### Returns

`Element`

#### Defined in

[src/app/Routes/Admin/EstateDetails/index.tsx:21](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Admin/EstateDetails/index.tsx#lines-21)
