[client](../../../index.md) / [Exports](../modules.md) / src/app/Routes/Dashboard

# Module: src/app/Routes/Dashboard

## Table of contents

### Functions

- [default](src_app_Routes_Dashboard.md#default)

## Functions

### default

▸ **default**(): `Element`

#### Returns

`Element`

#### Defined in

[src/app/Routes/Dashboard/index.tsx:4](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Dashboard/index.tsx#lines-4)
