[client](../../../index.md) / [Exports](../modules.md) / src/app/Components/Controls/MenuButton

# Module: src/app/Components/Controls/MenuButton

## Table of contents

### Interfaces

- [IMenuButtonItem](../interfaces/src_app_Components_Controls_MenuButton.IMenuButtonItem.md)
- [IMenuButtonProps](../interfaces/src_app_Components_Controls_MenuButton.IMenuButtonProps.md)

### Functions

- [MenuButton](src_app_Components_Controls_MenuButton.md#menubutton)

## Functions

### MenuButton

▸ **MenuButton**(`props`): `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `props` | [`IMenuButtonProps`](../interfaces/src_app_Components_Controls_MenuButton.IMenuButtonProps.md) |

#### Returns

`Element`

#### Defined in

[src/app/Components/Controls/MenuButton/index.tsx:35](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Controls/MenuButton/index.tsx#lines-35)
