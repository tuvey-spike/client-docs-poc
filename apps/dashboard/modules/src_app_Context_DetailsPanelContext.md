[client](../../../index.md) / [Exports](../modules.md) / src/app/Context/DetailsPanelContext

# Module: src/app/Context/DetailsPanelContext

## Table of contents

### Interfaces

- [IDetailsPanelContent](../interfaces/src_app_Context_DetailsPanelContext.IDetailsPanelContent.md)
- [IDetailsPanelContext](../interfaces/src_app_Context_DetailsPanelContext.IDetailsPanelContext.md)
- [IDetailsPanelContextProviderProps](../interfaces/src_app_Context_DetailsPanelContext.IDetailsPanelContextProviderProps.md)
- [IDetailsPanelSection](../interfaces/src_app_Context_DetailsPanelContext.IDetailsPanelSection.md)

### Functions

- [DetailsPanelContextProvider](src_app_Context_DetailsPanelContext.md#detailspanelcontextprovider)
- [useDetailsPanel](src_app_Context_DetailsPanelContext.md#usedetailspanel)

## Functions

### DetailsPanelContextProvider

▸ **DetailsPanelContextProvider**(`props`): `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `props` | [`IDetailsPanelContextProviderProps`](../interfaces/src_app_Context_DetailsPanelContext.IDetailsPanelContextProviderProps.md) |

#### Returns

`Element`

#### Defined in

[src/app/Context/DetailsPanelContext.tsx:37](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Context/DetailsPanelContext.tsx#lines-37)

___

### useDetailsPanel

▸ **useDetailsPanel**(): `Object`

#### Returns

`Object`

| Name | Type |
| :------ | :------ |
| `closePanel` | () => `void` |
| `content` | ``null`` \| [`IDetailsPanelContent`](../interfaces/src_app_Context_DetailsPanelContext.IDetailsPanelContent.md) |
| `setContent` | (`content`: ``null`` \| [`IDetailsPanelContent`](../interfaces/src_app_Context_DetailsPanelContext.IDetailsPanelContent.md)) => `void` |

#### Defined in

[src/app/Context/DetailsPanelContext.tsx:48](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Context/DetailsPanelContext.tsx#lines-48)
