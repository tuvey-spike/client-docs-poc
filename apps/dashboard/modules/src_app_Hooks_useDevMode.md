[client](../../../index.md) / [Exports](../modules.md) / src/app/Hooks/useDevMode

# Module: src/app/Hooks/useDevMode

## Table of contents

### Functions

- [default](src_app_Hooks_useDevMode.md#default)

## Functions

### default

▸ **default**(`initValue?`): `Object`

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `initValue` | `boolean` | `false` |

#### Returns

`Object`

| Name | Type |
| :------ | :------ |
| `devMode` | `boolean` |
| `setDevMode` | `Dispatch`<`SetStateAction`<`boolean`\>\> |

#### Defined in

[src/app/Hooks/useDevMode.ts:3](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Hooks/useDevMode.ts#lines-3)
