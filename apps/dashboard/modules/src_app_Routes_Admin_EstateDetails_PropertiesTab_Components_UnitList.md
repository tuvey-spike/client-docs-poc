[client](../../../index.md) / [Exports](../modules.md) / src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/UnitList

# Module: src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/UnitList

## Table of contents

### Interfaces

- [IUnitListProps](../interfaces/src_app_Routes_Admin_EstateDetails_PropertiesTab_Components_UnitList.IUnitListProps.md)

### Functions

- [UnitList](src_app_Routes_Admin_EstateDetails_PropertiesTab_Components_UnitList.md#unitlist)

## Functions

### UnitList

▸ **UnitList**(`__namedParameters`): `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | [`IUnitListProps`](../interfaces/src_app_Routes_Admin_EstateDetails_PropertiesTab_Components_UnitList.IUnitListProps.md) |

#### Returns

`Element`

#### Defined in

[src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/UnitList/index.tsx:20](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/UnitList/index.tsx#lines-20)
