[client](../../../index.md) / [Exports](../modules.md) / src/app/Routes/Admin/EstateEdit

# Module: src/app/Routes/Admin/EstateEdit

## Table of contents

### Interfaces

- [IEstateEditLocationState](../interfaces/src_app_Routes_Admin_EstateEdit.IEstateEditLocationState.md)

### Functions

- [EstateEdit](src_app_Routes_Admin_EstateEdit.md#estateedit)

## Functions

### EstateEdit

▸ **EstateEdit**(): `Element`

#### Returns

`Element`

#### Defined in

[src/app/Routes/Admin/EstateEdit/index.tsx:21](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Admin/EstateEdit/index.tsx#lines-21)
