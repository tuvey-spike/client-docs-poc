[client](../../../index.md) / [Exports](../modules.md) / src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/BuildingList

# Module: src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/BuildingList

## Table of contents

### Interfaces

- [IBuildingListProps](../interfaces/src_app_Routes_Admin_EstateDetails_PropertiesTab_Components_BuildingList.IBuildingListProps.md)

### Functions

- [BuildingList](src_app_Routes_Admin_EstateDetails_PropertiesTab_Components_BuildingList.md#buildinglist)

## Functions

### BuildingList

▸ **BuildingList**(`__namedParameters`): `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | [`IBuildingListProps`](../interfaces/src_app_Routes_Admin_EstateDetails_PropertiesTab_Components_BuildingList.IBuildingListProps.md) |

#### Returns

`Element`

#### Defined in

[src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/BuildingList/index.tsx:20](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/BuildingList/index.tsx#lines-20)
