[client](../../../index.md) / [Exports](../modules.md) / src/app/Themes/DarkTheme

# Module: src/app/Themes/DarkTheme

## Table of contents

### Variables

- [darkPalette](src_app_Themes_DarkTheme.md#darkpalette)
- [default](src_app_Themes_DarkTheme.md#default)

## Variables

### darkPalette

• **darkPalette**: [`UnitePaletteOptions`](../interfaces/src_app_Themes_UniteTheme.UnitePaletteOptions.md)

#### Defined in

[src/app/Themes/DarkTheme.ts:6](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Themes/DarkTheme.ts#lines-6)

___

### default

• **default**: `Theme`

#### Defined in

[src/app/Themes/DarkTheme.ts:21](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Themes/DarkTheme.ts#lines-21)
