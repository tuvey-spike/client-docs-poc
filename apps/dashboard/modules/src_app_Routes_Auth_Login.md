[client](../../../index.md) / [Exports](../modules.md) / src/app/Routes/Auth/Login

# Module: src/app/Routes/Auth/Login

## Table of contents

### Functions

- [Login](src_app_Routes_Auth_Login.md#login)

## Functions

### Login

▸ **Login**(): `Element`

#### Returns

`Element`

#### Defined in

[src/app/Routes/Auth/Login/index.tsx:1](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Auth/Login/index.tsx#lines-1)
