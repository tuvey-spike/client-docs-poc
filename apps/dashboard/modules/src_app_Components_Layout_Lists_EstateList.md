[client](../../../index.md) / [Exports](../modules.md) / src/app/Components/Layout/Lists/EstateList

# Module: src/app/Components/Layout/Lists/EstateList

## Table of contents

### Interfaces

- [IEstateListAction](../interfaces/src_app_Components_Layout_Lists_EstateList.IEstateListAction.md)
- [IEstateListProps](../interfaces/src_app_Components_Layout_Lists_EstateList.IEstateListProps.md)

### Functions

- [EstateList](src_app_Components_Layout_Lists_EstateList.md#estatelist)

## Functions

### EstateList

▸ **EstateList**(`props`): `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `props` | [`IEstateListProps`](../interfaces/src_app_Components_Layout_Lists_EstateList.IEstateListProps.md) |

#### Returns

`Element`

#### Defined in

[src/app/Components/Layout/Lists/EstateList/index.tsx:30](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Lists/EstateList/index.tsx#lines-30)
