[client](../../../index.md) / [Exports](../modules.md) / src/environments/environment.prod

# Module: src/environments/environment.prod

## Table of contents

### Variables

- [environment](src_environments_environment_prod.md#environment)

## Variables

### environment

• **environment**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `production` | `boolean` |

#### Defined in

[src/environments/environment.prod.ts:1](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/environments/environment.prod.ts#lines-1)
