[client](../../../index.md) / [Exports](../modules.md) / src/app/Components/Layout/Containers

# Module: src/app/Components/Layout/Containers

## Table of contents

### References

- [AppContainer](src_app_Components_Layout_Containers.md#appcontainer)
- [IAppContainerProps](src_app_Components_Layout_Containers.md#iappcontainerprops)
- [IRouteContainerProps](src_app_Components_Layout_Containers.md#iroutecontainerprops)
- [RouteContainer](src_app_Components_Layout_Containers.md#routecontainer)

## References

### AppContainer

Re-exports [AppContainer](src_app_Components_Layout_Containers_AppContainer.md#appcontainer)

___

### IAppContainerProps

Re-exports [IAppContainerProps](../interfaces/src_app_Components_Layout_Containers_AppContainer.IAppContainerProps.md)

___

### IRouteContainerProps

Re-exports [IRouteContainerProps](../interfaces/src_app_Components_Layout_Containers_RouteContainer.IRouteContainerProps.md)

___

### RouteContainer

Re-exports [RouteContainer](src_app_Components_Layout_Containers_RouteContainer.md#routecontainer)
