[client](../../../index.md) / [Exports](../modules.md) / src/app/Themes/UniteTheme

# Module: src/app/Themes/UniteTheme

## Table of contents

### Interfaces

- [UnitePalette](../interfaces/src_app_Themes_UniteTheme.UnitePalette.md)
- [UnitePaletteOptions](../interfaces/src_app_Themes_UniteTheme.UnitePaletteOptions.md)
- [UniteTheme](../interfaces/src_app_Themes_UniteTheme.UniteTheme.md)
- [UniteThemeOptions](../interfaces/src_app_Themes_UniteTheme.UniteThemeOptions.md)
- [UniteTypography](../interfaces/src_app_Themes_UniteTheme.UniteTypography.md)
- [UniteTypographyOptions](../interfaces/src_app_Themes_UniteTheme.UniteTypographyOptions.md)

### Functions

- [createUniteTheme](src_app_Themes_UniteTheme.md#createunitetheme)

## Functions

### createUniteTheme

▸ **createUniteTheme**(`palette`): `Theme`

#### Parameters

| Name | Type |
| :------ | :------ |
| `palette` | [`UnitePaletteOptions`](../interfaces/src_app_Themes_UniteTheme.UnitePaletteOptions.md) |

#### Returns

`Theme`

#### Defined in

[src/app/Themes/UniteTheme.ts:71](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Themes/UniteTheme.ts#lines-71)
