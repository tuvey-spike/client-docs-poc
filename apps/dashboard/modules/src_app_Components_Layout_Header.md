[client](../../../index.md) / [Exports](../modules.md) / src/app/Components/Layout/Header

# Module: src/app/Components/Layout/Header

## Table of contents

### Functions

- [Header](src_app_Components_Layout_Header.md#header)

## Functions

### Header

▸ **Header**(): `Element`

#### Returns

`Element`

#### Defined in

[src/app/Components/Layout/Header/index.tsx:29](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Header/index.tsx#lines-29)
