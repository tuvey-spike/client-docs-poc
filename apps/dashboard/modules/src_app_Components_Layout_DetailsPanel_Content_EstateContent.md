[client](../../../index.md) / [Exports](../modules.md) / src/app/Components/Layout/DetailsPanel/Content/EstateContent

# Module: src/app/Components/Layout/DetailsPanel/Content/EstateContent

## Table of contents

### Functions

- [GetEstateContent](src_app_Components_Layout_DetailsPanel_Content_EstateContent.md#getestatecontent)

## Functions

### GetEstateContent

▸ **GetEstateContent**(`estate`, `open?`): [`IDetailsPanelContent`](../interfaces/src_app_Context_DetailsPanelContext.IDetailsPanelContent.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `estate` | `EstateModel` |
| `open?` | () => `void` |

#### Returns

[`IDetailsPanelContent`](../interfaces/src_app_Context_DetailsPanelContext.IDetailsPanelContent.md)

#### Defined in

[src/app/Components/Layout/DetailsPanel/Content/EstateContent.ts:6](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/DetailsPanel/Content/EstateContent.ts#lines-6)
