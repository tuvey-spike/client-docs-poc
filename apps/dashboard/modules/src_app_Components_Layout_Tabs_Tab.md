[client](../../../index.md) / [Exports](../modules.md) / src/app/Components/Layout/Tabs/Tab

# Module: src/app/Components/Layout/Tabs/Tab

## Table of contents

### Interfaces

- [ITabProps](../interfaces/src_app_Components_Layout_Tabs_Tab.ITabProps.md)

### Functions

- [Tab](src_app_Components_Layout_Tabs_Tab.md#tab)

## Functions

### Tab

▸ **Tab**(`props`): `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `props` | [`ITabProps`](../interfaces/src_app_Components_Layout_Tabs_Tab.ITabProps.md) |

#### Returns

`Element`

#### Defined in

[src/app/Components/Layout/Tabs/Tab.tsx:16](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Tabs/Tab.tsx#lines-16)
