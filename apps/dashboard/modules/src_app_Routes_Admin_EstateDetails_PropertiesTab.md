[client](../../../index.md) / [Exports](../modules.md) / src/app/Routes/Admin/EstateDetails/PropertiesTab

# Module: src/app/Routes/Admin/EstateDetails/PropertiesTab

## Table of contents

### Interfaces

- [IPropertiesTabProps](../interfaces/src_app_Routes_Admin_EstateDetails_PropertiesTab.IPropertiesTabProps.md)

### Functions

- [PropertiesTab](src_app_Routes_Admin_EstateDetails_PropertiesTab.md#propertiestab)

## Functions

### PropertiesTab

▸ **PropertiesTab**(`props`): `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `props` | [`IPropertiesTabProps`](../interfaces/src_app_Routes_Admin_EstateDetails_PropertiesTab.IPropertiesTabProps.md) |

#### Returns

`Element`

#### Defined in

[src/app/Routes/Admin/EstateDetails/PropertiesTab/index.tsx:18](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Admin/EstateDetails/PropertiesTab/index.tsx#lines-18)
