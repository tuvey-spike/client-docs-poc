[client](../../../index.md) / [Exports](../modules.md) / src/app/Routes/Admin/UnitEdit

# Module: src/app/Routes/Admin/UnitEdit

## Table of contents

### Interfaces

- [IUnitEditLocationState](../interfaces/src_app_Routes_Admin_UnitEdit.IUnitEditLocationState.md)

### Functions

- [UnitEdit](src_app_Routes_Admin_UnitEdit.md#unitedit)

## Functions

### UnitEdit

▸ **UnitEdit**(): `Element`

#### Returns

`Element`

#### Defined in

[src/app/Routes/Admin/UnitEdit/index.tsx:24](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Admin/UnitEdit/index.tsx#lines-24)
