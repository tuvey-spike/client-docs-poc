[client](../../../index.md) / [Exports](../modules.md) / src/app/Components/Forms/BuildingForm

# Module: src/app/Components/Forms/BuildingForm

## Table of contents

### Interfaces

- [IBuildingFormProps](../interfaces/src_app_Components_Forms_BuildingForm.IBuildingFormProps.md)

### Functions

- [BuildingForm](src_app_Components_Forms_BuildingForm.md#buildingform)

## Functions

### BuildingForm

▸ **BuildingForm**(`props`): `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `props` | [`IBuildingFormProps`](../interfaces/src_app_Components_Forms_BuildingForm.IBuildingFormProps.md) |

#### Returns

`Element`

#### Defined in

[src/app/Components/Forms/BuildingForm/index.tsx:54](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Forms/BuildingForm/index.tsx#lines-54)
