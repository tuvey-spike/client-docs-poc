[client](../../../index.md) / [Exports](../modules.md) / jest.config

# Module: jest.config

## Table of contents

### Namespaces

- [export&#x3D;](jest_config.export_.md)

### Properties

- [export&#x3D;](jest_config.md#export&#x3D;)

## Properties

### export&#x3D;

• **export=**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `coverageDirectory` | `string` |
| `displayName` | `string` |
| `globals` | `Object` |
| `globals.ts-jest` | `Object` |
| `globals.ts-jest.tsconfig` | `string` |
| `moduleFileExtensions` | `string`[] |
| `preset` | `string` |
| `transform` | `Object` |
| transform.^(?!.*\.(js\|jsx\|ts\|tsx\|css\|json)$) | `string` |
| transform.^.+\.[tj]sx?$ | `string` |

#### Defined in

[jest.config.js:1](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/jest.config.js#lines-1)
