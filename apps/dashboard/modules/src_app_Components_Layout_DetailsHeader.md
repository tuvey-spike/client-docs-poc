[client](../../../index.md) / [Exports](../modules.md) / src/app/Components/Layout/DetailsHeader

# Module: src/app/Components/Layout/DetailsHeader

## Table of contents

### Interfaces

- [IDetailsHeaderAction](../interfaces/src_app_Components_Layout_DetailsHeader.IDetailsHeaderAction.md)
- [IDetailsHeaderProps](../interfaces/src_app_Components_Layout_DetailsHeader.IDetailsHeaderProps.md)

### Functions

- [DetailsHeader](src_app_Components_Layout_DetailsHeader.md#detailsheader)

## Functions

### DetailsHeader

▸ **DetailsHeader**(`props`): `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `props` | [`IDetailsHeaderProps`](../interfaces/src_app_Components_Layout_DetailsHeader.IDetailsHeaderProps.md) |

#### Returns

`Element`

#### Defined in

[src/app/Components/Layout/DetailsHeader/index.tsx:53](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/DetailsHeader/index.tsx#lines-53)
