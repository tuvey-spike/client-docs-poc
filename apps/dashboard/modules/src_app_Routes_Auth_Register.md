[client](../../../index.md) / [Exports](../modules.md) / src/app/Routes/Auth/Register

# Module: src/app/Routes/Auth/Register

## Table of contents

### Functions

- [Register](src_app_Routes_Auth_Register.md#register)

## Functions

### Register

▸ **Register**(): `Element`

#### Returns

`Element`

#### Defined in

[src/app/Routes/Auth/Register/index.tsx:1](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Auth/Register/index.tsx#lines-1)
