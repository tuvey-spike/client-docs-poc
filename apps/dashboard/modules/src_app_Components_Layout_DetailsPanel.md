[client](../../../index.md) / [Exports](../modules.md) / src/app/Components/Layout/DetailsPanel

# Module: src/app/Components/Layout/DetailsPanel

## Table of contents

### References

- [GetEstateContent](src_app_Components_Layout_DetailsPanel.md#getestatecontent)

### Functions

- [DetailsPanel](src_app_Components_Layout_DetailsPanel.md#detailspanel)

## References

### GetEstateContent

Re-exports [GetEstateContent](src_app_Components_Layout_DetailsPanel_Content_EstateContent.md#getestatecontent)

## Functions

### DetailsPanel

▸ **DetailsPanel**(): ``null`` \| `Element`

#### Returns

``null`` \| `Element`

#### Defined in

[src/app/Components/Layout/DetailsPanel/index.tsx:21](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/DetailsPanel/index.tsx#lines-21)
