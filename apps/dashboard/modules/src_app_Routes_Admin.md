[client](../../../index.md) / [Exports](../modules.md) / src/app/Routes/Admin

# Module: src/app/Routes/Admin

## Table of contents

### Functions

- [default](src_app_Routes_Admin.md#default)

## Functions

### default

▸ **default**(): `Element`

#### Returns

`Element`

#### Defined in

[src/app/Routes/Admin/index.tsx:18](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Admin/index.tsx#lines-18)
