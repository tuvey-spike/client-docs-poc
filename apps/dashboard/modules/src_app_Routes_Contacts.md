[client](../../../index.md) / [Exports](../modules.md) / src/app/Routes/Contacts

# Module: src/app/Routes/Contacts

## Table of contents

### Functions

- [default](src_app_Routes_Contacts.md#default)

## Functions

### default

▸ **default**(): `Element`

#### Returns

`Element`

#### Defined in

[src/app/Routes/Contacts/index.tsx:10](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Contacts/index.tsx#lines-10)
