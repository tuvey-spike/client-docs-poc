[client](../../../index.md) / [Exports](../modules.md) / [src/app/Routes/Admin/BuildingEdit](../modules/src_app_Routes_Admin_BuildingEdit.md) / IBuildingEditLocationState

# Interface: IBuildingEditLocationState

[src/app/Routes/Admin/BuildingEdit](../modules/src_app_Routes_Admin_BuildingEdit.md).IBuildingEditLocationState

## Table of contents

### Properties

- [estateId](src_app_Routes_Admin_BuildingEdit.IBuildingEditLocationState.md#estateid)
- [id](src_app_Routes_Admin_BuildingEdit.IBuildingEditLocationState.md#id)

## Properties

### estateId

• **estateId**: `string`

#### Defined in

[src/app/Routes/Admin/BuildingEdit/index.tsx:19](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Admin/BuildingEdit/index.tsx#lines-19)

___

### id

• `Optional` **id**: `string`

#### Defined in

[src/app/Routes/Admin/BuildingEdit/index.tsx:20](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Admin/BuildingEdit/index.tsx#lines-20)
