[client](../../../index.md) / [Exports](../modules.md) / [src/app/Context/DetailsPanelContext](../modules/src_app_Context_DetailsPanelContext.md) / IDetailsPanelContextProviderProps

# Interface: IDetailsPanelContextProviderProps

[src/app/Context/DetailsPanelContext](../modules/src_app_Context_DetailsPanelContext.md).IDetailsPanelContextProviderProps

## Table of contents

### Properties

- [children](src_app_Context_DetailsPanelContext.IDetailsPanelContextProviderProps.md#children)

## Properties

### children

• **children**: `ReactChild` \| `ReactChild`[]

#### Defined in

[src/app/Context/DetailsPanelContext.tsx:34](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Context/DetailsPanelContext.tsx#lines-34)
