[client](../../../index.md) / [Exports](../modules.md) / [src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/BuildingList](../modules/src_app_Routes_Admin_EstateDetails_PropertiesTab_Components_BuildingList.md) / IBuildingListProps

# Interface: IBuildingListProps

[src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/BuildingList](../modules/src_app_Routes_Admin_EstateDetails_PropertiesTab_Components_BuildingList.md).IBuildingListProps

## Table of contents

### Properties

- [buildings](src_app_Routes_Admin_EstateDetails_PropertiesTab_Components_BuildingList.IBuildingListProps.md#buildings)

### Methods

- [onPageEnd](src_app_Routes_Admin_EstateDetails_PropertiesTab_Components_BuildingList.IBuildingListProps.md#onpageend)

## Properties

### buildings

• **buildings**: `BuildingModel`[]

#### Defined in

[src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/BuildingList/index.tsx:16](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/BuildingList/index.tsx#lines-16)

## Methods

### onPageEnd

▸ `Optional` **onPageEnd**(): `void`

#### Returns

`void`

#### Defined in

[src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/BuildingList/index.tsx:17](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/BuildingList/index.tsx#lines-17)
