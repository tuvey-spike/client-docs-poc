[client](../../../index.md) / [Exports](../modules.md) / [src/app/Components/Controls/MenuButton](../modules/src_app_Components_Controls_MenuButton.md) / IMenuButtonProps

# Interface: IMenuButtonProps

[src/app/Components/Controls/MenuButton](../modules/src_app_Components_Controls_MenuButton.md).IMenuButtonProps

## Table of contents

### Properties

- [buttonSx](src_app_Components_Controls_MenuButton.IMenuButtonProps.md#buttonsx)
- [icon](src_app_Components_Controls_MenuButton.IMenuButtonProps.md#icon)
- [items](src_app_Components_Controls_MenuButton.IMenuButtonProps.md#items)
- [menuItemSx](src_app_Components_Controls_MenuButton.IMenuButtonProps.md#menuitemsx)
- [menuSx](src_app_Components_Controls_MenuButton.IMenuButtonProps.md#menusx)
- [title](src_app_Components_Controls_MenuButton.IMenuButtonProps.md#title)

## Properties

### buttonSx

• `Optional` **buttonSx**: `SxProps`<`Theme`\>

#### Defined in

[src/app/Components/Controls/MenuButton/index.tsx:32](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Controls/MenuButton/index.tsx#lines-32)

___

### icon

• `Optional` **icon**: `string`

#### Defined in

[src/app/Components/Controls/MenuButton/index.tsx:27](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Controls/MenuButton/index.tsx#lines-27)

___

### items

• **items**: [`IMenuButtonItem`](src_app_Components_Controls_MenuButton.IMenuButtonItem.md)[]

#### Defined in

[src/app/Components/Controls/MenuButton/index.tsx:29](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Controls/MenuButton/index.tsx#lines-29)

___

### menuItemSx

• `Optional` **menuItemSx**: `SxProps`<`Theme`\>

#### Defined in

[src/app/Components/Controls/MenuButton/index.tsx:31](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Controls/MenuButton/index.tsx#lines-31)

___

### menuSx

• `Optional` **menuSx**: `SxProps`<`Theme`\>

#### Defined in

[src/app/Components/Controls/MenuButton/index.tsx:30](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Controls/MenuButton/index.tsx#lines-30)

___

### title

• **title**: `string`

#### Defined in

[src/app/Components/Controls/MenuButton/index.tsx:28](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Controls/MenuButton/index.tsx#lines-28)
