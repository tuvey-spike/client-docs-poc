[client](../../../index.md) / [Exports](../modules.md) / [src/app/Routes/Admin/UnitEdit](../modules/src_app_Routes_Admin_UnitEdit.md) / IUnitEditLocationState

# Interface: IUnitEditLocationState

[src/app/Routes/Admin/UnitEdit](../modules/src_app_Routes_Admin_UnitEdit.md).IUnitEditLocationState

## Table of contents

### Properties

- [buildingId](src_app_Routes_Admin_UnitEdit.IUnitEditLocationState.md#buildingid)
- [estateId](src_app_Routes_Admin_UnitEdit.IUnitEditLocationState.md#estateid)
- [id](src_app_Routes_Admin_UnitEdit.IUnitEditLocationState.md#id)

## Properties

### buildingId

• `Optional` **buildingId**: `string`

#### Defined in

[src/app/Routes/Admin/UnitEdit/index.tsx:20](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Admin/UnitEdit/index.tsx#lines-20)

___

### estateId

• `Optional` **estateId**: `string`

#### Defined in

[src/app/Routes/Admin/UnitEdit/index.tsx:19](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Admin/UnitEdit/index.tsx#lines-19)

___

### id

• `Optional` **id**: `string`

#### Defined in

[src/app/Routes/Admin/UnitEdit/index.tsx:21](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Admin/UnitEdit/index.tsx#lines-21)
