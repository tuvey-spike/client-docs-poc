[client](../../../index.md) / [Exports](../modules.md) / [src/app/Themes/UniteTheme](../modules/src_app_Themes_UniteTheme.md) / UnitePaletteOptions

# Interface: UnitePaletteOptions

[src/app/Themes/UniteTheme](../modules/src_app_Themes_UniteTheme.md).UnitePaletteOptions

## Hierarchy

- `PaletteOptions`

  ↳ **`UnitePaletteOptions`**

## Table of contents

### Properties

- [colours](src_app_Themes_UniteTheme.UnitePaletteOptions.md#colours)
- [content](src_app_Themes_UniteTheme.UnitePaletteOptions.md#content)
- [greys](src_app_Themes_UniteTheme.UnitePaletteOptions.md#greys)

## Properties

### colours

• `Optional` **colours**: `Object`

#### Index signature

▪ [key: `string`]: `string`

#### Defined in

[src/app/Themes/UniteTheme.ts:35](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Themes/UniteTheme.ts#lines-35)

___

### content

• `Optional` **content**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `primary` | `string` |
| `secondary` | `string` |

#### Defined in

[src/app/Themes/UniteTheme.ts:31](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Themes/UniteTheme.ts#lines-31)

___

### greys

• `Optional` **greys**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `main?` | `string` |
| `sidebar?` | `string` |

#### Defined in

[src/app/Themes/UniteTheme.ts:27](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Themes/UniteTheme.ts#lines-27)
