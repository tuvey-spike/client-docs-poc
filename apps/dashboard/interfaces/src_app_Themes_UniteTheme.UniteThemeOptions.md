[client](../../../index.md) / [Exports](../modules.md) / [src/app/Themes/UniteTheme](../modules/src_app_Themes_UniteTheme.md) / UniteThemeOptions

# Interface: UniteThemeOptions

[src/app/Themes/UniteTheme](../modules/src_app_Themes_UniteTheme.md).UniteThemeOptions

## Hierarchy

- `ThemeOptions`

  ↳ **`UniteThemeOptions`**
