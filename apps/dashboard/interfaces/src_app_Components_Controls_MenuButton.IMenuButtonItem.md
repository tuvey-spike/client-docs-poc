[client](../../../index.md) / [Exports](../modules.md) / [src/app/Components/Controls/MenuButton](../modules/src_app_Components_Controls_MenuButton.md) / IMenuButtonItem

# Interface: IMenuButtonItem

[src/app/Components/Controls/MenuButton](../modules/src_app_Components_Controls_MenuButton.md).IMenuButtonItem

## Table of contents

### Properties

- [element](src_app_Components_Controls_MenuButton.IMenuButtonItem.md#element)
- [icon](src_app_Components_Controls_MenuButton.IMenuButtonItem.md#icon)
- [title](src_app_Components_Controls_MenuButton.IMenuButtonItem.md#title)

### Methods

- [onClick](src_app_Components_Controls_MenuButton.IMenuButtonItem.md#onclick)

## Properties

### element

• `Optional` **element**: `Element`

#### Defined in

[src/app/Components/Controls/MenuButton/index.tsx:20](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Controls/MenuButton/index.tsx#lines-20)

___

### icon

• `Optional` **icon**: `Element`

#### Defined in

[src/app/Components/Controls/MenuButton/index.tsx:21](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Controls/MenuButton/index.tsx#lines-21)

___

### title

• `Optional` **title**: `string`

#### Defined in

[src/app/Components/Controls/MenuButton/index.tsx:22](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Controls/MenuButton/index.tsx#lines-22)

## Methods

### onClick

▸ `Optional` **onClick**(): `void`

#### Returns

`void`

#### Defined in

[src/app/Components/Controls/MenuButton/index.tsx:23](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Controls/MenuButton/index.tsx#lines-23)
