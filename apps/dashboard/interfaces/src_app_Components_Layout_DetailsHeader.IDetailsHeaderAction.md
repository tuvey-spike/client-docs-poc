[client](../../../index.md) / [Exports](../modules.md) / [src/app/Components/Layout/DetailsHeader](../modules/src_app_Components_Layout_DetailsHeader.md) / IDetailsHeaderAction

# Interface: IDetailsHeaderAction

[src/app/Components/Layout/DetailsHeader](../modules/src_app_Components_Layout_DetailsHeader.md).IDetailsHeaderAction

## Table of contents

### Properties

- [icon](src_app_Components_Layout_DetailsHeader.IDetailsHeaderAction.md#icon)
- [title](src_app_Components_Layout_DetailsHeader.IDetailsHeaderAction.md#title)

### Methods

- [element](src_app_Components_Layout_DetailsHeader.IDetailsHeaderAction.md#element)
- [onClick](src_app_Components_Layout_DetailsHeader.IDetailsHeaderAction.md#onclick)

## Properties

### icon

• `Optional` **icon**: `Element`

#### Defined in

[src/app/Components/Layout/DetailsHeader/index.tsx:20](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/DetailsHeader/index.tsx#lines-20)

___

### title

• `Optional` **title**: `string`

#### Defined in

[src/app/Components/Layout/DetailsHeader/index.tsx:19](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/DetailsHeader/index.tsx#lines-19)

## Methods

### element

▸ `Optional` **element**(): `Element`

#### Returns

`Element`

#### Defined in

[src/app/Components/Layout/DetailsHeader/index.tsx:18](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/DetailsHeader/index.tsx#lines-18)

___

### onClick

▸ `Optional` **onClick**(): `void`

#### Returns

`void`

#### Defined in

[src/app/Components/Layout/DetailsHeader/index.tsx:21](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/DetailsHeader/index.tsx#lines-21)
