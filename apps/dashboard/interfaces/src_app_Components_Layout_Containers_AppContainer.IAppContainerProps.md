[client](../../../index.md) / [Exports](../modules.md) / [src/app/Components/Layout/Containers/AppContainer](../modules/src_app_Components_Layout_Containers_AppContainer.md) / IAppContainerProps

# Interface: IAppContainerProps

[src/app/Components/Layout/Containers/AppContainer](../modules/src_app_Components_Layout_Containers_AppContainer.md).IAppContainerProps

## Table of contents

### Properties

- [children](src_app_Components_Layout_Containers_AppContainer.IAppContainerProps.md#children)
- [className](src_app_Components_Layout_Containers_AppContainer.IAppContainerProps.md#classname)
- [sidebarLinks](src_app_Components_Layout_Containers_AppContainer.IAppContainerProps.md#sidebarlinks)

## Properties

### children

• `Optional` **children**: `ReactChild` \| `ReactChild`[]

#### Defined in

[src/app/Components/Layout/Containers/AppContainer.tsx:12](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Containers/AppContainer.tsx#lines-12)

___

### className

• `Optional` **className**: `string`

#### Defined in

[src/app/Components/Layout/Containers/AppContainer.tsx:14](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Containers/AppContainer.tsx#lines-14)

___

### sidebarLinks

• `Optional` **sidebarLinks**: [`SidebarLink`](src_app_Components_Layout_Sidebar.SidebarLink.md)[]

#### Defined in

[src/app/Components/Layout/Containers/AppContainer.tsx:13](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Containers/AppContainer.tsx#lines-13)
