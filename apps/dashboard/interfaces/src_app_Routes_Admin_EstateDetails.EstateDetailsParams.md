[client](../../../index.md) / [Exports](../modules.md) / [src/app/Routes/Admin/EstateDetails](../modules/src_app_Routes_Admin_EstateDetails.md) / EstateDetailsParams

# Interface: EstateDetailsParams

[src/app/Routes/Admin/EstateDetails](../modules/src_app_Routes_Admin_EstateDetails.md).EstateDetailsParams

## Table of contents

### Properties

- [id](src_app_Routes_Admin_EstateDetails.EstateDetailsParams.md#id)

## Properties

### id

• **id**: `string`

#### Defined in

[src/app/Routes/Admin/EstateDetails/index.tsx:18](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Admin/EstateDetails/index.tsx#lines-18)
