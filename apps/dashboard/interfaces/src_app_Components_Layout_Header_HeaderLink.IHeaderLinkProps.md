[client](../../../index.md) / [Exports](../modules.md) / [src/app/Components/Layout/Header/HeaderLink](../modules/src_app_Components_Layout_Header_HeaderLink.md) / IHeaderLinkProps

# Interface: IHeaderLinkProps

[src/app/Components/Layout/Header/HeaderLink](../modules/src_app_Components_Layout_Header_HeaderLink.md).IHeaderLinkProps

## Table of contents

### Properties

- [icon](src_app_Components_Layout_Header_HeaderLink.IHeaderLinkProps.md#icon)
- [menuItemSx](src_app_Components_Layout_Header_HeaderLink.IHeaderLinkProps.md#menuitemsx)
- [menuItems](src_app_Components_Layout_Header_HeaderLink.IHeaderLinkProps.md#menuitems)
- [menuSx](src_app_Components_Layout_Header_HeaderLink.IHeaderLinkProps.md#menusx)

## Properties

### icon

• **icon**: `Element`

#### Defined in

[src/app/Components/Layout/Header/HeaderLink/index.tsx:20](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Header/HeaderLink/index.tsx#lines-20)

___

### menuItemSx

• `Optional` **menuItemSx**: `SxProps`<`Theme`\>

#### Defined in

[src/app/Components/Layout/Header/HeaderLink/index.tsx:23](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Header/HeaderLink/index.tsx#lines-23)

___

### menuItems

• `Optional` **menuItems**: [`IHeaderLinkMenuItem`](src_app_Components_Layout_Header_HeaderLink.IHeaderLinkMenuItem.md)[]

#### Defined in

[src/app/Components/Layout/Header/HeaderLink/index.tsx:21](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Header/HeaderLink/index.tsx#lines-21)

___

### menuSx

• `Optional` **menuSx**: `SxProps`<`Theme`\>

#### Defined in

[src/app/Components/Layout/Header/HeaderLink/index.tsx:22](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Header/HeaderLink/index.tsx#lines-22)
