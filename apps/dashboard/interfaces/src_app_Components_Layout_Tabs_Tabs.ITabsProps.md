[client](../../../index.md) / [Exports](../modules.md) / [src/app/Components/Layout/Tabs/Tabs](../modules/src_app_Components_Layout_Tabs_Tabs.md) / ITabsProps

# Interface: ITabsProps

[src/app/Components/Layout/Tabs/Tabs](../modules/src_app_Components_Layout_Tabs_Tabs.md).ITabsProps

## Table of contents

### Properties

- [children](src_app_Components_Layout_Tabs_Tabs.ITabsProps.md#children)
- [initPath](src_app_Components_Layout_Tabs_Tabs.ITabsProps.md#initpath)

## Properties

### children

• **children**: `ReactElement`<[`ITabProps`](src_app_Components_Layout_Tabs_Tab.ITabProps.md), `string` \| `JSXElementConstructor`<`any`\>\> \| `ReactElement`<[`ITabProps`](src_app_Components_Layout_Tabs_Tab.ITabProps.md), `string` \| `JSXElementConstructor`<`any`\>\>[]

#### Defined in

[src/app/Components/Layout/Tabs/Tabs.tsx:30](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Tabs/Tabs.tsx#lines-30)

___

### initPath

• `Optional` **initPath**: `string`

#### Defined in

[src/app/Components/Layout/Tabs/Tabs.tsx:31](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Tabs/Tabs.tsx#lines-31)
