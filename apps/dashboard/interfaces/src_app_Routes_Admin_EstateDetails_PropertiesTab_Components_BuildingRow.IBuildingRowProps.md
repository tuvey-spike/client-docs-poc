[client](../../../index.md) / [Exports](../modules.md) / [src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/BuildingRow](../modules/src_app_Routes_Admin_EstateDetails_PropertiesTab_Components_BuildingRow.md) / IBuildingRowProps

# Interface: IBuildingRowProps

[src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/BuildingRow](../modules/src_app_Routes_Admin_EstateDetails_PropertiesTab_Components_BuildingRow.md).IBuildingRowProps

## Table of contents

### Properties

- [building](src_app_Routes_Admin_EstateDetails_PropertiesTab_Components_BuildingRow.IBuildingRowProps.md#building)

## Properties

### building

• **building**: `BuildingModel`

#### Defined in

[src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/BuildingRow/index.tsx:15](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/BuildingRow/index.tsx#lines-15)
