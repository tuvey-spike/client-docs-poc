[client](../../../index.md) / [Exports](../modules.md) / [src/app/Components/Forms/EstateForm](../modules/src_app_Components_Forms_EstateForm.md) / IEstateFormProps

# Interface: IEstateFormProps

[src/app/Components/Forms/EstateForm](../modules/src_app_Components_Forms_EstateForm.md).IEstateFormProps

## Table of contents

### Properties

- [estate](src_app_Components_Forms_EstateForm.IEstateFormProps.md#estate)

### Methods

- [onCancel](src_app_Components_Forms_EstateForm.IEstateFormProps.md#oncancel)
- [onSubmit](src_app_Components_Forms_EstateForm.IEstateFormProps.md#onsubmit)

## Properties

### estate

• `Optional` **estate**: ``null`` \| `EstateModel`

#### Defined in

[src/app/Components/Forms/EstateForm/index.tsx:29](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Forms/EstateForm/index.tsx#lines-29)

## Methods

### onCancel

▸ **onCancel**(): `void`

#### Returns

`void`

#### Defined in

[src/app/Components/Forms/EstateForm/index.tsx:27](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Forms/EstateForm/index.tsx#lines-27)

___

### onSubmit

▸ **onSubmit**(`estate`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `estate` | `EstateModel` |

#### Returns

`void`

#### Defined in

[src/app/Components/Forms/EstateForm/index.tsx:28](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Forms/EstateForm/index.tsx#lines-28)
