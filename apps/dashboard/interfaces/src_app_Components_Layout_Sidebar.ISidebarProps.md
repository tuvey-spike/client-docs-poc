[client](../../../index.md) / [Exports](../modules.md) / [src/app/Components/Layout/Sidebar](../modules/src_app_Components_Layout_Sidebar.md) / ISidebarProps

# Interface: ISidebarProps

[src/app/Components/Layout/Sidebar](../modules/src_app_Components_Layout_Sidebar.md).ISidebarProps

## Table of contents

### Properties

- [links](src_app_Components_Layout_Sidebar.ISidebarProps.md#links)

## Properties

### links

• **links**: [`SidebarLink`](src_app_Components_Layout_Sidebar.SidebarLink.md)[]

#### Defined in

[src/app/Components/Layout/Sidebar/index.tsx:18](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Sidebar/index.tsx#lines-18)
