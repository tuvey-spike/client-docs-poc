[client](../../../index.md) / [Exports](../modules.md) / [src/app/Components/Layout/Sidebar](../modules/src_app_Components_Layout_Sidebar.md) / SidebarLink

# Interface: SidebarLink

[src/app/Components/Layout/Sidebar](../modules/src_app_Components_Layout_Sidebar.md).SidebarLink

## Table of contents

### Properties

- [icon](src_app_Components_Layout_Sidebar.SidebarLink.md#icon)
- [link](src_app_Components_Layout_Sidebar.SidebarLink.md#link)
- [title](src_app_Components_Layout_Sidebar.SidebarLink.md#title)

## Properties

### icon

• **icon**: `Element`

#### Defined in

[src/app/Components/Layout/Sidebar/index.tsx:13](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Sidebar/index.tsx#lines-13)

___

### link

• **link**: `string`

#### Defined in

[src/app/Components/Layout/Sidebar/index.tsx:12](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Sidebar/index.tsx#lines-12)

___

### title

• **title**: `string`

#### Defined in

[src/app/Components/Layout/Sidebar/index.tsx:14](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Sidebar/index.tsx#lines-14)
