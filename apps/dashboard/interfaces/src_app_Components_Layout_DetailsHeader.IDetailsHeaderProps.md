[client](../../../index.md) / [Exports](../modules.md) / [src/app/Components/Layout/DetailsHeader](../modules/src_app_Components_Layout_DetailsHeader.md) / IDetailsHeaderProps

# Interface: IDetailsHeaderProps

[src/app/Components/Layout/DetailsHeader](../modules/src_app_Components_Layout_DetailsHeader.md).IDetailsHeaderProps

## Table of contents

### Properties

- [actions](src_app_Components_Layout_DetailsHeader.IDetailsHeaderProps.md#actions)
- [panelHeader](src_app_Components_Layout_DetailsHeader.IDetailsHeaderProps.md#panelheader)
- [title](src_app_Components_Layout_DetailsHeader.IDetailsHeaderProps.md#title)
- [titleComponent](src_app_Components_Layout_DetailsHeader.IDetailsHeaderProps.md#titlecomponent)
- [titleSmallText](src_app_Components_Layout_DetailsHeader.IDetailsHeaderProps.md#titlesmalltext)

## Properties

### actions

• `Optional` **actions**: [`IDetailsHeaderAction`](src_app_Components_Layout_DetailsHeader.IDetailsHeaderAction.md)[]

#### Defined in

[src/app/Components/Layout/DetailsHeader/index.tsx:49](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/DetailsHeader/index.tsx#lines-49)

___

### panelHeader

• `Optional` **panelHeader**: `boolean`

#### Defined in

[src/app/Components/Layout/DetailsHeader/index.tsx:50](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/DetailsHeader/index.tsx#lines-50)

___

### title

• **title**: `string`

#### Defined in

[src/app/Components/Layout/DetailsHeader/index.tsx:46](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/DetailsHeader/index.tsx#lines-46)

___

### titleComponent

• `Optional` **titleComponent**: `ReactNode`

#### Defined in

[src/app/Components/Layout/DetailsHeader/index.tsx:47](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/DetailsHeader/index.tsx#lines-47)

___

### titleSmallText

• `Optional` **titleSmallText**: `string`

#### Defined in

[src/app/Components/Layout/DetailsHeader/index.tsx:48](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/DetailsHeader/index.tsx#lines-48)
