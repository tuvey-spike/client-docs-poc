[client](../../../index.md) / [Exports](../modules.md) / [src/app/Themes/UniteTheme](../modules/src_app_Themes_UniteTheme.md) / UniteTheme

# Interface: UniteTheme

[src/app/Themes/UniteTheme](../modules/src_app_Themes_UniteTheme.md).UniteTheme

## Hierarchy

- `Theme`

  ↳ **`UniteTheme`**

## Table of contents

### Properties

- [palette](src_app_Themes_UniteTheme.UniteTheme.md#palette)
- [typography](src_app_Themes_UniteTheme.UniteTheme.md#typography)

## Properties

### palette

• **palette**: [`UnitePalette`](src_app_Themes_UniteTheme.UnitePalette.md)

#### Overrides

Theme.palette

#### Defined in

[src/app/Themes/UniteTheme.ts:67](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Themes/UniteTheme.ts#lines-67)

___

### typography

• **typography**: [`UniteTypography`](src_app_Themes_UniteTheme.UniteTypography.md)

#### Overrides

Theme.typography

#### Defined in

[src/app/Themes/UniteTheme.ts:68](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Themes/UniteTheme.ts#lines-68)
