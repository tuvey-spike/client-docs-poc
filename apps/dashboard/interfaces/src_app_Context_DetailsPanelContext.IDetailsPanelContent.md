[client](../../../index.md) / [Exports](../modules.md) / [src/app/Context/DetailsPanelContext](../modules/src_app_Context_DetailsPanelContext.md) / IDetailsPanelContent

# Interface: IDetailsPanelContent

[src/app/Context/DetailsPanelContext](../modules/src_app_Context_DetailsPanelContext.md).IDetailsPanelContent

## Table of contents

### Properties

- [sections](src_app_Context_DetailsPanelContext.IDetailsPanelContent.md#sections)
- [title](src_app_Context_DetailsPanelContext.IDetailsPanelContent.md#title)

### Methods

- [open](src_app_Context_DetailsPanelContext.IDetailsPanelContent.md#open)

## Properties

### sections

• **sections**: [`IDetailsPanelSection`](src_app_Context_DetailsPanelContext.IDetailsPanelSection.md)[]

#### Defined in

[src/app/Context/DetailsPanelContext.tsx:19](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Context/DetailsPanelContext.tsx#lines-19)

___

### title

• `Optional` **title**: `string`

#### Defined in

[src/app/Context/DetailsPanelContext.tsx:18](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Context/DetailsPanelContext.tsx#lines-18)

## Methods

### open

▸ `Optional` **open**(): `void`

#### Returns

`void`

#### Defined in

[src/app/Context/DetailsPanelContext.tsx:20](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Context/DetailsPanelContext.tsx#lines-20)
