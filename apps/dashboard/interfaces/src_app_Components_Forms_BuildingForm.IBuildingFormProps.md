[client](../../../index.md) / [Exports](../modules.md) / [src/app/Components/Forms/BuildingForm](../modules/src_app_Components_Forms_BuildingForm.md) / IBuildingFormProps

# Interface: IBuildingFormProps

[src/app/Components/Forms/BuildingForm](../modules/src_app_Components_Forms_BuildingForm.md).IBuildingFormProps

## Table of contents

### Properties

- [building](src_app_Components_Forms_BuildingForm.IBuildingFormProps.md#building)
- [estate](src_app_Components_Forms_BuildingForm.IBuildingFormProps.md#estate)

### Methods

- [onCancel](src_app_Components_Forms_BuildingForm.IBuildingFormProps.md#oncancel)
- [onSubmit](src_app_Components_Forms_BuildingForm.IBuildingFormProps.md#onsubmit)

## Properties

### building

• `Optional` **building**: ``null`` \| `BuildingModel`

#### Defined in

[src/app/Components/Forms/BuildingForm/index.tsx:31](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Forms/BuildingForm/index.tsx#lines-31)

___

### estate

• `Optional` **estate**: ``null`` \| `EstateModel`

#### Defined in

[src/app/Components/Forms/BuildingForm/index.tsx:32](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Forms/BuildingForm/index.tsx#lines-32)

## Methods

### onCancel

▸ **onCancel**(): `void`

#### Returns

`void`

#### Defined in

[src/app/Components/Forms/BuildingForm/index.tsx:29](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Forms/BuildingForm/index.tsx#lines-29)

___

### onSubmit

▸ **onSubmit**(`building`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `building` | `BuildingModel` |

#### Returns

`void`

#### Defined in

[src/app/Components/Forms/BuildingForm/index.tsx:30](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Forms/BuildingForm/index.tsx#lines-30)
