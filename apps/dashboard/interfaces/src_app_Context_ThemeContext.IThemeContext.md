[client](../../../index.md) / [Exports](../modules.md) / [src/app/Context/ThemeContext](../modules/src_app_Context_ThemeContext.md) / IThemeContext

# Interface: IThemeContext

[src/app/Context/ThemeContext](../modules/src_app_Context_ThemeContext.md).IThemeContext

## Table of contents

### Properties

- [theme](src_app_Context_ThemeContext.IThemeContext.md#theme)

### Methods

- [toggleTheme](src_app_Context_ThemeContext.IThemeContext.md#toggletheme)

## Properties

### theme

• **theme**: [`ThemeType`](../modules/src_app_Context_ThemeContext.md#themetype)

#### Defined in

[src/app/Context/ThemeContext.tsx:20](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Context/ThemeContext.tsx#lines-20)

## Methods

### toggleTheme

▸ **toggleTheme**(): `void`

#### Returns

`void`

#### Defined in

[src/app/Context/ThemeContext.tsx:21](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Context/ThemeContext.tsx#lines-21)
