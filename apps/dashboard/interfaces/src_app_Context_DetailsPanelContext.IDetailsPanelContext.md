[client](../../../index.md) / [Exports](../modules.md) / [src/app/Context/DetailsPanelContext](../modules/src_app_Context_DetailsPanelContext.md) / IDetailsPanelContext

# Interface: IDetailsPanelContext

[src/app/Context/DetailsPanelContext](../modules/src_app_Context_DetailsPanelContext.md).IDetailsPanelContext

## Table of contents

### Properties

- [content](src_app_Context_DetailsPanelContext.IDetailsPanelContext.md#content)

### Methods

- [setContent](src_app_Context_DetailsPanelContext.IDetailsPanelContext.md#setcontent)

## Properties

### content

• **content**: ``null`` \| [`IDetailsPanelContent`](src_app_Context_DetailsPanelContext.IDetailsPanelContent.md)

#### Defined in

[src/app/Context/DetailsPanelContext.tsx:24](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Context/DetailsPanelContext.tsx#lines-24)

## Methods

### setContent

▸ **setContent**(`content`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `content` | ``null`` \| [`IDetailsPanelContent`](src_app_Context_DetailsPanelContext.IDetailsPanelContent.md) |

#### Returns

`void`

#### Defined in

[src/app/Context/DetailsPanelContext.tsx:25](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Context/DetailsPanelContext.tsx#lines-25)
