[client](../../../index.md) / [Exports](../modules.md) / [src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/UnitList](../modules/src_app_Routes_Admin_EstateDetails_PropertiesTab_Components_UnitList.md) / IUnitListProps

# Interface: IUnitListProps

[src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/UnitList](../modules/src_app_Routes_Admin_EstateDetails_PropertiesTab_Components_UnitList.md).IUnitListProps

## Table of contents

### Properties

- [buildingId](src_app_Routes_Admin_EstateDetails_PropertiesTab_Components_UnitList.IUnitListProps.md#buildingid)

## Properties

### buildingId

• **buildingId**: `undefined` \| ``null`` \| `string`

#### Defined in

[src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/UnitList/index.tsx:17](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/UnitList/index.tsx#lines-17)
