[client](../../../index.md) / [Exports](../modules.md) / [src/app/Components/Layout/DetailsPanel/DetailsPanelSection](../modules/src_app_Components_Layout_DetailsPanel_DetailsPanelSection.md) / IDetailsPanelSectionProps

# Interface: IDetailsPanelSectionProps

[src/app/Components/Layout/DetailsPanel/DetailsPanelSection](../modules/src_app_Components_Layout_DetailsPanel_DetailsPanelSection.md).IDetailsPanelSectionProps

## Table of contents

### Properties

- [section](src_app_Components_Layout_DetailsPanel_DetailsPanelSection.IDetailsPanelSectionProps.md#section)

## Properties

### section

• **section**: [`IDetailsPanelSection`](src_app_Context_DetailsPanelContext.IDetailsPanelSection.md)

#### Defined in

[src/app/Components/Layout/DetailsPanel/DetailsPanelSection.tsx:15](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/DetailsPanel/DetailsPanelSection.tsx#lines-15)
