[client](../../../index.md) / [Exports](../modules.md) / [src/app/Themes/UniteTheme](../modules/src_app_Themes_UniteTheme.md) / UniteTypographyOptions

# Interface: UniteTypographyOptions

[src/app/Themes/UniteTheme](../modules/src_app_Themes_UniteTheme.md).UniteTypographyOptions

## Hierarchy

- `TypographyOptions`

  ↳ **`UniteTypographyOptions`**

## Table of contents

### Properties

- [body1](src_app_Themes_UniteTheme.UniteTypographyOptions.md#body1)
- [body2](src_app_Themes_UniteTheme.UniteTypographyOptions.md#body2)
- [button](src_app_Themes_UniteTheme.UniteTypographyOptions.md#button)
- [caption](src_app_Themes_UniteTheme.UniteTypographyOptions.md#caption)
- [dataGridTitle](src_app_Themes_UniteTheme.UniteTypographyOptions.md#datagridtitle)
- [details](src_app_Themes_UniteTheme.UniteTypographyOptions.md#details)
- [h1](src_app_Themes_UniteTheme.UniteTypographyOptions.md#h1)
- [h2](src_app_Themes_UniteTheme.UniteTypographyOptions.md#h2)
- [h3](src_app_Themes_UniteTheme.UniteTypographyOptions.md#h3)
- [h4](src_app_Themes_UniteTheme.UniteTypographyOptions.md#h4)
- [h5](src_app_Themes_UniteTheme.UniteTypographyOptions.md#h5)
- [h6](src_app_Themes_UniteTheme.UniteTypographyOptions.md#h6)
- [overline](src_app_Themes_UniteTheme.UniteTypographyOptions.md#overline)
- [subtitle1](src_app_Themes_UniteTheme.UniteTypographyOptions.md#subtitle1)
- [subtitle2](src_app_Themes_UniteTheme.UniteTypographyOptions.md#subtitle2)

## Properties

### body1

• **body1**: `undefined` \| `TypographyStyleOptions`

#### Inherited from

TypographyOptions.body1

___

### body2

• **body2**: `undefined` \| `TypographyStyleOptions`

#### Inherited from

TypographyOptions.body2

___

### button

• **button**: `undefined` \| `TypographyStyleOptions`

#### Inherited from

TypographyOptions.button

___

### caption

• **caption**: `undefined` \| `TypographyStyleOptions`

#### Inherited from

TypographyOptions.caption

___

### dataGridTitle

• **dataGridTitle**: `TypographyStyleOptions`

#### Defined in

[src/app/Themes/UniteTheme.ts:23](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Themes/UniteTheme.ts#lines-23)

___

### details

• **details**: `TypographyStyleOptions`

#### Defined in

[src/app/Themes/UniteTheme.ts:22](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Themes/UniteTheme.ts#lines-22)

___

### h1

• **h1**: `undefined` \| `TypographyStyleOptions`

#### Inherited from

TypographyOptions.h1

___

### h2

• **h2**: `undefined` \| `TypographyStyleOptions`

#### Inherited from

TypographyOptions.h2

___

### h3

• **h3**: `undefined` \| `TypographyStyleOptions`

#### Inherited from

TypographyOptions.h3

___

### h4

• **h4**: `undefined` \| `TypographyStyleOptions`

#### Inherited from

TypographyOptions.h4

___

### h5

• **h5**: `undefined` \| `TypographyStyleOptions`

#### Inherited from

TypographyOptions.h5

___

### h6

• **h6**: `undefined` \| `TypographyStyleOptions`

#### Inherited from

TypographyOptions.h6

___

### overline

• **overline**: `undefined` \| `TypographyStyleOptions`

#### Inherited from

TypographyOptions.overline

___

### subtitle1

• **subtitle1**: `undefined` \| `TypographyStyleOptions`

#### Inherited from

TypographyOptions.subtitle1

___

### subtitle2

• **subtitle2**: `undefined` \| `TypographyStyleOptions`

#### Inherited from

TypographyOptions.subtitle2
