[client](../../../index.md) / [Exports](../modules.md) / [src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/UnitRow](../modules/src_app_Routes_Admin_EstateDetails_PropertiesTab_Components_UnitRow.md) / IUnitRowProps

# Interface: IUnitRowProps

[src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/UnitRow](../modules/src_app_Routes_Admin_EstateDetails_PropertiesTab_Components_UnitRow.md).IUnitRowProps

## Table of contents

### Properties

- [unit](src_app_Routes_Admin_EstateDetails_PropertiesTab_Components_UnitRow.IUnitRowProps.md#unit)

## Properties

### unit

• **unit**: `UnitModel`

#### Defined in

[src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/UnitRow/index.tsx:10](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Admin/EstateDetails/PropertiesTab/Components/UnitRow/index.tsx#lines-10)
