[client](../../../index.md) / [Exports](../modules.md) / [src/app/Routes/Admin/EstateDetails/PropertiesTab](../modules/src_app_Routes_Admin_EstateDetails_PropertiesTab.md) / IPropertiesTabProps

# Interface: IPropertiesTabProps

[src/app/Routes/Admin/EstateDetails/PropertiesTab](../modules/src_app_Routes_Admin_EstateDetails_PropertiesTab.md).IPropertiesTabProps

## Table of contents

### Properties

- [estate](src_app_Routes_Admin_EstateDetails_PropertiesTab.IPropertiesTabProps.md#estate)

## Properties

### estate

• **estate**: `EstateModel`

#### Defined in

[src/app/Routes/Admin/EstateDetails/PropertiesTab/index.tsx:15](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Admin/EstateDetails/PropertiesTab/index.tsx#lines-15)
