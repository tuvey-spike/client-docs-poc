[client](../../../index.md) / [Exports](../modules.md) / [src/app/Components/Layout/Tabs/Tab](../modules/src_app_Components_Layout_Tabs_Tab.md) / ITabProps

# Interface: ITabProps

[src/app/Components/Layout/Tabs/Tab](../modules/src_app_Components_Layout_Tabs_Tab.md).ITabProps

## Table of contents

### Properties

- [component](src_app_Components_Layout_Tabs_Tab.ITabProps.md#component)
- [path](src_app_Components_Layout_Tabs_Tab.ITabProps.md#path)
- [title](src_app_Components_Layout_Tabs_Tab.ITabProps.md#title)

## Properties

### component

• **component**: `Element`

#### Defined in

[src/app/Components/Layout/Tabs/Tab.tsx:12](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Tabs/Tab.tsx#lines-12)

___

### path

• **path**: `string`

#### Defined in

[src/app/Components/Layout/Tabs/Tab.tsx:13](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Tabs/Tab.tsx#lines-13)

___

### title

• **title**: `string`

#### Defined in

[src/app/Components/Layout/Tabs/Tab.tsx:11](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Tabs/Tab.tsx#lines-11)
