[client](../../../index.md) / [Exports](../modules.md) / [src/app/Components/Layout/Lists/EstateList](../modules/src_app_Components_Layout_Lists_EstateList.md) / IEstateListAction

# Interface: IEstateListAction

[src/app/Components/Layout/Lists/EstateList](../modules/src_app_Components_Layout_Lists_EstateList.md).IEstateListAction

## Table of contents

### Properties

- [icon](src_app_Components_Layout_Lists_EstateList.IEstateListAction.md#icon)

### Methods

- [onClick](src_app_Components_Layout_Lists_EstateList.IEstateListAction.md#onclick)

## Properties

### icon

• `Optional` **icon**: `Element`

#### Defined in

[src/app/Components/Layout/Lists/EstateList/index.tsx:19](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Lists/EstateList/index.tsx#lines-19)

## Methods

### onClick

▸ `Optional` **onClick**(`estate`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `estate` | `EstateModel` |

#### Returns

`void`

#### Defined in

[src/app/Components/Layout/Lists/EstateList/index.tsx:20](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Lists/EstateList/index.tsx#lines-20)
