[client](../../../index.md) / [Exports](../modules.md) / [src/app/Components/Layout/Header/HeaderLink](../modules/src_app_Components_Layout_Header_HeaderLink.md) / IHeaderLinkMenuItem

# Interface: IHeaderLinkMenuItem

[src/app/Components/Layout/Header/HeaderLink](../modules/src_app_Components_Layout_Header_HeaderLink.md).IHeaderLinkMenuItem

## Table of contents

### Properties

- [icon](src_app_Components_Layout_Header_HeaderLink.IHeaderLinkMenuItem.md#icon)
- [title](src_app_Components_Layout_Header_HeaderLink.IHeaderLinkMenuItem.md#title)

### Methods

- [element](src_app_Components_Layout_Header_HeaderLink.IHeaderLinkMenuItem.md#element)
- [onClick](src_app_Components_Layout_Header_HeaderLink.IHeaderLinkMenuItem.md#onclick)

## Properties

### icon

• `Optional` **icon**: `Element`

#### Defined in

[src/app/Components/Layout/Header/HeaderLink/index.tsx:15](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Header/HeaderLink/index.tsx#lines-15)

___

### title

• `Optional` **title**: `string`

#### Defined in

[src/app/Components/Layout/Header/HeaderLink/index.tsx:14](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Header/HeaderLink/index.tsx#lines-14)

## Methods

### element

▸ `Optional` **element**(): `Element`

#### Returns

`Element`

#### Defined in

[src/app/Components/Layout/Header/HeaderLink/index.tsx:13](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Header/HeaderLink/index.tsx#lines-13)

___

### onClick

▸ `Optional` **onClick**(): `void`

#### Returns

`void`

#### Defined in

[src/app/Components/Layout/Header/HeaderLink/index.tsx:16](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Header/HeaderLink/index.tsx#lines-16)
