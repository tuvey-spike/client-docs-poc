[client](../../../index.md) / [Exports](../modules.md) / [src/app/Components/Layout/Lists/EstateList](../modules/src_app_Components_Layout_Lists_EstateList.md) / IEstateListProps

# Interface: IEstateListProps

[src/app/Components/Layout/Lists/EstateList](../modules/src_app_Components_Layout_Lists_EstateList.md).IEstateListProps

## Table of contents

### Properties

- [actions](src_app_Components_Layout_Lists_EstateList.IEstateListProps.md#actions)
- [estates](src_app_Components_Layout_Lists_EstateList.IEstateListProps.md#estates)

### Methods

- [onClick](src_app_Components_Layout_Lists_EstateList.IEstateListProps.md#onclick)
- [onPageEnd](src_app_Components_Layout_Lists_EstateList.IEstateListProps.md#onpageend)

## Properties

### actions

• **actions**: [`IEstateListAction`](src_app_Components_Layout_Lists_EstateList.IEstateListAction.md)[]

#### Defined in

[src/app/Components/Layout/Lists/EstateList/index.tsx:25](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Lists/EstateList/index.tsx#lines-25)

___

### estates

• **estates**: `EstateModel`[]

#### Defined in

[src/app/Components/Layout/Lists/EstateList/index.tsx:24](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Lists/EstateList/index.tsx#lines-24)

## Methods

### onClick

▸ `Optional` **onClick**(`estate`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `estate` | `EstateModel` |

#### Returns

`void`

#### Defined in

[src/app/Components/Layout/Lists/EstateList/index.tsx:26](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Lists/EstateList/index.tsx#lines-26)

___

### onPageEnd

▸ `Optional` **onPageEnd**(): `void`

#### Returns

`void`

#### Defined in

[src/app/Components/Layout/Lists/EstateList/index.tsx:27](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Lists/EstateList/index.tsx#lines-27)
