[client](../../../index.md) / [Exports](../modules.md) / [src/app/Context/DetailsPanelContext](../modules/src_app_Context_DetailsPanelContext.md) / IDetailsPanelSection

# Interface: IDetailsPanelSection

[src/app/Context/DetailsPanelContext](../modules/src_app_Context_DetailsPanelContext.md).IDetailsPanelSection

## Table of contents

### Properties

- [title](src_app_Context_DetailsPanelContext.IDetailsPanelSection.md#title)
- [values](src_app_Context_DetailsPanelContext.IDetailsPanelSection.md#values)

## Properties

### title

• `Optional` **title**: `string`

#### Defined in

[src/app/Context/DetailsPanelContext.tsx:10](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Context/DetailsPanelContext.tsx#lines-10)

___

### values

• **values**: { `label`: `string` ; `values`: `string`[]  }[]

#### Defined in

[src/app/Context/DetailsPanelContext.tsx:11](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Context/DetailsPanelContext.tsx#lines-11)
