[client](../../../index.md) / [Exports](../modules.md) / [src/app/Components/Layout/Containers/RouteContainer](../modules/src_app_Components_Layout_Containers_RouteContainer.md) / IRouteContainerProps

# Interface: IRouteContainerProps

[src/app/Components/Layout/Containers/RouteContainer](../modules/src_app_Components_Layout_Containers_RouteContainer.md).IRouteContainerProps

## Table of contents

### Properties

- [children](src_app_Components_Layout_Containers_RouteContainer.IRouteContainerProps.md#children)
- [className](src_app_Components_Layout_Containers_RouteContainer.IRouteContainerProps.md#classname)
- [detailsPanel](src_app_Components_Layout_Containers_RouteContainer.IRouteContainerProps.md#detailspanel)
- [fullWidth](src_app_Components_Layout_Containers_RouteContainer.IRouteContainerProps.md#fullwidth)
- [headerProps](src_app_Components_Layout_Containers_RouteContainer.IRouteContainerProps.md#headerprops)

## Properties

### children

• **children**: `ReactChild` \| `ReactChild`[]

#### Defined in

[src/app/Components/Layout/Containers/RouteContainer.tsx:20](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Containers/RouteContainer.tsx#lines-20)

___

### className

• `Optional` **className**: `string`

#### Defined in

[src/app/Components/Layout/Containers/RouteContainer.tsx:19](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Containers/RouteContainer.tsx#lines-19)

___

### detailsPanel

• `Optional` **detailsPanel**: `boolean`

#### Defined in

[src/app/Components/Layout/Containers/RouteContainer.tsx:17](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Containers/RouteContainer.tsx#lines-17)

___

### fullWidth

• `Optional` **fullWidth**: `boolean`

#### Defined in

[src/app/Components/Layout/Containers/RouteContainer.tsx:18](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Containers/RouteContainer.tsx#lines-18)

___

### headerProps

• `Optional` **headerProps**: [`IDetailsHeaderProps`](src_app_Components_Layout_DetailsHeader.IDetailsHeaderProps.md)

#### Defined in

[src/app/Components/Layout/Containers/RouteContainer.tsx:16](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Layout/Containers/RouteContainer.tsx#lines-16)
