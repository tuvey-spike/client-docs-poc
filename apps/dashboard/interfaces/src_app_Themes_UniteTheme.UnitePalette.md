[client](../../../index.md) / [Exports](../modules.md) / [src/app/Themes/UniteTheme](../modules/src_app_Themes_UniteTheme.md) / UnitePalette

# Interface: UnitePalette

[src/app/Themes/UniteTheme](../modules/src_app_Themes_UniteTheme.md).UnitePalette

## Hierarchy

- `Palette`

  ↳ **`UnitePalette`**

## Table of contents

### Properties

- [colours](src_app_Themes_UniteTheme.UnitePalette.md#colours)
- [content](src_app_Themes_UniteTheme.UnitePalette.md#content)
- [greys](src_app_Themes_UniteTheme.UnitePalette.md#greys)

## Properties

### colours

• `Optional` **colours**: `Object`

#### Index signature

▪ [key: `string`]: `string`

#### Defined in

[src/app/Themes/UniteTheme.ts:49](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Themes/UniteTheme.ts#lines-49)

___

### content

• `Optional` **content**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `primary` | `string` |
| `secondary` | `string` |

#### Defined in

[src/app/Themes/UniteTheme.ts:45](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Themes/UniteTheme.ts#lines-45)

___

### greys

• `Optional` **greys**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `main?` | `string` |
| `sidebar?` | `string` |

#### Defined in

[src/app/Themes/UniteTheme.ts:41](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Themes/UniteTheme.ts#lines-41)
