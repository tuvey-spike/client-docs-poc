[client](../../../index.md) / [Exports](../modules.md) / [src/app/Components/Forms/UnitForm](../modules/src_app_Components_Forms_UnitForm.md) / IUnitFormProps

# Interface: IUnitFormProps

[src/app/Components/Forms/UnitForm](../modules/src_app_Components_Forms_UnitForm.md).IUnitFormProps

## Table of contents

### Properties

- [buildings](src_app_Components_Forms_UnitForm.IUnitFormProps.md#buildings)
- [estates](src_app_Components_Forms_UnitForm.IUnitFormProps.md#estates)
- [unit](src_app_Components_Forms_UnitForm.IUnitFormProps.md#unit)

### Methods

- [onCancel](src_app_Components_Forms_UnitForm.IUnitFormProps.md#oncancel)
- [onSubmit](src_app_Components_Forms_UnitForm.IUnitFormProps.md#onsubmit)

## Properties

### buildings

• **buildings**: `BuildingModel`[]

#### Defined in

[src/app/Components/Forms/UnitForm/index.tsx:32](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Forms/UnitForm/index.tsx#lines-32)

___

### estates

• **estates**: `EstateModel`[]

#### Defined in

[src/app/Components/Forms/UnitForm/index.tsx:31](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Forms/UnitForm/index.tsx#lines-31)

___

### unit

• `Optional` **unit**: ``null`` \| `UnitModel`

#### Defined in

[src/app/Components/Forms/UnitForm/index.tsx:30](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Forms/UnitForm/index.tsx#lines-30)

## Methods

### onCancel

▸ **onCancel**(): `void`

#### Returns

`void`

#### Defined in

[src/app/Components/Forms/UnitForm/index.tsx:28](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Forms/UnitForm/index.tsx#lines-28)

___

### onSubmit

▸ **onSubmit**(`unit`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `unit` | `UnitModel` |

#### Returns

`void`

#### Defined in

[src/app/Components/Forms/UnitForm/index.tsx:29](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Components/Forms/UnitForm/index.tsx#lines-29)
