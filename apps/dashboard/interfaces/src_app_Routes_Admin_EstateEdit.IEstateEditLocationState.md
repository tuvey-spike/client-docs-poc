[client](../../../index.md) / [Exports](../modules.md) / [src/app/Routes/Admin/EstateEdit](../modules/src_app_Routes_Admin_EstateEdit.md) / IEstateEditLocationState

# Interface: IEstateEditLocationState

[src/app/Routes/Admin/EstateEdit](../modules/src_app_Routes_Admin_EstateEdit.md).IEstateEditLocationState

## Table of contents

### Properties

- [id](src_app_Routes_Admin_EstateEdit.IEstateEditLocationState.md#id)

## Properties

### id

• **id**: `string`

#### Defined in

[src/app/Routes/Admin/EstateEdit/index.tsx:18](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Routes/Admin/EstateEdit/index.tsx#lines-18)
