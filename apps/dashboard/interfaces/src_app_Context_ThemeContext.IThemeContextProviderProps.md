[client](../../../index.md) / [Exports](../modules.md) / [src/app/Context/ThemeContext](../modules/src_app_Context_ThemeContext.md) / IThemeContextProviderProps

# Interface: IThemeContextProviderProps

[src/app/Context/ThemeContext](../modules/src_app_Context_ThemeContext.md).IThemeContextProviderProps

## Table of contents

### Properties

- [children](src_app_Context_ThemeContext.IThemeContextProviderProps.md#children)

## Properties

### children

• **children**: `ReactChild` \| `ReactChild`[]

#### Defined in

[src/app/Context/ThemeContext.tsx:34](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Context/ThemeContext.tsx#lines-34)
