[client](../../../index.md) / [Exports](../modules.md) / [src/app/Themes/UniteTheme](../modules/src_app_Themes_UniteTheme.md) / UniteTypography

# Interface: UniteTypography

[src/app/Themes/UniteTheme](../modules/src_app_Themes_UniteTheme.md).UniteTypography

## Hierarchy

- `Typography`

  ↳ **`UniteTypography`**

## Table of contents

### Properties

- [body1](src_app_Themes_UniteTheme.UniteTypography.md#body1)
- [body2](src_app_Themes_UniteTheme.UniteTypography.md#body2)
- [button](src_app_Themes_UniteTheme.UniteTypography.md#button)
- [caption](src_app_Themes_UniteTheme.UniteTypography.md#caption)
- [dataGridTile](src_app_Themes_UniteTheme.UniteTypography.md#datagridtile)
- [details](src_app_Themes_UniteTheme.UniteTypography.md#details)
- [h1](src_app_Themes_UniteTheme.UniteTypography.md#h1)
- [h2](src_app_Themes_UniteTheme.UniteTypography.md#h2)
- [h3](src_app_Themes_UniteTheme.UniteTypography.md#h3)
- [h4](src_app_Themes_UniteTheme.UniteTypography.md#h4)
- [h5](src_app_Themes_UniteTheme.UniteTypography.md#h5)
- [h6](src_app_Themes_UniteTheme.UniteTypography.md#h6)
- [overline](src_app_Themes_UniteTheme.UniteTypography.md#overline)
- [subtitle1](src_app_Themes_UniteTheme.UniteTypography.md#subtitle1)
- [subtitle2](src_app_Themes_UniteTheme.UniteTypography.md#subtitle2)

## Properties

### body1

• **body1**: `CSSProperties`

#### Inherited from

Typography.body1

___

### body2

• **body2**: `CSSProperties`

#### Inherited from

Typography.body2

___

### button

• **button**: `CSSProperties`

#### Inherited from

Typography.button

___

### caption

• **caption**: `CSSProperties`

#### Inherited from

Typography.caption

___

### dataGridTile

• **dataGridTile**: `CSSProperties`

#### Defined in

[src/app/Themes/UniteTheme.ts:56](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Themes/UniteTheme.ts#lines-56)

___

### details

• **details**: `CSSProperties`

#### Defined in

[src/app/Themes/UniteTheme.ts:55](https://bitbucket.org/spikedevs/unite2/src/bba2574/client/apps/dashboard/src/app/Themes/UniteTheme.ts#lines-55)

___

### h1

• **h1**: `CSSProperties`

#### Inherited from

Typography.h1

___

### h2

• **h2**: `CSSProperties`

#### Inherited from

Typography.h2

___

### h3

• **h3**: `CSSProperties`

#### Inherited from

Typography.h3

___

### h4

• **h4**: `CSSProperties`

#### Inherited from

Typography.h4

___

### h5

• **h5**: `CSSProperties`

#### Inherited from

Typography.h5

___

### h6

• **h6**: `CSSProperties`

#### Inherited from

Typography.h6

___

### overline

• **overline**: `CSSProperties`

#### Inherited from

Typography.overline

___

### subtitle1

• **subtitle1**: `CSSProperties`

#### Inherited from

Typography.subtitle1

___

### subtitle2

• **subtitle2**: `CSSProperties`

#### Inherited from

Typography.subtitle2
